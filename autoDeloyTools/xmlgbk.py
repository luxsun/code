# -*- coding:utf-8 -*-
###
# File: xmlgbk.py
# Created Date: 2020-11-16
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Monday November 16th 2020 11:34:23 am
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
###

f = ET.parse('/usr/weaver/ecology/WEB-INF/web.xml',parser=xmlp)
for i in f.iterfind('filter/filter-name'):
    print(i.text)