import os
import loggingtest
import datetime
from rich.console import Console

today = datetime.datetime.now()
# log.logger.debug(today)
formatted_today=today.strftime('%Y-%m-%d-%H-%M')
log = loggingtest.Logger('AutoDeployTools-{0}.log'.format(formatted_today),level='debug')
# 打印信息
def print_self(string,color):
    log.logger.debug(string+'\n')
    console = Console()
    if color == 'red':
        console.print("{0}".format(string), style="red on black")
        # log.logger.debug()
    elif color == 'green':
        console.print("{0}".format(string), style="green on black")
    elif color == 'yellow':
        console.print("{0}".format(string), style="yellow on black")
    else:
        print(string)

#判断路径是否存在
def path_exists(path):
    if os.path.exists(path):
        return True
    else:
        print_self(
            '\t{0} path is not exist,Please input the path again'.format(path), 'red')
        # print('{0} path is not exist,Please input the path again'.format(path))
        os._exit()
        # return False

# 判断文件写权限
def file_w(filename):
    return os.access(filename, os.W_OK)

# 判断文件读权限
def file_r(filename):
    return os.access(filename,os.R_OK)

# 判断文件执行权限
def file_x(filename):
   return os.access(filename, os.X_OK)

#  exec the sys command
def execCmd(cmd):
    """exec sys cmd

    Args:
        cmd ([str]): [the command wait exec]

    Returns:
        [str]: [ the command results ]
    """    
    try:  
        # print(cmd)
        r = os.popen(cmd)  
        text = r.read()  
        r.close()
    except BaseException as e:
        print_self('\tRunError \t {0}'.format(str(e)),'red')  
        text = ''
        os.exit(1)
    return text 
