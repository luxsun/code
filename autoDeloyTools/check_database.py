#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# File: check_database.py
###
import os
import pymysql
import pymssql
import re
import jpype,jaydebeapi
import common_tools as common_tools


def mysql_test(host, port, user, passwd, db):
    try:
        db_connect = pymysql.connect(
            host=host, port=port, user=user, password=passwd, db=db, charset='utf8')
    except BaseException as e:
        common_tools.print_self('\tThe Connection to the DB Error,the reason maybe is {0}'.format(e), 'red')
        return
    try:
        cursor = db_connect.cursor()
        cursor.execute("SELECT * FROM hrmresourcemanager")
        row = cursor.fetchone()
        common_tools.print_self('\t' + str(row),'green')
    except BaseException as e:
        common_tools.print_self('\tThe Query is error,the reason maybe is {0}'.format(e),'red')


def sqlserver_test(host, user, passwd, db,port):
    try:
        # print(host,port,user,passwd,db)
        conn = pymssql.connect(host=str(host+':'+port),user=str(user),password=str(passwd),database=str(db))
    except BaseException as e:
        common_tools.print_self('\tThe Connection to the DB Error,the reason maybe is {0}'.format(e), 'red')
        return
    try:
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM  HrmResourceManager;")
        row = cursor.fetchone()
        common_tools.print_self('\t'+str(row),'green')
    except BaseException as e:
        common_tools.print_self('\tThe Query is error,the reason maybe is {0}'.format(e),'red')


def oracle_test(jarpath,url,user,password,error_dic):
    if jarpath == '':
        return "Oracle jar can't find Skip check!"
    else:
        ora_error_list = []
        try:
            common_tools.print_self('\t{0}'.format(jarpath), '')
            jpype.startJVM(classpath=[jarpath])
            db=jaydebeapi.connect('oracle.jdbc.OracleDriver',url,[user,password])
        except BaseException as e:
            for key in error_dic.keys():
                if str(e).find(key) != -1:
                    common_tools.print_self('\t{0}, {1}'.format(key, error_dic[key]), 'red')
                    return
                else:
                    ora_error_list.append(str(e))
        if len(set(ora_error_list)) > 0:
            for i in set(ora_error_list):
                common_tools.print_self('\t{0}'.format(i), 'red')
                return
        try:
            cursor = db.cursor()
            cursor.execute("SELECT  * FROM HRMRESOURCEMANAGER h ")
            row = cursor.fetchone()
            common_tools.print_self('\t'+str(row),'green')
        except BaseException as e:
            common_tools.print_self('\tThe Query is error,the reason maybe is {0}'.format(e),'red')


def check_database(app_path,error_dic):
    if os.path.exists(os.path.join(app_path,'WEB-INF/lib/ojdbc6.jar')):
        jar_path=os.path.join(app_path,'WEB-INF/lib/ojdbc6.jar')
    elif os.path.exists(os.path.join(app_path,'WEB-INF/lib/ojdbc-6g.jar')):
        jar_path=os.path.join(app_path,'WEB-INF/lib/ojdbc-6g.jar')
    else:
        common_tools.print_self('ojdbc.jar or ojdbc-6g can not found,the oracle db connect will error','red')
        jar_path=""
    weaver_conf = os.path.join(app_path, 'WEB-INF/prop/weaver.properties')
    if os.path.exists(weaver_conf):
        if os.path.getsize(weaver_conf) > 0:
            with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as f:
                if 'mysql' in f.read():
                    # print('According to the configuration file, determine that the database type is mysql, and start to verify the mysql link')
                    with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as g:
                        for i in g:
                            if 'jdbc:mysql' in i:
                                i = i.replace("\\","")
                                common_tools.print_self('\t' + i,'')
                                db_host_ip = re.findall(r'\d+.\d+.\d+.\d+', i)
                                db_port = ((i.split('?')[0]).split(
                                    '/')[2]).split(':')[1].strip()
                                db_name = ((i.split('?')[0])).split('/')[3].strip()
                            elif 'ecology.user' in i:
                                db_user = i.split('=')[1].strip()
                            elif 'ecology.password' in i:
                                db_passwd = i.split('=')[1].strip()
                    try:
                        common_tools.print_self('\tThe DB properties in weaver.properties is {0} {1} {2} {3} {4}'.format(db_host_ip[0],int(db_port),
                               db_user, db_passwd, db_name),'green')
                        mysql_test(db_host_ip[0],int(db_port),db_user,db_passwd,db_name)
                    except BaseException as e:
                        common_tools.print_self(e,'red')
            with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as f:
                # print('sqlserver')
                if 'microsoft.sqlserver' in f.read():
                    # print('According to the configuration file, determine that the database type is sqlserver, and start to verify the sqlserver link')
                    with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as g:
                        for i in g:
                            if 'jdbc:sqlserver' in i:
                                i = i.replace("\\","")
                                common_tools.print_self('\t' + i,'')
                                # print(i)
                                db_host_ip = (
                                    (i.split('//')[1]).split(';')[0]).split(':')[0].strip()
                                db_port = (
                                    (i.split('//')[1]).split(';')[0]).split(':')[1].strip()
                                db_name = i.split('=')[2].strip()
                            elif 'ecology.user' in i:
                                db_user = i.split('=')[1].strip()
                            elif 'ecology.password' in i:
                                db_passwd = i.split('=')[1].strip()
                    try:
                        common_tools.print_self('\tThe DB properties in weaver.properties is {0} {1} {2} {3} {4}'.format(db_host_ip,db_port,db_user,db_passwd
                        ,db_name), 'green')
                        sqlserver_test(db_host_ip,db_user,db_passwd,db_name,db_port)
                    except BaseException as e:
                        common_tools.print_self(e,'red')
            with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as f:
                if 'oracle' in f.read():
                    # print('According to the configuration file, determine that the database type is Oracle, and start to verify the Oracle link')
                    with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as g:
                        for i in g:
                            if 'oracle' in i and "jdbc" in i and ':' in i:
                                # ecology.url = jdbc\:oracle\:thin:@192.168.7.32\:1521/ecology
                                i = i.replace("\\","")
                                common_tools.print_self('\t' + i,'')
                                url=i.split("=")[1].strip()
                            elif 'ecology.user' in i:

                                db_user = i.split('=')[1].strip()
                            elif 'ecology.password' in i:
                                db_passwd = i.split('=')[1].strip()
                    
                    try: 
                        common_tools.print_self('\tThe DB properties in weaver.properties is {0} {1} {2}'.format(url, db_user, db_passwd), 'green')
                        oracle_test(jar_path, url, db_user,db_passwd,error_dic)
                    except BaseException as e:
                        common_tools.print_self('\t Get DB info error,the reason maybe is {0},Please check the weaver.properties'.format(str(e)),'red')
                        # ...
            with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as f:
                count = 0
                for i in f:
                    if i.find('oracle') != -1 or i.find('sqlserver') !=-1 or i.find('mysql') != -1:
                        count+=1
                    else:
                        pass
                if count > 0:
                    pass
                else:
                    common_tools.print_self('\tThe link information of the database oracle or sqlserver or mysql is not found, please check!','red')
        else:
            common_tools.print_self('\tThe file weaver.properties is Empty,Please Check','red')
    else:
        common_tools.print_self('\tThe {0} Path can not be found,Please Check!'.format(weaver_conf),'red')
