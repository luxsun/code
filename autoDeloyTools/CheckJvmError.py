#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# File: CheckJvmError.py
###
import os
import re
from collections import deque
import common_tools
import socket
# import common_error_keyword as keyword_list

def isIncludeKeyWord(detailinfo, keyword_list, num):
    # 用来判断是否有关键字错误
    #  去重行，行内容太多
    for tmp_keyword in keyword_list:
        # 正则表达查询性能较差，先用find函数过滤，因为大部分字符串都不需要走正则查询
        if -1 != detailinfo.find(tmp_keyword):
            # 匹配^tmp_keyword$或^tmp_keyword(非字母数字下划线)(任意字符)
            # 或(任意字符)(非字母数字下划线)tmp_keyword$
            # 或(任意字符)(非字母数字下划线)tmp_keyword(非字母数字下划线)(任意字符)
            pattern_str = '(^'+tmp_keyword+'$)|(^'+tmp_keyword+'(\W+).*)|(.*(\W+)' + \
                tmp_keyword+'$)|(.*(\W+)'+tmp_keyword+'(\W+).*)'
            m = re.match(r''+pattern_str+'', detailinfo)
            if m:
                if detailinfo.strip() not in temp_list and tmp_keyword not in ''.join(temp_list):
                    temp_list.append(detailinfo.strip())
                    # print(num, detailinfo.strip())
                        
                # with open('error.log','a+',encoding='utf-8',errors='ignore') as f:
                # 	# 强转为str，不然写入报错
                # 	str_num = str(num)
                # 	str_detailinfo = str(detailinfo)
                # 	f.writelines((str_num,str_detailinfo))
                # #
    return 0


temp_list = []

# def check_resin_status(port,os_type):
#     """[check_resin_status]

#     Args:
#         port ([int]): [the resin listening port]
#     """
#     if os_type == "Linux"
#         text = common_tools.execCmd('ss -tanlp' + '| grep {0}'.format(port)  + '| grep java' + ' | grep -v 6800' + '|grep -v monitor' + ' | wc -l' )
#         stat = text.strip()
#     elif os_type == "windows":
#         text = common_tools.execCmd('netstat -ano | findstr {0}'.format(port))
#     if int(stat) >=1:
#         return True
#     else:
#         return False


def check_port_in_use(port, host='127.0.0.1'):
    s = None
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(1)
        s.connect((host, int(port)))
        return True
    except socket.error:
        return False
    finally:
        if s:
            s.close()


def CheckJvmError(resin_path,error_keyword_list,error_keyword_dic):
    resin4_conf = os.path.join(resin_path,'conf/resin.properties')
    resin3_conf = os.path.join(resin_path,'conf/resin.conf')
    if os.path.exists(resin4_conf):
        with open(resin4_conf,'r',encoding='utf-8',errors='ignore') as f:
            app_http_list = re.findall(r'app.http.+\d',f.read())
            port = app_http_list[0].split(':')[1].strip() 
    elif os.path.exists(resin3_conf):
        with open(resin3_conf,'r',encoding='utf-8',errors='ignore') as f:
            try:
                app_http_list = re.findall(r'<http.*',f.read())
                fin = re.findall(r"\d+\.?\d*",''.join(app_http_list))
                port=int(fin[0]) 
            except  BaseException as e:
                port=80
                print(str(e))
    else:
        common_tools.print_self('\tCant Find The resin.properties or Resin.conf ,Please Check! Exit!','red')
        os._exit(0)
    for i in os.listdir(os.path.join(resin_path,'log')):
        if re.findall(r'jvm.*log$',i):
            log_name = i
            jvm_log_path=os.path.join(resin_path,'log/{0}'.format(i))
            break
    stdout_path = os.path.join(resin_path, 'log/stdout.log')
    stderr_path = os.path.join(resin_path,'log/stderr.log')
    if os.path.exists(jvm_log_path) and os.path.getsize(jvm_log_path) > 0:
        with open(jvm_log_path, 'r', encoding='utf-8', errors='ignore') as fr, open('temp.log', 'w+', encoding='utf-8', errors='ignore') as fw:
            dq = deque(fr)
            while dq:
                last_row = dq.pop()
                fw.write(last_row)
                if last_row.find('Starting Resin on') != -1:
                    break
        common_tools.print_self('\tStart scanning the Error Log after the last Strart Resin in the last {0}'.format(log_name),'green')
        with open('temp.log', 'r', encoding='utf-8', errors='ignore') as f:
            for num, value in enumerate(f):
                isIncludeKeyWord(value, error_keyword_list, num)
        if len(temp_list) == 0:
            common_tools.print_self('\tError message about error keyword not found in {0}'.format(log_name),'red')
        else:
            for i in temp_list:
                for j in error_keyword_list:
                    if str(j)  in str(i):
                        common_tools.print_self('\tFind error keyword\t {0},Recommended solutions\t{1}'.format(i,error_keyword_dic[j]),'yellow')
                # print()
        temp_list.clear()
        # print('因错误日志过多，请在当前目录下的error.log文件中查看{0}的错误信息'.format(jvm_log_path))
        # # 从后往前扫，扫到第一个start resin为止， 取start resin至末尾的内容
        # with open(jvm_log_path,'r',encoding='utf-8',errors='ignore') as f:
        # 	for num,value in enumerate(f):
        # 		# print(num,value)
        # 		isIncludeKeyWord(value,error_keyword_list,num)
    else:
        common_tools.print_self('\t{0} file does not exists,please check!'.format(jvm_log_path),'red')
        if not check_port_in_use(port=port):
            common_tools.print_self('\tResin is not detected, please start Resin.!','red')
        else:
            common_tools.print_self('\t{0}The file does not exist or the file is empty, please check！'.format(jvm_log_path),'red')
    print('\n')

    common_tools.print_self('\tStart scanning error logs in stdout','green')
    if os.path.exists(stdout_path) and os.path.getsize(stdout_path) > 0:
        with open(stdout_path, 'r', encoding='utf-8', errors='ignore') as f:
            for num, value in enumerate(f):
                # print(num,value)
                isIncludeKeyWord(value, error_keyword_list, num)
        if len(temp_list) == 0:
            common_tools.print_self('\tError message about error keyword not found in stdout','red')
        else:
            for i in temp_list:
                for j in error_keyword_list:
                    if str(j)  in str(i):
                        common_tools.print_self('\tFind error keyword\t {0},Recommended solutions\t{1}'.format(i,error_keyword_dic[j]),'red')
        temp_list.clear()
    else:
        common_tools.print_self('\t{0} file does not exists,please check!'.format(stdout_path),'red')
        if not check_port_in_use(port=port):
            common_tools.print_self('\tResin is not detected, please start Resin.!','red')
        else:
            common_tools.print_self('\t{0}The file does not exist or the file is empty, please check！'.format(stdout_path),'red')

    common_tools.print_self('\tStart scanning error logs in stderr','green')
    if os.path.exists(stderr_path) and os.path.getsize(stderr_path) > 0:
        with open(stderr_path, 'r', encoding='utf-8', errors='ignore') as f:
            for num, value in enumerate(f):
                # print(num,value)
                isIncludeKeyWord(value, error_keyword_list, num)
        if len(temp_list) == 0:
            common_tools.print_self('\tError message about error keyword not found in stderr','red')
        else:
            for i in temp_list:
                for j in error_keyword_list:
                    if str(j)  in str(i):
                        common_tools.print_self('\tFind error keyword\t {0},Recommended solutions\t{1}'.format(i,error_keyword_dic[j]),'red')
        temp_list.clear()
    else:
        common_tools.print_self('\t{0} file does not exists,please check!'.format(stderr_path),'red')
        if not  check_port_in_use(port=port):
            common_tools.print_self('\tResin is not detected, please start Resin.!','red')
        else:
            common_tools.print_self('\t{0}The file does not exist or the file is empty, please check！'.format(stdout_path),'red')

