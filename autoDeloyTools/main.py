import os
from platform import version

import check_database as check_db
import CheckJvmError as check_jvm
import common_tools as common_tools
import check_web_env as check_env
import configparser
import platform


if __name__ == "__main__":
    # 判断系统平台
    os.system('')
    os_platform=platform.system()
    config_path=os.path.join(os.getcwd(),'config')
    if os.path.exists(config_path):
        # 解析配置
        config = configparser.ConfigParser()
        config.read('config',encoding='utf8')
    else:

        common_tools.print_self('the  config does not esixts,please check','red')
        os._exit(0)
    try:
        version = config['config']['version']
        app_path=config['config']['ecology_path']
        resin_path=config['config']['resin_path']
        jdk_path=config['config']['jdk_path']
        if os.path.exists(app_path) and os.path.exists(resin_path) and os.path.exists(jdk_path):
            ...
        else:
            common_tools.print_self("The path Can't Find,please check the config",'red')
            os._exit(0)
        db_error_dic=eval(config['config']['db_error_dic'])
        # print(db_error_dic)
        error_keyword_dic=eval(config['config']['error_keyword_dic'])
        # print(error_keyword_dic)
        # 将字典的键设置为错误关键字列表
        error_keyword_list=error_keyword_dic.keys()
        jre_path = os.path.join(jdk_path,'jre')
        os.environ["JAVA_HOME"] = jre_path
        if os_platform == 'Linux':
            javac_path = os.path.join(jdk_path, 'bin/javac')
            
        elif os_platform == 'Windows':
            javac_path = os.path.join(jdk_path, 'bin\javac')
        else:
            print('The current system type is not currently supported, please make sure the current system is linux or windows type.')
        # ! 使用eval函数，将类字典转为字典
        
    except BaseException as e:
        common_tools.print_self('ConfigParse Error,Please Check the config,reason is {0}'.format(str(e)),'red')
        os._exit(0)
    common_tools.print_self('=========================================================================','yellow')
    common_tools.print_self('        Test results (green for normal, red for abnormal) start:        ','yellow')
    common_tools.print_self('=========================================================================','yellow')
    common_tools.print_self('The script version {0}'.format(version),'green')
    common_tools.print_self('Disk space usage check results：','yellow')
    check_env.check_disk(app_path,os_platform)
    if os_platform  == "Linux":
        common_tools.print_self('\nResin configuration information check results：','yellow')
        check_env.check_resin(app_path,resin_path,javac_path,os_platform)

        common_tools.print_self('\nStartup file permission check results：','yellow')
        check_env.check_file_auth(resin_path,jdk_path)
        common_tools.print_self('\nPath detection results in the startup file：','yellow')
        check_env.check_sh(jdk_path,resin_path)
        common_tools.print_self('\nWeb.xml file format check results：','yellow')
        check_env.check_webxml(app_path)
        common_tools.print_self('\nDatabase connectivity check results：','yellow')
        check_db.check_database(app_path,db_error_dic)
        common_tools.print_self('\nThe Resin log check results：','yellow')
        check_jvm.CheckJvmError(resin_path,error_keyword_list,error_keyword_dic)
        common_tools.print_self("\nNFS check",'yellow')
        check_env.nfs_check()
        
    elif os_platform == "Windows":
        common_tools.print_self('\nResin configuration information check results：','yellow')
        check_env.check_resin(app_path,resin_path,javac_path,os_platform)
        common_tools.print_self('\nWeb.xml file format check results：','yellow')
        check_env.check_webxml(app_path)
        common_tools.print_self('\nThe Resin log check results：','yellow')
        try:
            check_jvm.CheckJvmError(resin_path,error_keyword_list,error_keyword_dic)
        except BaseException as e:
            print(str(e))
        common_tools.print_self('\nDatabase connectivity check results：','yellow')
        check_db.check_database(app_path,db_error_dic)
        os.system('pause')

    common_tools.print_self('=========================================================================','yellow')
    common_tools.print_self('        Test results (green for normal, red for abnormal) start:        ','yellow')
    common_tools.print_self('=========================================================================','yellow')
    os._exit(1)
