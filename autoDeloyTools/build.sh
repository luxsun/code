#!/bin/bash
#---------------------------------------------------------------------------
# File: build.sh
# Created Date: 2020-11-02
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Friday November 6th 2020 5:40:23 pm
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
#---------------------------------------------------------------------------
datetime=$(date +"%F-%H-%M")

if [ -e "main.py" ];then
    read -p "please input version" version
    pyinstaller --noconfirm --onefile --console --add-binary   "/env/python36/lib/python3.6/site-packages/org.jpype.jar;." main.py -n AutoDeployTools-$datetime
    staticx ./dist/AutoDeployTools-$datetime ./dist/AutoDeployTools-$datetime-$version-all
    cp ./config ./dist
    cd dist
    sed -i '2d' ./config && sed -i "2i version=$version" ./config
    
    tar -zcvf AutoDeployTools-$datetime-$version.tar.gz AutoDeployTools-$datetime-$version-all config oracle-instantclient18.3-basic-18.3.0.0.0-3.x86_64.rpm
fi
