# -*- coding:utf-8 -*-
###
# File: check_web_env.py
# Created Date: 2020-11-04
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Wednesday November 18th 2020 5:10:23 pm
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
###
import os

import psutil
import xml.etree.ElementTree as ET
xmlp = ET.XMLParser(encoding="utf-8")
# import lxml.etree as ET
import common_tools as common_tools
import re

#检查磁盘空间================================================================
def check_disk(app_path,ostype):
    if ostype == 'Linux':
        root_path_usage = psutil.disk_usage('/')[3]
        available=round(psutil.disk_usage(
            '/')[2]/1024/1024/1024, 2)
    elif ostype == 'Windows':
        root_path_usage = psutil.disk_usage('C:')[3]
        available=round(psutil.disk_usage(
            'C:')[2]/1024/1024/1024, 2)
    else:
        root_path_usage = 0
        available= 0 
    app_path_usage = psutil.disk_usage(app_path)[3]
    common_tools.print_self('\tThe root paration(default C: for windows) use percent is {0}%\tavailable use {1}GB\n\tThe app Paration use Percent is {2}%\tavailable use {3}GB'.format(root_path_usage, available, app_path_usage, round(psutil.disk_usage(app_path)[2]/1024/1024/1024, 2)), 'green')


    
    if root_path_usage > 80 :
        # common_tools.print_self('The disk used Percent is > 80%,Please Check!')
        common_tools.print_self('\tThe root partion or C: for windows  used Percent is {0}  > 80%,Please Check!'.format(root_path_usage), 'red')
    elif app_path_usage > 80:
        common_tools.print_self('\tThe app partion  used Percent is > 80%,Please Check!', 'red')
    else:
        ...

def alter(file,old_str,new_str):
    """[修改文件内容]

    Args:
        file ([str]): [file path]
        old_str ([str]): [需要修改的字符]
        new_str ([str]): [修改后的字符]
    """    
    with open(file, "r", encoding="utf-8") as f1,open("%s.bak" % file, "w", encoding="utf-8") as f2:
        for line in f1:
            f2.write(re.sub(old_str,new_str,line))
    os.remove(file)
    os.rename("%s.bak" % file, file)


#检查resin配置================================================================
def check_resin(app_path,resin_path,javac_path,os_platform):
    resin_xml_path = os.path.join(resin_path, 'conf/resin.xml')
    resin_conf_path = os.path.join(resin_path,'conf/resin.conf')
    if os.path.exists(resin_xml_path):
        with open(resin_xml_path, 'r', encoding='utf-8') as f:
            for i in f:
                if '<javac compiler' in str(i) and '#' not in str(i):
                    temp_javac = str((i.split('=')[1]).split(' ')[
                                     0]).replace('"', '')
                    if os_platform == "Windows":
                        temp_javac=temp_javac.lower()
                        javac_path = javac_path.lower()
                        # print(temp_javac,javac_path)
                    else:
                        ...
                    if temp_javac == javac_path:
                        common_tools.print_self(
                            '\tThe javac path in resin.xml is {0},Check Passed!'.format(temp_javac), 'green')
                    else:
                        common_tools.print_self(
                            '\tThe javac path in resin.xml is {0},Check Error,Please Check!'.format(temp_javac), 'red')
                elif 'ecology' in str(i) and 'root-directory' in str(i) and '#' not in str(i) :
                    temp_ecology_path = str(
                        (i.split('=')[2]).split('>')[0]).replace('"', '')
                    if os_platform == "Windows":
                        temp_ecology_path=temp_ecology_path.lower()
                        app_path = app_path.lower()
                    else:
                        ...
                    if temp_ecology_path == str(app_path):
                        common_tools.print_self('\tThe ecology path in resin.xml is {0},Check Passed'.format(
                            temp_ecology_path), 'green')
                    else:
                        common_tools.print_self('\tThe ecology path in resin.xml is {0},Check Error,Please Check'.format(
                            temp_ecology_path), 'red')
    elif os.path.exists(resin_conf_path):
         with open(resin_conf_path, 'r', encoding='utf-8') as f:
            for i in f:
                if '<javac compiler' in str(i) and '#' not in str(i):
                    temp_javac = str((i.split('=')[1]).split(' ')[
                                     0]).replace('"', '')
                    if os_platform == "Windows":
                        temp_javac=temp_javac.lower()
                        javac_path = javac_path.lower()
                        # print(temp_javac,javac_path)
                    if temp_javac == javac_path:
                        common_tools.print_self(
                            '\tThe javac path in resin.conf is {0},Check Passed!'.format(temp_javac), 'green')
                    else:
                        common_tools.print_self(
                            '\tThe javac path in resin.conf is {0},Check Error,Please Check!'.format(temp_javac), 'red')
                elif 'ecology' in str(i) and 'root-directory' in str(i) and '#' not in str(i) :
                    temp_ecology_path = str(
                        (i.split('=')[2]).split('>')[0]).replace('"', '')
                    if temp_ecology_path == str(app_path):
                        common_tools.print_self('\tThe ecology path in resin.conf is {0},Check Passed'.format(
                            temp_ecology_path), 'green')
                    else:
                        common_tools.print_self('\tThe ecology path in resin.conf is {0},Check Error,Please Check'.format(
                            temp_ecology_path), 'red')

    else:
        common_tools.print_self('\tresin.xml or resin.conf file can not be found,Please Check', 'red')


#检查关键文件是否有可执行权限===========================================================
def check_file_auth(resin_path,jdk_path):
    #resin启动文件
    x1 = common_tools.file_x(os.path.join(resin_path, 'bin/startresin.sh'))
    x2 = common_tools.file_x(os.path.join(resin_path, 'bin/stopresin.sh'))
    x3 = common_tools.file_x(os.path.join(resin_path, 'bin/resin.sh'))

    #jdk执行文件
    x4 = common_tools.file_x(os.path.join(jdk_path, 'bin/java'))
    x5 = common_tools.file_x(os.path.join(jdk_path, 'bin/javac'))
    if x1 == False or x2 == False or x3 == False or x4 == False or x5 == False :
        common_tools.print_self("\tResin startup script (path："+os.path.join(resin_path, 'bin/') +")  or Jdk start command(path:"+os.path.join(jdk_path, 'bin/')+")No executable privileges, please empower.！", 'red')
        common_tools.print_self("\tStart fixing executive privileges",'red')
        resin_exec_path=os.path.join(resin_path, 'bin/')
        for i in os.listdir(resin_exec_path):
            os.system('chmod +x {0}'.format(os.path.join(resin_exec_path,i)))
        common_tools.print_self("\tFix execution permissions complete, please check",'green')

    else :
        common_tools.print_self("\tResin startup scripts and jdk startup commands have normal permissions. ,check Passed'", 'green')

# web.xml配置文件检测================================================================
def check_webxml(app_path):
    try:
        #检测web.xml格式
        file_path = os.path.join(app_path, 'WEB-INF/web.xml')
        ET.parse(file_path,parser=xmlp)
        common_tools.print_self('\tweb.xml file formate correct ,check Passed', 'green')
    except BaseException as e:
        common_tools.print_self('\tweb.xml format error,the reason maybe is {0},Please Check'.format(e), 'red')

def check_sh(jdk_path,resin_path):
    """check the path in startresin.sh 

    Args:
        jdk_path ([str]): [jdk_path]
        resin_path ([str]): [resin]
    """    
    # sh路径
    correct_path=os.path.join(resin_path,'bin/resin.sh')
    startsh=os.path.join(resin_path,'bin/startresin.sh')
    stopsh=os.path.join(resin_path,'bin/stopresin.sh')
    if os.path.exists(correct_path) and os.path.exists(startsh) and os.path.exists(stopsh):
        with open(startsh,'r',encoding='utf-8') as f:
            for i in f:
                if i.find('resin.sh') != -1 and '#' not in str(i):
                    if i.split(' ')[0] == correct_path:
                        common_tools.print_self('\tstartresin.sh CheckPassed','green')
                    else:
                        common_tools.print_self('\t{0} check error'.format(i.split(' ')[0]),'red')
                        common_tools.print_self('\tBegin to repair','red')
                        alter(startsh,i.split(' ')[0],correct_path)
                        os.system('chmod +x {0}'.format(startsh))
                        common_tools.print_self('\tRepair Success, please check!','green')

        with open(stopsh,'r',encoding='utf-8') as f:
            for i in f:
                if i.find('resin.sh') != -1 and '#' not in str(i):
                    if i.split(' ')[0] == correct_path:
                        common_tools.print_self('\tstopresin.sh CheckPassed','green')
                    else:
                        common_tools.print_self('\t{0} check error'.format(i.split(' ')[0]),'red')
                        common_tools.print_self('\tBegin to repair','red')
                        alter(stopsh,i.split(' ')[0],correct_path)
                        os.system('chmod +x {0}'.format(stopsh))
                        common_tools.print_self('\tRepair Success, please check!','green')
                        

        with open(correct_path,'r+',encoding='utf-8') as f:
            for i in f:
                # common_tools.print_self(i)
                if i.find('JAVA_HOME=') != -1 and '#' not in str(i):
                    if i.split('=')[1].strip() == jdk_path:
                        common_tools.print_self('\tresin.sh CheckPassed','green')
                    else:
                        common_tools.print_self('\t{0} check error'.format(i.split('=')[1].strip(
                        )),'red')
                        common_tools.print_self('\tbegin to repair','red')
                        alter(correct_path,i.split('=')[1].strip(),jdk_path)
                        os.system('chmod +x {0}'.format(correct_path))
                        common_tools.print_self('\tRepair Success, please check!','green')
                        

    else:
        common_tools.print_self('\tPleae Check the resin path,can not found resin.sh or startresin.sh or stopresin.sh','red')


def nfs_check():
    with open('/etc/fstab','r',encoding='utf-8',errors='ignore') as f:
        if 'nfs' in ''.join(f.readlines()):
            with open('/etc/fstab','r',encoding='utf-8',errors='ignore') as f:
                for i in f:
                    # print(i)
                    if re.findall(r'\d+\.\d+\.\d+\.\d+',str(i)) and re.findall('nfs',str(i),flags=re.IGNORECASE):
                        common_tools.print_self('\tNFS is detected mounted on the system,Start testing if nfs can read and write files','green')
                        mountlist=list(filter(None,i.split(' ')))
                        common_tools.print_self('\tNFS info {0}'.format(i),'green')
                        # ['192.168.81.40:/data/shared', '/usr/mountpoint', 'nfs', 'defaults', '0', '0', '\n']
                        # 检测是否挂载了nfs 
                        mountresult=common_tools.execCmd('df -h | grep {0} | wc -l'.format(mountlist[1])).strip()
                        if int(mountresult) == 1:
                            print("\t{0} NFS already mounted,begin the check the write/read".format(mountlist[0]))
                            try:
                                with open(os.path.join(mountlist[1],'nfstest'),'w',encoding='utf8',errors='ignore') as f:
                                    f.write('nfs write/read check!')
                                    common_tools.print_self('\tnfs check passed','green')
                            except BaseException as e:
                                common_tools.print_self('\tNfs write error,try to repair!','red')
                                input_y=input("\tit will exec unmount -l  {0},please enter y to continue,else exit!".format(mountlist[1]))
                                if input_y =='y' or input_y == 'Y':
                                    try:
                                        common_tools.execCmd('umount -l  {0}'.format(mountlist[1]))
                                        common_tools.print_self('\tUmount Successed,Begin to mount!','green')
                                        common_tools.execCmd('mount -t nfs {0} {1}'.format(mountlist[0],mountlist[1]))
                                        common_tools.print_self('\tRemounted nfs successed, if still error, please check nfs permission.','green')
                                    except BaseException as e:
                                        common_tools.print_self("\trepair error,please check,the reason maybe is {0}".format(e),'red')
                                else:
                                    common_tools.print_self('\tenter other keys,function exit!','red')
                        else :
                            common_tools.print_self('\tThe nfs not mounted','red')
                            common_tools.print_self('\tTry to repair the nfs mount','yellow')
                                # mountexec=common_tools.execCmd('mount -t nfs {0} {1}'.format(mountlist[0],mountlist[1]))
                            input_y=input("\tit will exec  mount -t nfs {0} {1}',please enter y to continue,else exit!".format(mountlist[0],mountlist[1]))
                            if input_y =='y' or input_y == 'Y':
                                mountexec=common_tools.execCmd('mount -t nfs {0} {1}'.format(mountlist[0],mountlist[1]))
                                if mountexec != '':
                                    common_tools.print_self('\trepair error,please try mount manaual','red')
                                else:
                                    common_tools.print_self('\trepair success!','green')
                            else:
                                common_tools.print_self('\tenter other keys,function exit!','green')
        else:
            common_tools.print_self('\tNot found NFS,NFS check Passed!','green')