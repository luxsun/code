# -*- coding:utf-8 -*-
###
# File: initdb.py
# Created Date: 2020-10-16
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Wednesday October 21st 2020 11:48:19 am
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
###
import threading
import requests
import os
import time
import json
import multiprocessing as mp

def init_db(url):
    print('开始初始化数据库')
    s = requests.get(url)
    # if s.status_code != 200:
    #     print('The url access error!')
    #     os._exit(0)
    # else:
    #     __doc = json.loads(s.text)
    #     err_log = __doc['stat']['errormsg']
    #     print(err_log)
    #     os._exit()


def get_status(percent, status_url):

    print('开始处理状态')
    while percent != '100%':
        retrun_msg = ''
        response = []
        times = 0
        while times < 10:
            try:
                pro = requests.get(status_url,timeout=10)
                time.sleep(2)
                print(pro.text)
                response.append(pro.text)
                times = times + 1
            except BaseException as e:
                print('Connect Error,Please Retry ' + str(e))
                times = times + 1
        if retrun_msg.join(response).find('completePercent') != -1:
            pass
        else:
            print(response)
            os._exit(0)

        pro = requests.get(status_url,timeout=10)
        # print(pro.text)
        if (pro.text).find('errormsg') != -1:
            __doc = json.loads(pro.text)
            err_log = __doc['stat']['errormsg']
            msg = __doc['stat']['msg']
            if err_log == '':
                per = __doc['stat']['completePercent']
                # print()
                percent = per
                time.sleep(5)
                if msg.find('Success') != -1:
                    print('The current process percent is 100%!')
                    print('\tinit db success,please login !','green')
                else:  
                    print('The current process percent is {0},message {1}'.format(percent,msg))
            else:
                print('init error,the reason is {0},the database maybe already exists,please empty the database!'.format(err_log),'red')
                break
                os._exit(0)
        else:
            # await init_db()
            print('init error,the database maybe already exists,please empty the database and retry!','red')
            # print('initerror' + pro.text)
            os._exit(0)


if __name__ == '__main__':
    percent = ''
    base_url='http://192.168.81.49:80'
    status_url = base_url + '/api/system/createDB/base/getStatus'
    db_url = base_url + '/api/system/createDB/base/initdb?dbType=sqlserver&dbInstance=mssqlserver&dbIp=192.168.81.75&dbName=testsunzhe13&dbPort=1433&password=scmadmin&useCurrentDB=0&username=sa&validCode=wEAver2018'
    p = mp.Process(target=init_db,args=(db_url,))
    p.start()
    # time.sleep(5)
    get_status(percent,status_url)