# -*- coding:utf-8 -*-
###
# File: initdb_fin.py
# Created Date: 2020-10-21
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Tuesday October 27th 2020 4:26:44 pm
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
###
import requests
import re
import os
import time
import configparser
import checkdb
import json
# import aiohttp
# import asyncio
from requests.adapters import HTTPAdapter
import multiprocessing as mp
 

s = requests.Session()
s.mount('http://', HTTPAdapter(max_retries=3))
s.mount('https://', HTTPAdapter(max_retries=3))
# print(config['DATABASE']['DB_TYPE'])
local_base_path=os.getcwd()
# resin_f=os.path.join(local_base_path,'Resin/conf/resin.properties')
# resin_bin=os.path.join(local_base_path,'Resin/bin/startresin.sh')
db_info_path=os.path.join(local_base_path,'db.info')
oracle_ins_path=os.path.join(local_base_path,'oracle-instantclient18.3-basic-18.3.0.0.0-3.x86_64.rpm')


def print_self(string,color):
    if color == 'red':
        print('\033[5;31;40m{0}\033[0m'.format(string))
    elif color == 'green':
        print('\033[5;32;40m{0}\033[0m'.format(string))
    else:
        print(string)
# parse the db_info





def get_url(path):
    '''
    Get The Resin Port
    '''
    if os.path.exists(path):
        with open(path,'r',encoding='utf-8',errors='ignore') as f:
            app_http_list = re.findall(r'app.http.+\d',f.read())
            port = app_http_list[0].split(':')[1].strip()
    else:
        print_self('the Resin Conf path does not exists,please Check!','red')
    return port

def strart_resin(path):
    if os.path.exists(path):
        print_self('Start Resin,Please Wait!','green')
        execCmd('sh {0} >/dev/null 2&>1'.format(path))
        time.sleep(10)

        # result =         

def get_resin_stat(port):
    text = execCmd('ss -tanlp' + '| grep {0}'.format(port)  + '| grep java' + ' | grep -v 6800' + '|grep -v monitor' + ' | wc -l' )
    stat = text.strip()
    if int(stat) >=1:
        return True
    else:
        return False



def resin_start():
    if get_resin_stat(get_url(resin_f)) == False:
        strart_resin(resin_bin)
        # print('\twaiting the resin start!')
        time.sleep(10)
    else:
        print_self('\n\tThe Resin has been started','green')





def oracle_ins(path):
    if os.path.exists(path):
        execCmd('rpm -ivh {0}'.format(path))
        execCmd('sudo sh -c "echo /usr/lib/oracle/18.3/client64/lib > /etc/ld.so.conf.d/oracle-instantclient.conf"')
        execCmd('export LD_LIBRARY_PATH=/usr/lib/oracle/18.3/client64/lib:$LD_LIBRARY_PATH')
    else:
        print_self('Please check the oracle_instances.rpm in current dir','red')

def db_check(ver_code,db_type,db_host,db_port,db_name,db_user,db_passwd,db_instance):
    print_self('Check the db.info file','green')
    # print(ver_code)
    # print(db_type)
    if ver_code != '' and db_type != '' and db_host !='' and db_port !='' and db_name != '' and db_user !='' and db_passwd != '':
        if db_type == 'mysql':
            print("The DB_TYPE is MySQL")
            print(checkdb.mysql_test(db_host, int(db_port), db_user, db_passwd, db_name))
        elif db_type == 'oracle':
            print("The DB_TYPE is Oracle!")
            if not os.path.exists('/etc/ld.so.conf.d/oracle-instantclient.conf'):
                print('Begin to install oracle driver')
                oracle_ins(oracle_ins_path)
            oracle_conn=db_host + ':' + '1521' + '/' + db_name
            # print(oracle_conn)
            checkdb.oracle_test(db_user,db_passwd,oracle_conn)
            
        elif db_type == 'sqlserver':
            print("The DB_TYPE is SQLSever")
            # print('sqlserver')
            if db_instance != '':
                checkdb.sqlserver_test(db_host, int(db_port), db_user, db_passwd, db_name)
                
            else:
                print('Please input the DB_INStance,like mssqlserver')
                
    else:
        print_self('the config error,please check','red')
        os._exit(0)

# default sys url

# print(base_url)
def init_db_request(db_type,instance,host,dbname,port,passwd,user,ver_code,base_url):
    if db_type=='mysql' or db_type == 'oracle':
        url = base_url + '/api/system/createDB/base/initdb?dbType={0}&dbInstance=&dbIp={1}&dbName={2}&dbPort={3}&password={4}&useCurrentDB=0&username={5}&validCode={6}'\
            .format(db_type,host,dbname,port,passwd,user,ver_code)
    elif db_type=='sqlserver':
        url = base_url + '/api/system/createDB/base/initdb?dbType={0}&dbInstance={1}&dbIp={2}&dbName={3}&dbPort={4}&password={5}&useCurrentDB=0&username={6}&validCode={7}'\
            .format(db_type,instance,host,dbname,port,passwd,user,ver_code)
    else:
        print_self('Sorry,{0} does not support,pleae check!'.format(db_type),'red')
    # print(url)
    return url



# db_url = base_url + '/api/system/createDB/base/initdb?dbType=sqlserver&dbInstance=mssqlserver&dbIp=192.168.81.75&dbName=test3&dbPort=1433&password=scmadmin&useCurrentDB=0&username=sa&validCode=wEAver2018'
def init_db(url):
    try:
        print('Begin to init DB')
        r = s.get(url,timeout=500)
    except BaseException as e:
        print('Init DB Url Error,Exit' + str(e))
        os._exit(0)
        # status_url_check(staturl)

def get_status(percent, status_url):
    # await init_db(db_url)
    # print('Begin to Get the Processes!')
    while percent != '100%':
        retrun_msg = ''
        response = []
        times = 0
        while times < 10:
            try:
                pro = s.get(status_url,timeout=5)
                time.sleep(2)
                # print(pro.text)
                response.append(pro.text)
                times = times + 1
            except BaseException as e:
                print('Connect Error,Please Retry ' + str(e))
                times = times + 1
        if retrun_msg.join(response).find('completePercent') != -1:
            pass
        else:
            print('The Status Url Get Error' +str(set(response)))
            os._exit(0)

        pro = s.get(status_url,timeout=5)
        if (pro.text).find('errormsg') != -1:
            __doc = json.loads(pro.text)
            err_log = __doc['stat']['errormsg']
            msg = __doc['stat']['msg']
            if err_log == '':
                per = __doc['stat']['completePercent']
                # print()
                percent = per
                time.sleep(5)
                if msg.find('Success') != -1:
                    print('The current process percent is 100%!')
                    print_self('\tinit db success,please login !','green')
                else:  
                    print('The current process percent is {0},message {1}'.format(percent,msg))
            else:
                print_self('init error,the reason is {0},the database maybe already exists,please empty the database!'.format(err_log),'red')
                break
                os._exit(0)
        else:
            # await init_db()
            print_self('init error,the database maybe already exists,please empty the database and retry!','red')
            # print('initerror' + pro.text)
            os._exit(0)

def init_db_fin():
    if os.path.exists(db_info_path):
        config = configparser.ConfigParser()
        config.read('db.info')
    else:
        print_self('the db.info config does not esixts,please check','red')
        os._exit(0)
    try:
        cfg_ver_code=config['DATABASE']['Verification_code']
        cfg_db_type=config['DATABASE']['DB_TYPE']
        cfg_host=config['DATABASE']['DB_HOST']
        cfg_port=config['DATABASE']['DB_PORT']
        cfg_db_name=config['DATABASE']['DB_name']
        cfg_db_user=config['DATABASE']['DB_USER']
        cfg_db_passwd=config['DATABASE']['DB_PASSWD']
        cfg_db_instance=config['DATABASE']['DB_INSTANCE']
        cfg_oa_url = config['DATABASE']['OA_URL']
    except BaseException as e:
        print_self('ConfigParse Error,Please Check the db.info','red')
        os._exit(0)
    
    percent = ''
    base_url=cfg_oa_url
    status_url = base_url + '/api/system/createDB/base/getStatus'
    
    db_check(cfg_ver_code,cfg_db_type,cfg_host,cfg_port,cfg_db_name,cfg_db_user,cfg_db_passwd,cfg_db_instance)
    print('''\033[5;31;40m请确认以下要求已满足：
\t数据库是否已执行alter system set "_allow_level_without_connect_by"=true,如果未执行，会导致初始化数据库脚本执行失败。
\t如果为12C容器模式数据库，需要在$ORACLE_HOME/network/admin/sqlnet.ora文件末尾处增加SQLNET.ALLOWED_LOGON_VERSION=8\033[0m''')
    text = input('if you have acquired Please input y else enter other keys to esc:')
    if text == 'y' or text == 'Y':
        pass
        # print('contineue')
    else:
        # print('input error')
        os._exit(0)
    try:
        fin_sta = s.get(base_url,timeout=5)
    except BaseException as e:
        print_self('Access OA_URL Error,please acquire the resin Has been started!','red')
        os._exit(0)
    if fin_sta.status_code != 200:
        print_self('The Resin Access Error,Please Check!','red')
    else:
        print_self('\t The Resin has been started!','green')
    db_url = init_db_request(cfg_db_type,cfg_db_instance,cfg_host,cfg_db_name,cfg_port,cfg_db_passwd,cfg_db_user,cfg_ver_code,base_url)
    p = mp.Process(target=init_db,args=(db_url,))
    p.start()
    # time.sleep(5)
    get_status(percent,status_url)
