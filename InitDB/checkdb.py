import pymysql
import pymssql
import cx_Oracle
import os

def print_self(string,color):
    if color == 'red':
        print('\033[5;31;40m{0}\033[0m'.format(string))
    elif color == 'green':
        print('\033[5;32;40m{0}\033[0m'.format(string))
    else:
        print(string)


def mysql_test(host, port, user, passwd, db):
    try:
        db_connect = pymysql.connect(
            host=host, port=port, user=user, password=passwd, db=db, charset='utf8')
            # return True
    except BaseException as e:
        print_self('The Connection to the DB Error,the reason maybe is {0}'.format(e), 'red')
        os._exit(0)
        # return False
    # cursor = db_connect.cursor()
    # try:
    #     cursor.execute("select * from hrmresourcemanager;")
    # except BaseException as e:
    #     print('The Query is error,the reason maybe is {0}'.format(e),'red')


def sqlserver_test(host, port, user, passwd, db):
    try:
        conn = pymssql.connect(server=host, user=user,
                               password=passwd, database=db)
    except BaseException as e:
        print_self('The Connection to the DB Error,the reason maybe is {0}'.format(e), 'red')
        os._exit(0)
    # cursor = conn.cursor()
    # try:
    #     cursor.execute("select * from hrmresourcemanager;")
    # except BaseException as e:
    #     print('The Query is error,the reason maybe is {0}'.format(e),'red')


def oracle_test(user, passwd, conn_str):
    # conn_str 192.168.7.32:1521/ecology
    ora_error_list = []
    try:
        db = cx_Oracle.connect(user, passwd, conn_str)
        print_self('DataBase Connect Correct','green')
    except BaseException as e:
        print(str(e))
        os._exit(0)
    #     for key in db_error_dic.keys():
    #         if str(e).find(key) != -1:
    #             print_self('{0}, {1}'.format(key, db_error_dic[key]), 'red')
    #             break
    #         else:
    #             ora_error_list.append(str(e))
    # if len(set(ora_error_list)) > 0:
    #     for i in set(ora_error_list):
    #         print(i)

    # cursor = db.cursor()
    # try:
    #     cursor.execute("select * from hrmresourcemanager;")
    # except BaseException as e:
    #     print('The Query is error,the reason maybe is {0}'.format(e),'red')
