#!/bin/bash
#---------------------------------------------------------------------------
# File: linux-syswithoutecology.sh
# Created Date: 2020-11-09
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Monday November 9th 2020 2:53:38 pm
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
#---------------------------------------------------------------------------
#!/bin/bash
#ecology system check
# export LANG=zh_CN.gbk
VERSION="2020.10.28"
echoError(){ echo -e $"\e[43;31;5m  "$1"\e[0m";}
###########check IP addr###########



# redis连接测试时需要用到
function redis_check(){
redis-cli --version &> /dev/null 
if [ $? -ne 0  ];then
    if [ -f ./redis-cli ];then
        chmod +x ./redis-cli
        ln -s "$PWD"/redis-cli /bin/redis-cli
    else
        echoError "Please upload redis-cli to current directory"
        exit 1
    fi
fi
}
# 测试本地是否有hdparm命令，如果没有需要安装
function hdparmtest(){
hdparm -V &> /dev/null 
if [ $? -ne 0 -a ! -f "./hdparm-9.43-4.el6.x86_64.rpm" ];then
    echoError "Please upload hdparm-9.43-4.el6.x86_64.rpm to current directory"
    exit 1
else
    rpm -vih ./hdparm-9.43-4.el6.x86_64.rpm &> /dev/null 
fi
}
tab_top()
{
echo -n -e "+-------------------------------------------------------------------------------------------+\n"
printf "%-1s %-8s %-1s %-16s %-1s %-13s %-1s %-20s %-1s %-20s %-1s\n" \| ID \| Item \| Value \| Std_Value \| other \|
}
tab_mod()
{
echo -n -e "+------------------+----------+---------------+----------------------+----------------------+\n"
printf "%-1s %-8s %-1s %-16s %-1s %-13s %-1s %-20s %-1s %-20s %-1s\n" \| "$1" \| "$2" \| "$3" \| "$4" \| "$5" \|
}
tab_end()
{
echo -n -e "+-------------------------------------------------------------------------------------------+\n"
}

#check system version
el7=$(uname -r | grep -c "el7")
function os_versin(){
if [ -f /etc/redhat-release ];then
    os_versin=$(cat /etc/redhat-release  | grep -P '\d+.' -o | tr -d '\n' | cut -d '.' -f 1-2)
else
    echoError 'This system is not belong Centos series or Red hat series'
    exit 1
fi
tab_top
tab_mod 1 'Os_version' "$os_versin" '7+' 'os_version'
}

# uptime
function cpu(){
cpu_cors=$(lscpu | grep CPU\(s\) | awk 'NR==1{print}' | awk '{print $2}')
# echo $cpu_cors
tab_mod 2 'Cpu_cors' "$cpu_cors" '8+' 'cpu_cors'

if [ $el7 -eq "1" ];then 
    loadav=$(uptime | cut -d ':'  -f 5 | tr  -d ' ' )
    tab_mod 2-1 'uptime' "$loadav" '' 'load_average'
else
    loadav=$(uptime | cut -d ':' -f 4 | tr -d ' ')
    tab_mod 2-1 'uptime' "$loadav" '' 'load_average'
fi
}
#get mem info
function mem(){
mem_format=1048576
mem_total=$(cat /proc/meminfo | grep MemTotal | awk '{print $2}')
mem=$(echo $mem_total $mem_format |awk '{printf ("%.1f\n",$1/$2)}')
useed=$(free -m  | grep Mem |awk '{print $3}' )
totalm=$(free -m  | grep Mem |awk '{print $2}')
usage_percent=$(echo $useed $totalm |awk '{printf ("%.2f\n",$1/$2*100)}')
# echo $usage_percent%
tab_mod 3 'Total_mem' "$mem"G '16G+' 'MemoryTotal'
tab_mod 3-1 'Mem_usage' "$usage_percent"% '<80%' 'MemUse' 
}
#get disk info
function disk_info(){
if [ $el7 -eq "1" ];then 
    disk_use=$(df -h "$oaPath" |awk '{print $5}' | grep -v Use)
    tab_mod 4 'DiskUsagePer' "$disk_use" '<80%' 'Disk_percent'
else
    disk_use=$(df -h "$oaPath" |awk '{print $4}' | grep -v Avail | awk 'NR==2{print}')
    tab_mod 4 'DiskUsagePer' "$disk_use" '<80%' 'Disk_percent'
fi
}

function get_ip(){
    if [ $el7 -eq "1" ];then
	net_name=$(ip addr show | grep BROADCAST | awk -F : '{print $2}' | awk 'NR==1{print}' | sed s/[[:space:]]//g)
	ip=$(ip addr show  | grep inet | grep -v inet6 | grep -v 127.0.0.1 | grep "$net_name"| awk '{print $2}'|cut -d '/' -f 1)
    echo $ip

    else
	net_name=$(ip addr show | grep BROADCAST | awk -F : '{print $2}' | awk 'NR==1{print}' | sed s/[[:space:]]//g)
    $(ip addr show  | grep inet | grep -v inet6 | grep -v 127.0.0.1 | grep "$net_name"| awk '{print $2}'|cut -d '/' -f 1) 
    echo $ip
    fi
}
IPADDR="$(get_ip)"

#check host ip 
function ip_check(){
if [ $el7 -eq "1" ];then
	net_name=$(ip addr show | grep BROADCAST | awk -F : '{print $2}' | awk 'NR==1{print}' | sed s/[[:space:]]//g)
	ip=$(ip addr show  | grep inet | grep -v inet6 | grep -v 127.0.0.1 | grep "$net_name"| awk '{print $2}'|cut -d '/' -f 1)
	hostname=$(hostname -i)
	tab_mod 5 'hostnameParsing' "$hostname" "$ip" 'Host_ip'
else
	net_name=$(ip addr show | grep BROADCAST | awk -F : '{print $2}' | awk 'NR==1{print}' | sed s/[[:space:]]//g)
	ip=$(ip addr show  | grep inet | grep -v inet6 | grep -v 127.0.0.1 | grep "$net_name"| awk '{print $2}'|cut -d '/' -f 1)
    host=$(hostname -i  >/dev/null 2>&1 )
    if [ $? -ne 0 ];then
        tab_mod 5 'hostnameParsing' "Parse_Error" "$ip" 'Host_ip'
	else
        tab_mod 5 'hostnameParsing' "$host" "$ip" 'Host_ip'
    fi
	
fi
}

function file_jdk(){
open_files=$(ulimit -a | grep open | awk '{print $4}')
tab_mod 6 'OpenFiles' "$open_files" 65535 'Files'

jdk_version=$("$jdkPath"/bin/java -version 2>&1 | grep version | awk '{print $3}' | tr -d '"')
tab_mod 7 Jdk_version "$jdk_version" '1.8+' '>1.8'
}
# 获取Resin相关信息
function resin_info(){
if [ ! -f "$resinPath/conf/resin.properties" ];then
    Xmx=$(grep Xmx $resinPath/conf/resin.conf | tr -d ' ' | cut -c 14-18)
    thread_max=$(grep thread-max "$resinPath"/conf/resin.conf | tr  -d [:alpha:] | tr -d [:punct:] | tr -d ' ' | tr -d '\n' | tr -d '\r')
	keepalivemax=$(grep keepalive-max "$resinPath"/conf/resin.conf | tr  -d [:alpha:] | tr -d [:punct:] | tr -d ' '|tr -d '\n' | tr -d '\r')
	tab_mod 8 thread_max "$thread_max" "3000" 'thread-max'
    tab_mod 9 keepalivemax "$keepalivemax" "500" 'keepalivemax'
    tab_mod 10 Xmx_mem "$Xmx" '5500M' '>5000M'
    port=$(grep app.http "$resinPath"/conf/resin.conf | awk 'NR==1{print}' | awk -F:  '{print $2}'| tr -d ' '| tr -d '\n' | tr -d '\r')
    # | tr -d '\n' | tr -d '\r'
 else
	Xmx=$(grep Xmx $resinPath/conf/resin.properties | awk '{print $3}' | cut -c 5-)
	port_thread_max=$(grep port_thread_max < "$resinPath"/conf/resin.properties |awk -F : '{print $2}' | tr -d ' ' | tr -d '\n' | tr -d '\r')
	tab_mod 8 Port_thread "$port_thread_max" "3000" 'Port_thread'
	accept_thread_max=$(grep accept_thread_max < "$resinPath"/conf/resin.properties |awk -F : '{print $2}'| tr -d ' ' | tr -d '\n' | tr -d '\r')
    port=$(grep app.http "$resinPath"/conf/resin.properties | awk 'NR==1{print}' | awk -F:  '{print $2}' |  tr -d ' '| tr -d '\n' | tr -d '\r')
	tab_mod 9 Accept_thread "$accept_thread_max" "3000" 'Accept_thread'
	tab_mod 10 Xmx_mem "$Xmx" '5500M' '>5000M'
fi
}

# 测试磁盘速度
# if [ $el7 -eq "1" ];then
#     disk=$(df -h "$oaPath" | awk '{print $1}' | grep -v Filesystem |grep -v 文件系统)
# else
#     disk=$(df -h "$oaPath" | awk '{print $1}' | grep -v Filesystem | grep -v 文件系统| awk 'NR==1{print $1}')
# fi
# disk_speed=$( hdparm -Tt "$disk" | grep Timing | awk -F "=" '{print $2 }')
# diskcache_read=$(echo "$disk_speed" | awk 'NR==1{print}' | tr -d ' ')
# diskbuffer_read=$(echo "$disk_speed" | awk 'NR==2{print}' |tr -d ' ')
# tab_mod 11 diskcache_read "$diskcache_read" '5500M/sec' '>5000'
# tab_mod 12 diskbuffer_read "$diskbuffer_read" '5500M/sec' '>5000'

# 检查数据库最大连接数
function db_conn(){
if [ -f "$oaPath"/WEB-INF/prop/weaver.properties ];then
    maxconn=$(grep  maxconn  "$oaPath"/WEB-INF/prop/weaver.properties | cut -d "=" -f 2)
    tab_mod 13 DB_max_conn "$maxconn" '500' 'DB_Connections'
else
    tab_mod 13 DB_max_conn "NotFound" '500' 'DB_Connections'
fi

# 检查是否开启数据库缓存
if [ -f "$oaPath"/WEB-INF/prop/initCache.properties ];then
    iscache=$(grep "iscache" "$oaPath"/WEB-INF/prop/initCache.properties | sed -e 's/\r//g' | grep -v "#" | grep -v 'update' )
    tab_mod 14 Is_cache_check  "$iscache" 'Is_cache=1' 'Is_cache=1'
else
    tab_mod 14 Is_cache_check  "NotFound" 'Is_cache=1' 'Is_cache=1'
fi
}
#redis check


function redis_connect(){
if [ -f "$oaPath"/WEB-INF/prop/weaver_new_session.properties ];then
    redis_info=$(grep  "^redis*" "$oaPath"/WEB-INF/prop/weaver_new_session.properties | awk -F '=' '{print $2}')
    redis_host=$(echo "$redis_info" | awk 'NR==1{print}' | tr -d '\r')
    redis_port=$(echo "$redis_info" | awk 'NR==2{print}' | tr -d '\r')
    redis_passwd=$(echo "$redis_info" | awk 'NR==3{print}' | tr -d '\r')
    timeout 5 ./redis-cli -h "$redis_host" -p "$redis_port" -a "$redis_passwd" info >/dev/null 2>&1 >/dev/null
    redis_result=$?
    if [ $redis_result = 0 ];then
        tab_mod 15 redis_check $redis_result 'Connect_Success'  'Redis_conn'
    else
        tab_mod 15 redis_check $redis_result 'Connect_Error'  'Redis_conn'
    fi
else
    # echoError "$oaPath/WEB-INF/prop/weaver_new_session.properties  不存在!"
    tab_mod 15 redis_check 'NotFound' 'Connect_Error'  'Redis_conn'
fi
}

function weaver_info(){
if [ -f "$oaPath"/prop/weaver_new_session.properties ];then
	WSession=$(grep WSessionClusterFilter  "$oaPath"/WEB-INF/prop/weaver_new_session.properties)
	tab_mod 16 WSession "${WSession:-NotFound}" 'Not_Null' 'Wsession'
else
	tab_mod 16 WSession "Not_foundfile" 'Not_Null' 'Wsession'
fi


# echoGreen "==============检测web.xml是否有启动时的编译配置============="
Precompile=$(grep  "com.caucho.jsp.JspPrecompileListener" "$oaPath"/WEB-INF/web.xml)
tab_mod 17 Precompile "${Precompile:-NotOpen}" 'Open' 'check_Precom'

if [ ! -f "$oaPath/WEB-INF/prop/doc_full_search.properties" ];then
	# echoError "$oaPath/WEB-INF/prop/doc_full_search.properties 不存在!"
	tab_mod 18 doc_search 'NotFound' 'Open' 'doc_search_open'
 else
	use_full_search=$(grep use_full_search "$oaPath"/WEB-INF/prop/doc_full_search.properties | sed -e 's/\r//g'|cut -d '='  -f 2 )
	tab_mod 18 doc_search "$use_full_search" '1' "1-Open*-Error"
fi
}
# echoGreen "==============检查防火墙是否开启============="
function firewall(){
if [ $el7 -eq "1" ];then
	firewall_status=$(systemctl status firewalld |grep  "dead" )
	if [[ -z ${firewall_status} ]];then
		tab_mod 19 firewall_stat "Open" 'Dead' "Close_Firewall"
	# echoError "防火墙未关闭,请使用systemctl stop firewalld && systemctl disable firewalld关闭防火墙"
	else
		tab_mod 19 firewall_stat "Dead" 'Dead' "Close_Firewall"
	# echoGreen "防火墙检测通过"
	fi
else
	firewall_status=$(service iptables status |grep -c "not running")
    if [ "$firewall_status" -eq "1" ];then
        tab_mod 19 firewall_stat "Dead" 'Dead' "Close_Firewall"
    else 
        tab_mod 19 firewall_stat "Open" 'Dead' "Close_Firewall"
    fi
fi
}
# if [ $el7 -eq "1" ];then 
#     root_disk_use=$(df -h / |awk '{print $5}' | grep -v Use)
#     tab_mod 20 'Root_Disk' "$root_disk_use" '<80%' 'Disk_percent'
# else
#     root_disk_use=$(df -h / |awk '{print $4}' | grep -v Avail | awk 'NR==2{print}')
#     tab_mod 20 'Root_Disk' "$root_disk_use" '<80%' 'Disk_percent'
# fi

tab_end

# 检测安全包配置顺序
function check_web_xml_sequence(){
echo Start checking the order of the filters in the configuration file web.xml
echo ''
xmllint -version &> /dev/null  || yum install libxml2-2.9.1-6.el7.4.x86_64
test=$(echo "cat //filter/filter-name" |xmllint --shell --noblanks "$oaPath"/WEB-INF/web.xml |grep "^<filter-name>"  | sed -e 's/<filter-name>//g' | sed -e 's/<\/filter-name>//g')
echo  "$test" | tr ' ' '\n' > temp.log
total_lines=$(wc -l temp.log  | awk '{print $1}')
# echo $test
# echo $test
num=1
WS='WSessionClusterFilter'
SEC='SecurityFilter'
SOC='SocialIMFilter'
EMF='EMFilter'
SESS='SessionCloudFilter'
MUL='MultiLangFilter'
WGZ='WGzipFilter'
for i in $test
do 
    if [ "$i" = "$WS" ];then
        first=$(awk 'NR==1{print}' temp.log)
        second=$(awk 'NR==2{print}' temp.log)
        if  [ "$first"  = "$WS" ] && [ "$second" = "$SEC" ] ;then
            printf "%-20s  %20s\n" "$WS" check_passed
            true
        else
            echo "WSessionClusterFilter Sequence Error"
            # exit 1
        fi
    elif  [ "$i" = "$SOC" ];then 
        line_num=$(grep -n "SocialIMFilter" ./temp.log |cut -d ':' -f 1)
        # result_line=`expr $line_num - $num`
        soczip=$(sed -n "1,$line_num"p ./temp.log | grep -E  'SecurityFilter' )
        if [  -z $soczip  ];then
           printf "%-20s  %20s\n" "$SOC" check_passed
        else
            echo 'SocialIMFilter not in before SecurityFilter,Sequence Error'
            # exit 1       
        fi
    elif [ "$i" = "$EMF" ];then
        line_num=$(grep -n "EMFilter" ./temp.log |cut -d ':' -f 1)
        grep_result=$(sed -n "$line_num,$total_lines"p ./temp.log | grep  "SecurityFilter" )
        if [ -z "$grep_result" ];then
            echo 'Securiry not in after EMFilter'
            # exit 1
        else
            printf "%-20s  %20s\n" "$EMF" check_passed
        fi
    elif [ "$i" = $SESS ];then
        line_num=$(grep -n "SessionCloudFilter" ./temp.log |cut -d ':' -f 1)
        grep_result=$(sed -n "$line_num,$total_lines"p ./temp.log | grep  "SecurityFilter" )
        if [ -z "$grep_result" ];then
            echo 'Securiry not in after SessionCloudFilter'
            # exit 1
           
        else
             printf "%-20s  %20s\n" "$SESS " check_passed
        fi
    elif [ "$i" = "$MUL" ];then
        line_num=$(grep -n "MultiLangFilter" ./temp.log |cut -d ':' -f 1)
        zip=$(sed -n "1,$line_num"p ./temp.log | grep -E  'WGzipFilter|MobileFilter' |wc -l )
        after_zip=$(sed -n "$line_num,$total_lines"p ./temp.log | grep "DialogHandleFilter"  |wc -l)
        if [ $zip -ge 1 -a $after_zip -ge 1 ];then
            printf "%-20s  %20s\n" "$MUL" check_passed
        else
            echo 'MultiLangFilter Sequence Error PlaseCheck'
            # exit 1
        fi
    elif [ "$i" = "$WGZ" ];then
        line_num=$(grep -n "WGzipFilter" ./temp.log |cut -d ':' -f 1)
        before_num=`expr $line_num - $num`
        before_str=$(sed -n "$before_num"p ./temp.log)
        if [ "$before_str" = "$SEC" ];then
            printf "%-20s  %20s\n" "$WGZ " check_passed
        else
            echo 'WGzipFilter Sequence Error,Please Check'
            # exit 1
        fi
    else
       printf "%-20s  %20s\n" "$i" check_passed
    fi
done
}


# 根据关键字解json文件，像Python字典 
parse_json(){
echo "${1//\"/}" | sed "s/.*$2:\([^,}]*\).*/\1/"
}


# 获取安全包相关配置
access_secur(){
    echo ''
    echo Start checking the Security Url 
    echo ''
    url='http://127.0.0.1:'$port"/security/monitor/MonitorStatusForServer.jsp"
    echo $url
    resutl=$(curl -s  $url > test.txt )
    if [ $? != 0 ];then
        echo 'Access Security page error,please insure the resin has been started! '
        exit 1
    fi
    fin=$(grep -v '^$' test.txt | tr -d '' |tr -d '\r')
    # tr -d '\r'  返回的接口信息中有回车， 需要删掉，不然解析会失败
    if [ -e AESCoder.jar ];then
        if [ -n $fin ];then
            # 测试信息
            # returnMsg="{sysid:6C3E226B4D4795D518AB341B0824EC29,companyName:E9TEST,ecVersion:9.00.2002.02,firewallStatus:true,autoUpdateStatus:true,softwareVersion:v10.29,ruleVersion:v10.10,loginStatus:true,pageStatus:true,dataStatus:true,enableServiceCheck:true,isUseESAPISQL:true,isUseESAPIXSS:true,isResetCookie:false,httpOnly:true,hostStatus:false,isRefAll:false,httpSep:true,isCheckSessionTimeout:true,isEnableForbiddenIp:false,autoRemind:true,isConfigFirewall:true,isEnableAccessLog:false,checkSocketTimeout:false,isResinAdmin:true,is404PageConfig:true,is500PageConfig:true,isDisabledHttpMethod:true,joinSystemSecurity:true,isWlanNetwork:true}"
            returnMsg=$($jdkPath/bin/java -jar ./AESCoder.jar $fin | tr -d ' ')
            # 返回的消息里面带有空格，需要去掉，必然解析会有错误
            # echo $returnMsg
            firewallstatus=$(parse_json $returnMsg "firewallStatus")
            AutoUpdateStatus=$(parse_json $returnMsg "autoUpdateStatus")
            SoftwareVersion=$(parse_json $returnMsg "softwareVersion")
            remote_stat=$(echo $returnMsg | grep remoteVersion | wc -l)
            # echo $remote_stat
            if [ $remote_stat -gt 0 ];then
                RemoteVersion=$(parse_json $returnMsg "remoteVersion")
            else
                RemoteVersion='not found'
            fi
            echo firewallstatus: $firewallstatus
            echo AutoUpdateStatus: $AutoUpdateStatus
            echo SoftwareVersion: $SoftwareVersion
            echo RemoteVersion: $RemoteVersion
        else
            echo 'Decrypt Error,Plase Check!'
        fi
    else
        echo 'AESCoder.jar Could not find in this dictory ,Please Check!'
    fi
}


export PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin
source /etc/profile
  
[ $(id -u) -gt 0 ] && echo "请用root用户执行此脚本！" && exit 1
centosVersion=$(awk '{print $(NF-1)}' /etc/redhat-release)

#日志相关
IPADDR="$(get_ip)"
PROGPATH=`echo $0 | sed -e 's,[\\/][^\\/][^\\/]*$,,'`
[ -f $PROGPATH ] && PROGPATH="."
LOGPATH="$PROGPATH/log"
[ -e $LOGPATH ] || mkdir $LOGPATH
RESULTFILE="Inspection-$IPADDR-`date +%Y%m%d`.txt"
  
  
#定义报表的全局变量
report_DateTime=""    #日期 ok
report_Hostname=""    #主机名 ok
report_OSRelease=""    #发行版本 ok
report_Kernel=""    #内核 ok
report_Language=""    #语言/编码 ok
report_LastReboot=""    #最近启动时间 ok
report_Uptime=""    #运行时间（天） ok
report_CPUs=""    #CPU数量 ok
report_CPUType=""    #CPU类型 ok
report_Arch=""    #CPU架构 ok
report_MemTotal=""    #内存总容量(MB) ok
report_MemFree=""    #内存剩余(MB) ok
report_MemUsedPercent=""    #内存使用率% ok
report_DiskTotal=""    #硬盘总容量(GB) ok
report_DiskFree=""    #硬盘剩余(GB) ok
report_DiskUsedPercent=""    #硬盘使用率% ok
report_InodeTotal=""    #Inode总量 ok
report_InodeFree=""    #Inode剩余 ok
report_InodeUsedPercent=""    #Inode使用率 ok
report_IP=""    #IP地址 ok
report_MAC=""    #MAC地址 ok
report_Gateway=""    #默认网关 ok
report_DNS=""    #DNS ok
report_Listen=""    #监听 ok
report_Selinux=""    #Selinux ok
report_Firewall=""    #防火墙 ok
report_USERs=""    #用户 ok
report_USEREmptyPassword=""   #空密码用户 ok
report_USERTheSameUID=""      #相同ID的用户 ok 
report_PasswordExpiry=""    #密码过期（天） ok
report_RootUser=""    #root用户 ok
report_Sudoers=""    #sudo授权  ok
report_SSHAuthorized=""    #SSH信任主机 ok
report_SSHDProtocolVersion=""    #SSH协议版本 ok
report_SSHDPermitRootLogin=""    #允许root远程登录 ok
report_DefunctProsess=""    #僵尸进程数量 ok
report_SelfInitiatedService=""    #自启动服务数量 ok
report_SelfInitiatedProgram=""    #自启动程序数量 ok
report_RuningService=""           #运行中服务数  ok
report_Crontab=""    #计划任务数 ok
report_Syslog=""    #日志服务 ok
report_SNMP=""    #SNMP  OK
report_NTP=""    #NTP ok
report_JDK=""    #JDK版本 ok
function version(){
    echo ""
    echo ""
    echo "系统巡检脚本：Version $VERSION"
}
  
function getCpuStatus(){
    echo ""
    echo ""
    echo "############################ CPU检查 #############################"
    Physical_CPUs=$(grep "physical id" /proc/cpuinfo| sort | uniq | wc -l)
    Virt_CPUs=$(grep "processor" /proc/cpuinfo | wc -l)
    CPU_Kernels=$(grep "cores" /proc/cpuinfo|uniq| awk -F ': ' '{print $2}')
    CPU_Type=$(grep "model name" /proc/cpuinfo | awk -F ': ' '{print $2}' | sort | uniq)
    CPU_Arch=$(uname -m)
    echo "物理CPU个数:$Physical_CPUs"
    echo "逻辑CPU个数:$Virt_CPUs"
    echo "每CPU核心数:$CPU_Kernels"
    echo "    CPU型号:$CPU_Type"
    echo "    CPU架构:$CPU_Arch"
    #报表信息
    report_CPUs=$Virt_CPUs    #CPU数量
    report_CPUType=$CPU_Type  #CPU类型
    report_Arch=$CPU_Arch     #CPU架构
}
  
function getMemStatus(){
    echo ""
    echo ""
    echo "############################ 内存检查 ############################"
    if [ $el7 -eq "1" ];then
        free -h
    else
        free -mo
    fi
    #报表信息
    MemTotal=$(grep MemTotal /proc/meminfo| awk '{print $2}')  #KB
    MemFree=$(grep MemFree /proc/meminfo| awk '{print $2}')    #KB
    let MemUsed=MemTotal-MemFree
    MemPercent=$(awk "BEGIN {if($MemTotal==0){printf 100}else{printf \"%.2f\",$MemUsed*100/$MemTotal}}")
    report_MemTotal="$((MemTotal/1024))""MB"        #内存总容量(MB)
    report_MemFree="$((MemFree/1024))""MB"          #内存剩余(MB)
    report_MemUsedPercent="$(awk "BEGIN {if($MemTotal==0){printf 100}else{printf \"%.2f\",$MemUsed*100/$MemTotal}}")""%"   #内存使用率%
}
  
function getDiskStatus(){
    echo ""
    echo ""
    echo "############################ 磁盘检查 ############################"
    df -hiP | sed 's/Mounted on/Mounted/'> /tmp/inode
    df -hTP | sed 's/Mounted on/Mounted/'> /tmp/disk 
    join /tmp/disk /tmp/inode | awk '{print $1,$2,"|",$3,$4,$5,$6,"|",$8,$9,$10,$11,"|",$12}'| column -t
    #报表信息
    diskdata=$(df -TP | sed '1d' | awk '$2!="tmpfs"{print}') #KB
    disktotal=$(echo "$diskdata" | awk '{total+=$3}END{print total}') #KB
    diskused=$(echo "$diskdata" | awk '{total+=$4}END{print total}')  #KB
    diskfree=$((disktotal-diskused)) #KB
    diskusedpercent=$(echo $disktotal $diskused | awk '{if($1==0){printf 100}else{printf "%.2f",$2*100/$1}}') 
    inodedata=$(df -iTP | sed '1d' | awk '$2!="tmpfs"{print}')
    inodetotal=$(echo "$inodedata" | awk '{total+=$3}END{print total}')
    inodeused=$(echo "$inodedata" | awk '{total+=$4}END{print total}')
    inodefree=$((inodetotal-inodeused))
    inodeusedpercent=$(echo $inodetotal $inodeused | awk '{if($1==0){printf 100}else{printf "%.2f",$2*100/$1}}')
    report_DiskTotal=$((disktotal/1024/1024))"GB"   #硬盘总容量(GB)
    report_DiskFree=$((diskfree/1024/1024))"GB"     #硬盘剩余(GB)
    report_DiskUsedPercent="$diskusedpercent""%"    #硬盘使用率%
    report_InodeTotal=$((inodetotal/1000))"K"       #Inode总量
    report_InodeFree=$((inodefree/1000))"K"         #Inode剩余
    report_InodeUsedPercent="$inodeusedpercent""%"  #Inode使用率%
  
}
  
function getSystemStatus(){
    echo ""
    echo ""
    echo "############################ 系统检查 ############################"
    if [ -e /etc/sysconfig/i18n ];then
        default_LANG="$(grep "LANG=" /etc/sysconfig/i18n | grep -v "^#" | awk -F '"' '{print $2}')"
    else
        default_LANG=$LANG
    fi
    export LANG="en_US.UTF-8"
    Release=$(cat /etc/redhat-release 2>/dev/null)
    Kernel=$(uname -r)
    OS=$(uname -o)
    Hostname=$(uname -n)
    SELinux=$(/usr/sbin/sestatus | grep "SELinux status: " | awk '{print $3}')
    LastReboot=$(who -b | awk '{print $3,$4}')
    uptime=$(uptime | sed 's/.*up \([^,]*\), .*/\1/')
    echo "     系统：$OS"
    echo " 发行版本：$Release"
    echo "     内核：$Kernel"
    echo "   主机名：$Hostname"
    echo "  SELinux：$SELinux"
    echo "语言/编码：$default_LANG"
    echo " 当前时间：$(date +'%F %T')"
    echo " 最后启动：$LastReboot"
    echo " 运行时间：$uptime"
    #报表信息
    report_DateTime=$(date +"%F %T")  #日期
    report_Hostname="$Hostname"       #主机名
    report_OSRelease="$Release"       #发行版本
    report_Kernel="$Kernel"           #内核
    report_Language="$default_LANG"   #语言/编码
    report_LastReboot="$LastReboot"   #最近启动时间
    report_Uptime="$uptime"           #运行时间（天）
    report_Selinux="$SELinux"
    export LANG="$default_LANG"
  
}
  
function getServiceStatus(){
    echo ""
    echo ""
    echo "############################ 服务检查 ############################"
    echo ""
    if [ $el7 -eq "1" ];then
        conf=$(systemctl list-unit-files --type=service --state=enabled --no-pager | grep "enabled")
        process=$(systemctl list-units --type=service --state=running --no-pager | grep ".service")
        #报表信息
        report_SelfInitiatedService="$(echo "$conf" | wc -l)"       #自启动服务数量
        report_RuningService="$(echo "$process" | wc -l)"           #运行中服务数量
    else
        conf=$(/sbin/chkconfig | grep -E ":on|:启用")
        process=$(/sbin/service --status-all 2>/dev/null | grep -E "is running|正在运行")
        #报表信息
        report_SelfInitiatedService="$(echo "$conf" | wc -l)"       #自启动服务数量
        report_RuningService="$(echo "$process" | wc -l)"           #运行中服务数量
    fi
    echo "服务配置"
    echo "--------"
    echo "$conf"  | column -t
    echo ""
    echo "正在运行的服务"
    echo "--------------"
    echo "$process"
  
}
  
  
function getAutoStartStatus(){
    echo ""
    echo ""
    echo "############################ 自启动检查 ##########################"
    conf=$(grep -v "^#" /etc/rc.d/rc.local| sed '/^$/d')
    echo "$conf"
    #报表信息
    report_SelfInitiatedProgram="$(echo $conf | wc -l)"    #自启动程序数量
}
  
function getLoginStatus(){
    echo ""
    echo ""
    echo "############################ 登录检查 ############################"
    last | head
}
  
function getNetworkStatus(){
    echo ""
    echo ""
    echo "############################ 网络检查 ############################"
    if [ $el7 -eq "1" ];then
        for i in $(ip link | grep BROADCAST | awk -F: '{print $2}');do ip add show $i | grep -E "BROADCAST|global"| awk '{print $2}' | tr '\n' ' ' ;echo "" ;done
        
    else
        /sbin/ifconfig -a | grep -v packets | grep -v collisions | grep -v inet6
    fi
    GATEWAY=$(ip route | grep default | awk '{print $3}')
    DNS=$(grep nameserver /etc/resolv.conf| grep -v "#" | awk '{print $2}' | tr '\n' ',' | sed 's/,$//')
    echo ""
    echo "网关：$GATEWAY "
    echo " DNS：$DNS"
    #报表信息
    IP=$(ip -f inet addr | grep -v 127.0.0.1 |  grep inet | awk '{print $NF,$2}' | tr '\n' ',' | sed 's/,$//')
    MAC=$(ip link | grep -v "LOOPBACK\|loopback" | awk '{print $2}' | sed 'N;s/\n//' | tr '\n' ',' | sed 's/,$//')
    report_IP="$IP"            #IP地址
    report_MAC=$MAC            #MAC地址
    report_Gateway="$GATEWAY"  #默认网关
    report_DNS="$DNS"          #DNS
}
  
function getListenStatus(){
    echo ""
    echo ""
    echo "############################ 监听检查 ############################"
    TCPListen=$(ss -ntul | column -t)
    echo "$TCPListen"
    #报表信息
    report_Listen="$(echo "$TCPListen"| sed '1d' | awk '/tcp/ {print $5}' | awk -F: '{print $NF}' | sort | uniq | wc -l)"
}
  
function getCronStatus(){
    echo ""
    echo ""
    echo "############################ 计划任务检查 ########################"
    Crontab=0
    for shell in $(grep -v "/sbin/nologin" /etc/shells);do
        for user in $(grep "$shell" /etc/passwd| awk -F: '{print $1}');do
            crontab -l -u $user >/dev/null 2>&1
            status=$?
            if [ $status -eq 0 ];then
                echo "$user"
                echo "--------"
                crontab -l -u $user
                let Crontab=Crontab+$(crontab -l -u $user | wc -l)
                echo ""
            fi
        done
    done
    #计划任务
    find /etc/cron* -type f | xargs -i ls -l {} | column  -t
    let Crontab=Crontab+$(find /etc/cron* -type f | wc -l)
    #报表信息
    report_Crontab="$Crontab"    #计划任务数
}
function getHowLongAgo(){
    # 计算一个时间戳离现在有多久了
    datetime="$*"
    [ -z "$datetime" ] && echo "错误的参数：getHowLongAgo() $*"
    Timestamp=$(date +%s -d "$datetime")    #转化为时间戳
    Now_Timestamp=$(date +%s)
    Difference_Timestamp=$(($Now_Timestamp-$Timestamp))
    days=0;hours=0;minutes=0;
    sec_in_day=$((60*60*24));
    sec_in_hour=$((60*60));
    sec_in_minute=60
    while (( $(($Difference_Timestamp-$sec_in_day)) > 1 ))
    do
        let Difference_Timestamp=Difference_Timestamp-sec_in_day
        let days++
    done
    while (( $(($Difference_Timestamp-$sec_in_hour)) > 1 ))
    do
        let Difference_Timestamp=Difference_Timestamp-sec_in_hour
        let hours++
    done
    echo "$days 天 $hours 小时前"
}
  
function getUserLastLogin(){
    # 获取用户最近一次登录的时间，含年份
    # 很遗憾last命令不支持显示年份，只有"last -t YYYYMMDDHHMMSS"表示某个时间之间的登录，我
    # 们只能用最笨的方法了，对比今天之前和今年元旦之前（或者去年之前和前年之前……）某个用户
    # 登录次数，如果登录统计次数有变化，则说明最近一次登录是今年。
    username=$1
    : ${username:="`whoami`"}
    thisYear=$(date +%Y)
    oldesYear=$(last | tail -n1 | awk '{print $NF}')
    while(( $thisYear >= $oldesYear));do
        loginBeforeToday=$(last $username | grep $username | wc -l)
        loginBeforeNewYearsDayOfThisYear=$(last $username -t $thisYear"0101000000" | grep $username | wc -l)
        if [ $loginBeforeToday -eq 0 ];then
            echo "从未登录过"
            break
        elif [ $loginBeforeToday -gt $loginBeforeNewYearsDayOfThisYear ];then
            lastDateTime=$(last -i $username | head -n1 | awk '{for(i=4;i<(NF-2);i++)printf"%s ",$i}')" $thisYear" #格式如: Sat Nov 2 20:33 2015
            lastDateTime=$(date "+%Y-%m-%d %H:%M:%S" -d "$lastDateTime")
            echo "$lastDateTime"
            break
        else
            thisYear=$((thisYear-1))
        fi
    done
  
}
  
function getUserStatus(){
    echo ""
    echo ""
    echo "############################ 用户检查 ############################"
    #/etc/passwd 最后修改时间
    pwdfile="$(cat /etc/passwd)"
    Modify=$(stat /etc/passwd | grep Modify | tr '.' ' ' | awk '{print $2,$3}')
  
    echo "/etc/passwd 最后修改时间：$Modify ($(getHowLongAgo $Modify))"
    echo ""
    echo "特权用户"
    echo "--------"
    RootUser=""
    for user in $(echo "$pwdfile" | awk -F: '{print $1}');do
        if [ $(id -u $user) -eq 0 ];then
            echo "$user"
            RootUser="$RootUser,$user"
        fi
    done
    echo ""
    echo "用户列表"
    echo "--------"
    USERs=0
    echo "$(
    echo "用户名 UID GID HOME SHELL 最后一次登录"
    for shell in $(grep -v "/sbin/nologin" /etc/shells);do
        for username in $(grep "$shell" /etc/passwd| awk -F: '{print $1}');do
            userLastLogin="$(getUserLastLogin $username)"
            echo "$pwdfile" | grep -w "$username" |grep -w "$shell"| awk -F: -v lastlogin="$(echo "$userLastLogin" | tr ' ' '_')" '{print $1,$3,$4,$6,$7,lastlogin}'
        done
        let USERs=USERs+$(echo "$pwdfile" | grep "$shell"| wc -l)
    done
    )" | column -t
    echo ""
    echo "空密码用户"
    echo "----------"
    USEREmptyPassword=""
    for shell in $(grep -v "/sbin/nologin" /etc/shells);do
            for user in $(echo "$pwdfile" | grep "$shell" | cut -d: -f1);do
            r=$(awk -F: '$2=="!!"{print $1}' /etc/shadow | grep -w $user)
            if [ ! -z $r ];then
                echo $r
                USEREmptyPassword="$USEREmptyPassword,"$r
            fi
        done    
    done
    echo ""
    echo "相同ID的用户"
    echo "------------"
    USERTheSameUID=""
    UIDs=$(cut -d: -f3 /etc/passwd | sort | uniq -c | awk '$1>1{print $2}')
    for uid in $UIDs;do
        echo -n "$uid";
        USERTheSameUID="$uid"
        r=$(awk -F: 'ORS="";$3=='"$uid"'{print ":",$1}' /etc/passwd)
        echo "$r"
        echo ""
        USERTheSameUID="$USERTheSameUID $r,"
    done
    #报表信息
    report_USERs="$USERs"    #用户
    report_USEREmptyPassword=$(echo $USEREmptyPassword | sed 's/^,//') 
    report_USERTheSameUID=$(echo $USERTheSameUID | sed 's/,$//') 
    report_RootUser=$(echo $RootUser | sed 's/^,//')    #特权用户
}
  
  
function getPasswordStatus {
    echo ""
    echo ""
    echo "############################ 密码检查 ############################"
    pwdfile="$(cat /etc/passwd)"
    echo ""
    echo "密码过期检查"
    echo "------------"
    result=""
    for shell in $(grep -v "/sbin/nologin" /etc/shells);do
        for user in $(echo "$pwdfile" | grep "$shell" | cut -d: -f1);do
            get_expiry_date=$(/usr/bin/chage -l $user | grep 'Password expires' | cut -d: -f2)
            if [[ $get_expiry_date = ' never' || $get_expiry_date = 'never' ]];then
                printf "%-15s 永不过期\n" $user
                result="$result,$user:never"
            else
                password_expiry_date=$(date -d "$get_expiry_date" "+%s")
                current_date=$(date "+%s")
                diff=$(($password_expiry_date-$current_date))
                let DAYS=$(($diff/(60*60*24)))
                printf "%-15s %s天后过期\n" $user $DAYS
                result="$result,$user:$DAYS days"
            fi
        done
    done
    report_PasswordExpiry=$(echo $result | sed 's/^,//')
  
    echo ""
    echo "密码策略检查"
    echo "------------"
    grep -v "#" /etc/login.defs | grep -E "PASS_MAX_DAYS|PASS_MIN_DAYS|PASS_MIN_LEN|PASS_WARN_AGE"
  
  
}
  
function getSudoersStatus(){
    echo ""
    echo ""
    echo "############################ Sudoers检查 #########################"
    conf=$(grep -v "^#" /etc/sudoers| grep -v "^Defaults" | sed '/^$/d')
    echo "$conf"
    echo ""
    #报表信息
    report_Sudoers="$(echo $conf | wc -l)"
}
  
function getInstalledStatus(){
    echo ""
    echo ""
    echo "############################ 软件检查 ############################"
    rpm -qa --last | head | column -t 
}
  
function getProcessStatus(){
    echo ""
    echo ""
    echo "############################ 进程检查 ############################"
    if [ $(ps -ef | grep defunct | grep -v grep | wc -l) -ge 1 ];then
        echo ""
        echo "僵尸进程";
        echo "--------"
        ps -ef | head -n1
        ps -ef | grep defunct | grep -v grep
    fi
    echo ""
    echo "内存占用TOP10"
    echo "-------------"
    echo -e "PID %MEM RSS COMMAND
    $(ps aux | awk '{print $2, $4, $6, $11}' | sort -k3rn | head -n 10 )"| column -t 
    echo ""
    echo "CPU占用TOP10"
    echo "------------"
    top b -n1 | head -17 | tail -11
    #报表信息
    report_DefunctProsess="$(ps -ef | grep defunct | grep -v grep|wc -l)"
}
  
function getJDKStatus(){
    echo ""
    echo ""
    echo "############################ JDK检查 #############################"
    java -version 2>/dev/null
    if [ $? -eq 0 ];then
        java -version 2>&1
    fi
    echo "JAVA_HOME=\"$JAVA_HOME\""
    #报表信息
    report_JDK="$(java -version 2>&1 | grep version | awk '{print $1,$3}' | tr -d '"')"
}
function getSyslogStatus(){
    echo ""
    echo ""
    echo "############################ syslog检查 ##########################"
    echo "服务状态：$(getState rsyslog)"
    echo ""
    echo "/etc/rsyslog.conf"
    echo "-----------------"
    cat /etc/rsyslog.conf 2>/dev/null | grep -v "^#" | grep -v "^\\$" | sed '/^$/d'  | column -t
    #报表信息
    report_Syslog="$(getState rsyslog)"
}
function getFirewallStatus(){
    echo ""
    echo ""
    echo "############################ 防火墙检查 ##########################"
    #防火墙状态，策略等
    if [ $el7 -ne "1" ];then
        /etc/init.d/iptables status >/dev/null  2>&1
        status=$?
        if [ $status -eq 0 ];then
                s="active"
        elif [ $status -eq 3 ];then
                s="inactive"
        elif [ $status -eq 4 ];then
                s="permission denied"
        else
                s="unknown"
        fi
    else
        s="$(getState iptables)"
    fi
    echo "iptables: $s"
    echo ""
    echo "/etc/sysconfig/iptables"
    echo "-----------------------"
    cat /etc/sysconfig/iptables 2>/dev/null
    #报表信息
    report_Firewall="$s"
}
  
function getSNMPStatus(){
    #SNMP服务状态，配置等
    echo ""
    echo ""
    echo "############################ SNMP检查 ############################"
    status="$(getState snmpd)"
    echo "服务状态：$status"
    echo ""
    if [ -e /etc/snmp/snmpd.conf ];then
        echo "/etc/snmp/snmpd.conf"
        echo "--------------------"
        cat /etc/snmp/snmpd.conf 2>/dev/null | grep -v "^#" | sed '/^$/d'
    fi
    #报表信息
    report_SNMP="$(getState snmpd)"
}
  
  
  
function getState(){
    if [ $el7 -ne "1" ];then
        if [ -e "/etc/init.d/$1" ];then
            if [ `/etc/init.d/$1 status 2>/dev/null | grep -E "is running|正在运行" | wc -l` -ge 1 ];then
                r="active"
            else
                r="inactive"
            fi
        else
            r="unknown"
        fi
    else
        #CentOS 7+
        r="$(systemctl is-active $1 2>&1)"
    fi
    echo "$r"
}
  
function getSSHStatus(){
    #SSHD服务状态，配置,受信任主机等
    echo ""
    echo ""
    echo "############################ SSH检查 #############################"
    #检查受信任主机
    pwdfile="$(cat /etc/passwd)"
    echo "服务状态：$(getState sshd)"
    Protocol_Version=$(cat /etc/ssh/sshd_config | grep Protocol | awk '{print $2}')
    echo "SSH协议版本：$Protocol_Version"
    echo ""
    echo "信任主机"
    echo "--------"
    authorized=0
    for user in $(echo "$pwdfile" | grep /bin/bash | awk -F: '{print $1}');do
        authorize_file=$(echo "$pwdfile" | grep -w $user | awk -F: '{printf $6"/.ssh/authorized_keys"}')
        authorized_host=$(cat $authorize_file 2>/dev/null | awk '{print $3}' | tr '\n' ',' | sed 's/,$//')
        if [ ! -z $authorized_host ];then
            echo "$user 授权 \"$authorized_host\" 无密码访问"
        fi
        let authorized=authorized+$(cat $authorize_file 2>/dev/null | awk '{print $3}'|wc -l)
    done
  
    echo ""
    echo "是否允许ROOT远程登录"
    echo "--------------------"
    config=$(cat /etc/ssh/sshd_config | grep PermitRootLogin)
    firstChar=${config:0:1}
    if [ $firstChar == "#" ];then
        PermitRootLogin="yes"  #默认是允许ROOT远程登录的
    else
        PermitRootLogin=$(echo $config | awk '{print $2}')
    fi
    echo "PermitRootLogin $PermitRootLogin"
  
    echo ""
    echo "/etc/ssh/sshd_config"
    echo "--------------------"
    cat /etc/ssh/sshd_config | grep -v "^#" | sed '/^$/d'
  
    #报表信息
    report_SSHAuthorized="$authorized"    #SSH信任主机
    report_SSHDProtocolVersion="$Protocol_Version"    #SSH协议版本
    report_SSHDPermitRootLogin="$PermitRootLogin"    #允许root远程登录
}
function getNTPStatus(){
    #NTP服务状态，当前时间，配置等
    echo ""
    echo ""
    echo "############################ NTP检查 #############################"
    if [ -e /etc/ntp.conf ];then
        echo "服务状态：$(getState ntpd)"
        echo ""
        echo "/etc/ntp.conf"
        echo "-------------"
        cat /etc/ntp.conf 2>/dev/null | grep -v "^#" | sed '/^$/d'
    else
        echo "未检测到/etc/ntp.conf"
    fi
    #报表信息
    report_NTP="$(getState ntpd)"
}
  
  
function uploadHostDailyCheckReport(){
    json="{
        \"DateTime\":\"$report_DateTime\",
        \"Hostname\":\"$report_Hostname\",
        \"OSRelease\":\"$report_OSRelease\",
        \"Kernel\":\"$report_Kernel\",
        \"Language\":\"$report_Language\",
        \"LastReboot\":\"$report_LastReboot\",
        \"Uptime\":\"$report_Uptime\",
        \"CPUs\":\"$report_CPUs\",
        \"CPUType\":\"$report_CPUType\",
        \"Arch\":\"$report_Arch\",
        \"MemTotal\":\"$report_MemTotal\",
        \"MemFree\":\"$report_MemFree\",
        \"MemUsedPercent\":\"$report_MemUsedPercent\",
        \"DiskTotal\":\"$report_DiskTotal\",
        \"DiskFree\":\"$report_DiskFree\",
        \"DiskUsedPercent\":\"$report_DiskUsedPercent\",
        \"InodeTotal\":\"$report_InodeTotal\",
        \"InodeFree\":\"$report_InodeFree\",
        \"InodeUsedPercent\":\"$report_InodeUsedPercent\",
        \"IP\":\"$report_IP\",
        \"MAC\":\"$report_MAC\",
        \"Gateway\":\"$report_Gateway\",
        \"DNS\":\"$report_DNS\",
        \"Listen\":\"$report_Listen\",
        \"Selinux\":\"$report_Selinux\",
        \"Firewall\":\"$report_Firewall\",
        \"USERs\":\"$report_USERs\",
        \"USEREmptyPassword\":\"$report_USEREmptyPassword\",
        \"USERTheSameUID\":\"$report_USERTheSameUID\",
        \"PasswordExpiry\":\"$report_PasswordExpiry\",
        \"RootUser\":\"$report_RootUser\",
        \"Sudoers\":\"$report_Sudoers\",
        \"SSHAuthorized\":\"$report_SSHAuthorized\",
        \"SSHDProtocolVersion\":\"$report_SSHDProtocolVersion\",
        \"SSHDPermitRootLogin\":\"$report_SSHDPermitRootLogin\",
        \"DefunctProsess\":\"$report_DefunctProsess\",
        \"SelfInitiatedService\":\"$report_SelfInitiatedService\",
        \"SelfInitiatedProgram\":\"$report_SelfInitiatedProgram\",
        \"RuningService\":\"$report_RuningService\",
        \"Crontab\":\"$report_Crontab\",
        \"Syslog\":\"$report_Syslog\",
        \"SNMP\":\"$report_SNMP\",
        \"NTP\":\"$report_NTP\",
        \"JDK\":\"$report_JDK\"
    }"
    #echo "$json" 
    curl -l -H "Content-type: application/json" -X POST -d "$json" "$uploadHostDailyCheckReportApi" 2>/dev/null
}
  
function check(){
    # redis_check
    # hdparmtest
    # os_versin
    # cpu
    # mem
    # disk_info
    # ip_check
    # file_jdk
    # resin_info
    # db_conn
    # redis_connect
    # weaver_info
    # firewall
    # version
    getSystemStatus
    getCpuStatus
    getMemStatus
    getDiskStatus
    getNetworkStatus
    getListenStatus
    getProcessStatus
    getServiceStatus
    getAutoStartStatus
    getLoginStatus
    getCronStatus
    getUserStatus
    getPasswordStatus
    getSudoersStatus
    getJDKStatus
    getFirewallStatus
    getSSHStatus
    getSyslogStatus
    getSNMPStatus
    getNTPStatus
    getInstalledStatus
    check_web_xml_sequence
    access_secur
}
  
  
#执行检查并保存检查结果
check |tee $RESULTFILE

  
echo "检查结果：$RESULTFILE"
echo ''





