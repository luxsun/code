+-------------------------------------------------------------------------------------------+
| ID       | Item             | Value         | Std_Value            | other                |
+------------------+----------+---------------+----------------------+----------------------+
| 1        | Os_version       | 7.8           | 7+                   | os_version           |
+------------------+----------+---------------+----------------------+----------------------+
| 2        | Cpu_cors         | 2             | 8+                   | cpu_cors             |
+------------------+----------+---------------+----------------------+----------------------+
| 2-1      | uptime           | 0.00,0.04,0.11 |                      | load_average         |
+------------------+----------+---------------+----------------------+----------------------+
| 3        | Total_mem        | 3.8G          | 16G+                 | MemoryTotal          |
+------------------+----------+---------------+----------------------+----------------------+
| 3-1      | Mem_usage        | 23.43%        | <80%                 | MemUse               |
+------------------+----------+---------------+----------------------+----------------------+
| 4        | DiskUsagePer     | 28%           | <80%                 | Disk_percent         |
+------------------+----------+---------------+----------------------+----------------------+
| 5        | hostnameParsing  | 192.168.81.26 | 192.168.81.26        | Host_ip              |
+------------------+----------+---------------+----------------------+----------------------+
| 6        | OpenFiles        | 65535         | 65535                | Files                |
+------------------+----------+---------------+----------------------+----------------------+
| 7        | Jdk_version      | 1.8.0_151     | 1.8+                 | >1.8                 |
+------------------+----------+---------------+----------------------+----------------------+
| 8        | Port_thread      | 2000          | 3000                 | Port_thread          |
+------------------+----------+---------------+----------------------+----------------------+
| 9        | Accept_thread    | 2000          | 3000                 | Accept_thread        |
+------------------+----------+---------------+----------------------+----------------------+
| 10       | Xmx_mem          | 5550m         | 5500M                | >5000M               |
+------------------+----------+---------------+----------------------+----------------------+
| 13       | DB_max_conn      |  300          | 500                  | DB_Connections       |
+------------------+----------+---------------+----------------------+----------------------+
| 14       | Is_cache_check   | iscache=1     | Is_cache=1           | Is_cache=1           |
+------------------+----------+---------------+----------------------+----------------------+
| 15       | redis_check      | 124           | Connect_Error        | Redis_conn           |
+------------------+----------+---------------+----------------------+----------------------+
| 16       | WSession         | Not_foundfile | Not_Null             | Wsession             |
+------------------+----------+---------------+----------------------+----------------------+
| 17       | Precompile       | NotOpen       | Open                 | check_Precom         |
+------------------+----------+---------------+----------------------+----------------------+
| 18       | doc_search       | 1             | 1                    | 1-Open*-Error        |
+------------------+----------+---------------+----------------------+----------------------+
| 19       | firewall_stat    | Dead          | Dead                 | Close_Firewall       |


系统巡检脚本：Version 2020.10.28


############################ 系统检查 ############################
     系统：GNU/Linux
 发行版本：CentOS Linux release 7.8.2003 (Core)
     内核：3.10.0-1127.el7.x86_64
   主机名：ald-29-server
  SELinux：disabled
语言/编码：en_US.UTF-8
 当前时间：2020-11-08 11:12:46
 最后启动：2020-11-08 10:00
 运行时间： 1:12


############################ CPU检查 #############################
物理CPU个数:2
逻辑CPU个数:2
每CPU核心数:1
    CPU型号:Intel(R) Core(TM) i5-8400 CPU @ 2.80GHz
    CPU架构:x86_64


############################ 内存检查 ############################
              total        used        free      shared  buff/cache   available
Mem:           3.8G        920M        2.6G         11M        350M        2.7G
Swap:          5.0G          0B        5.0G


############################ 磁盘检查 ############################
Filesystem                                Type      |  Size  Used  Avail  Use%  |  Inodes  IUsed  IFree  IUse%  |  Mounted
devtmpfs                                  devtmpfs  |  2.0G  0     2.0G   0%    |  489K    393    489K   1%     |  /dev
tmpfs                                     tmpfs     |  2.0G  0     2.0G   0%    |  492K    1      492K   1%     |  /dev/shm
tmpfs                                     tmpfs     |  2.0G  0     2.0G   0%    |  492K    1.3K   491K   1%     |  /run
tmpfs                                     tmpfs     |  2.0G  0     2.0G   0%    |  492K    16     492K   1%     |  /sys/fs/cgroup
tmpfs                                     tmpfs     |  2.0G  12M   2.0G   1%    |  492K    1      492K   1%     |  /dev/shm
tmpfs                                     tmpfs     |  2.0G  12M   2.0G   1%    |  492K    1.3K   491K   1%     |  /run
tmpfs                                     tmpfs     |  2.0G  12M   2.0G   1%    |  492K    16     492K   1%     |  /sys/fs/cgroup
tmpfs                                     tmpfs     |  2.0G  0     2.0G   0%    |  492K    1      492K   1%     |  /dev/shm
tmpfs                                     tmpfs     |  2.0G  0     2.0G   0%    |  492K    1.3K   491K   1%     |  /run
tmpfs                                     tmpfs     |  2.0G  0     2.0G   0%    |  492K    16     492K   1%     |  /sys/fs/cgroup
/dev/mapper/centos_kube--master--65-root  xfs       |  70G   20G   51G    28%   |  35M     351K   35M    1%     |  /
/dev/sda1                                 xfs       |  497M  130M  367M   27%   |  250K    326    250K   1%     |  /boot
tmpfs                                     tmpfs     |  394M  0     394M   0%    |  492K    9      492K   1%     |  /run/user/0


############################ 网络检查 ############################
ens33: 192.168.81.26/22 

网关：192.168.80.1 
 DNS：192.168.0.7,202.96.209.133


############################ 监听检查 ############################
Netid  State   Recv-Q  Send-Q  Local            Address:Port  Peer  Address:Port
udp    UNCONN  0       0       *:68             *:*
udp    UNCONN  0       0       *:111            *:*
udp    UNCONN  0       0       *:855            *:*
udp    UNCONN  0       0       [::]:111         [::]:*
udp    UNCONN  0       0       [::]:855         [::]:*
tcp    LISTEN  0       128     127.0.0.1:42558  *:*
tcp    LISTEN  0       128     *:10050          *:*
tcp    LISTEN  0       128     *:111            *:*
tcp    LISTEN  0       128     *:22             *:*
tcp    LISTEN  0       128     [::]:10050       [::]:*
tcp    LISTEN  0       128     [::]:111         [::]:*
tcp    LISTEN  0       128     [::]:22          [::]:*


############################ 进程检查 ############################

内存占用TOP10
-------------
PID   %MEM  RSS     COMMAND
1741  9.9   400056  /root/.vscode-server/bin/d2e414d9e4239a252d1ab117bd7067f125afd80a/node
1651  2.6   107936  /root/.vscode-server/bin/d2e414d9e4239a252d1ab117bd7067f125afd80a/node
1330  2.5   102216  /root/.vscode-server/bin/d2e414d9e4239a252d1ab117bd7067f125afd80a/node
1405  1.8   73624   /root/.vscode-server/bin/d2e414d9e4239a252d1ab117bd7067f125afd80a/node
1225  1.2   49712   /root/.vscode-server/bin/d2e414d9e4239a252d1ab117bd7067f125afd80a/node
1704  0.6   27940   /root/.vscode-server/bin/d2e414d9e4239a252d1ab117bd7067f125afd80a/node
1433  0.6   27580   /root/.vscode-server/bin/d2e414d9e4239a252d1ab117bd7067f125afd80a/node
1418  0.6   24732   /root/.vscode-server/bin/d2e414d9e4239a252d1ab117bd7067f125afd80a/node
1695  0.6   24696   /root/.vscode-server/bin/d2e414d9e4239a252d1ab117bd7067f125afd80a/node
1658  0.5   23600   /root/.vscode-server/bin/d2e414d9e4239a252d1ab117bd7067f125afd80a/node

CPU占用TOP10
------------
   PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
     1 root      20   0  191016   4044   2596 S   0.0  0.1   0:01.26 systemd
     2 root      20   0       0      0      0 S   0.0  0.0   0:00.00 kthreadd
     4 root       0 -20       0      0      0 S   0.0  0.0   0:00.00 kworker/0:0H
     5 root      20   0       0      0      0 S   0.0  0.0   0:00.13 kworker/u256:0
     6 root      20   0       0      0      0 S   0.0  0.0   0:00.06 ksoftirqd/0
     7 root      rt   0       0      0      0 S   0.0  0.0   0:00.03 migration/0
     8 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcu_bh
     9 root      20   0       0      0      0 S   0.0  0.0   0:01.16 rcu_sched
    10 root       0 -20       0      0      0 S   0.0  0.0   0:00.00 lru-add-drain
    11 root      rt   0       0      0      0 S   0.0  0.0   0:00.01 watchdog/0


############################ 服务检查 ############################

服务配置
--------
auditd.service                              enabled
autovt@.service                             enabled
crond.service                               enabled
dbus-org.freedesktop.nm-dispatcher.service  enabled
getty@.service                              enabled
irqbalance.service                          enabled
lvm2-monitor.service                        enabled
microcode.service                           enabled
NetworkManager-dispatcher.service           enabled
NetworkManager-wait-online.service          enabled
NetworkManager.service                      enabled
nginx.service                               enabled
postfix.service                             enabled
rhel-autorelabel-mark.service               enabled
rhel-autorelabel.service                    enabled
rhel-configure.service                      enabled
rhel-dmesg.service                          enabled
rhel-domainname.service                     enabled
rhel-import-state.service                   enabled
rhel-loadmodules.service                    enabled
rhel-readonly.service                       enabled
rpcbind.service                             enabled
rsyslog.service                             enabled
sshd.service                                enabled
systemd-readahead-collect.service           enabled
systemd-readahead-drop.service              enabled
systemd-readahead-replay.service            enabled
tuned.service                               enabled
vgauthd.service                             enabled
vmtoolsd-init.service                       enabled
vmtoolsd.service                            enabled
zabbix-agent.service                        enabled

正在运行的服务
--------------
auditd.service           loaded active running Security Auditing Service
crond.service            loaded active running Command Scheduler
dbus.service             loaded active running D-Bus System Message Bus
getty@tty1.service       loaded active running Getty on tty1
gssproxy.service         loaded active running GSSAPI Proxy Daemon
irqbalance.service       loaded active running irqbalance daemon
lvm2-lvmetad.service     loaded active running LVM2 metadata daemon
NetworkManager.service   loaded active running Network Manager
polkit.service           loaded active running Authorization Manager
rpcbind.service          loaded active running RPC bind service
rsyslog.service          loaded active running System Logging Service
sshd.service             loaded active running OpenSSH server daemon
systemd-journald.service loaded active running Journal Service
systemd-logind.service   loaded active running Login Service
systemd-udevd.service    loaded active running udev Kernel Device Manager
tuned.service            loaded active running Dynamic System Tuning Daemon
vgauthd.service          loaded active running VGAuth Service for open-vm-tools
vmtoolsd.service         loaded active running Service for virtual machines hosted on VMware
zabbix-agent.service     loaded active running Zabbix Agent


############################ 自启动检查 ##########################
touch /var/lock/subsys/local


############################ 登录检查 ############################
reboot   system boot  3.10.0-1127.el7. Sun Nov  8 10:00 - 11:12  (01:12)    
root     tty1                          Fri Nov  6 17:18 - crash (1+16:42)   
reboot   system boot  3.10.0-1127.el7. Fri Nov  6 15:29 - 11:12 (1+19:43)   
root     pts/1        192.168.81.75    Wed Nov  4 10:05 - crash (2+05:24)   
root     pts/1        192.168.81.75    Wed Nov  4 09:46 - 10:05  (00:18)    
reboot   system boot  3.10.0-1127.el7. Wed Nov  4 09:45 - 11:12 (4+01:27)   
root     pts/2        192.168.81.75    Wed Nov  4 09:30 - crash  (00:14)    
reboot   system boot  3.10.0-1127.el7. Wed Nov  4 09:15 - 11:12 (4+01:57)   
root     pts/1        192.168.81.75    Tue Nov  3 16:19 - crash  (16:55)    
reboot   system boot  3.10.0-1127.el7. Tue Nov  3 16:18 - 11:12 (4+18:54)   


############################ 计划任务检查 ########################
-rw-r--r--.  1  root  root  128  Aug  9   2019  /etc/cron.d/0hourly
-rwx------.  1  root  root  219  Apr  1   2020  /etc/cron.daily/logrotate
-rwxr-xr-x.  1  root  root  618  Oct  30  2018  /etc/cron.daily/man-db.cron
-rw-------.  1  root  root  0    Aug  9   2019  /etc/cron.deny
-rwxr-xr-x.  1  root  root  392  Aug  9   2019  /etc/cron.hourly/0anacron
-rw-r--r--.  1  root  root  451  Jun  10  2014  /etc/crontab


############################ 用户检查 ############################
/etc/passwd 最后修改时间：2020-11-04 09:44:57 (4 天 1 小时前)

特权用户
--------
root

用户列表
--------
用户名  UID  GID  HOME   SHELL     最后一次登录
root    0    0    /root  /bin/zsh  2020-11-06_17:18:00

空密码用户
----------

相同ID的用户
------------


############################ 密码检查 ############################

密码过期检查
------------
root            永不过期

密码策略检查
------------
PASS_MAX_DAYS	99999
PASS_MIN_DAYS	0
PASS_MIN_LEN	5
PASS_WARN_AGE	7


############################ Sudoers检查 #########################
root	ALL=(ALL) 	ALL
%wheel	ALL=(ALL)	ALL



############################ JDK检查 #############################
JAVA_HOME=""


############################ 防火墙检查 ##########################
iptables: inactive

/etc/sysconfig/iptables
-----------------------
# Generated by iptables-save v1.4.21 on Wed Nov  4 09:44:42 2020
*filter
:INPUT ACCEPT [1:78]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [1:156]
-A INPUT -p tcp -m tcp --dport 80 -j ACCEPT
-A INPUT -p tcp -m tcp --dport 10036 -j ACCEPT
COMMIT
# Completed on Wed Nov  4 09:44:42 2020


############################ SSH检查 #############################
服务状态：active
SSH协议版本：

信任主机
--------

是否允许ROOT远程登录
--------------------
PermitRootLogin yes

/etc/ssh/sshd_config
--------------------
HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key
SyslogFacility AUTHPRIV
AuthorizedKeysFile	.ssh/authorized_keys
PasswordAuthentication yes
ChallengeResponseAuthentication no
GSSAPIAuthentication yes
GSSAPICleanupCredentials no
UsePAM yes
X11Forwarding yes
AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
AcceptEnv XMODIFIERS
Subsystem	sftp	/usr/libexec/openssh/sftp-server


############################ syslog检查 ##########################
服务状态：active

/etc/rsyslog.conf
-----------------
*.info;mail.none;authpriv.none;cron.none  /var/log/messages
authpriv.*                                /var/log/secure
mail.*                                    -/var/log/maillog
cron.*                                    /var/log/cron
*.emerg                                   :omusrmsg:*
uucp,news.crit                            /var/log/spooler
local7.*                                  /var/log/boot.log


############################ SNMP检查 ############################
服务状态：unknown



############################ NTP检查 #############################
服务状态：unknown

/etc/ntp.conf
-------------
driftfile /var/lib/ntp/drift
restrict default nomodify notrap nopeer noquery
restrict 127.0.0.1 
restrict ::1
server 0.centos.pool.ntp.org iburst
server 1.centos.pool.ntp.org iburst
server 2.centos.pool.ntp.org iburst
server 3.centos.pool.ntp.org iburst
includefile /etc/ntp/crypto/pw
keys /etc/ntp/keys
disable monitor


############################ 软件检查 ############################
zabbix-agent-3.4.15-1.el7.x86_64           Wed  04  Nov  2020  09:44:57  AM  CST
gpg-pubkey-a14fe591-578876fd               Wed  04  Nov  2020  09:44:56  AM  CST
zabbix-release-3.4-1.el7.centos.noarch     Wed  04  Nov  2020  09:44:46  AM  CST
iptables-services-1.4.21-34.el7.x86_64     Wed  04  Nov  2020  09:44:42  AM  CST
net-tools-2.0-0.25.20131004git.el7.x86_64  Wed  04  Nov  2020  09:44:41  AM  CST
epel-release-7-12.noarch                   Wed  04  Nov  2020  09:44:22  AM  CST
e2fsprogs-devel-1.42.9-17.el7.x86_64       Tue  03  Nov  2020  03:22:03  PM  CST
openssl098e-0.9.8e-29.el7.centos.3.x86_64  Tue  03  Nov  2020  03:22:02  PM  CST
xz-lzma-compat-5.2.2-1.el7.x86_64          Thu  29  Oct  2020  07:14:16  PM  CST
nginx-filesystem-1.16.1-2.el7.noarch       Thu  29  Oct  2020  01:40:29  PM  CST
Start checking the order of the filters in the configuration file web.xml

EncodingFilterWeaver          check_passed
WeaSsoIocComponentFilter          check_passed
SessionCloudFilter            check_passed
SecurityFilter                check_passed
Compress                      check_passed
encodingFilter                check_passed
ECompatibleFilter             check_passed
IECompatibleFilter            check_passed
ConnFastFilter                check_passed
CheckFilter                   check_passed
Mobile                        check_passed
MultiLangFilter Sequence Error PlaseCheck
DialogHandleFilter            check_passed
WStatic                       check_passed
resin-ln                      check_passed
CloudOaFilter                 check_passed
CloudStore                    check_passed
CloudStoreCompatible          check_passed
SessionFilter                 check_passed
InitFilter                    check_passed
CloudStoreConfigFilter          check_passed
XssFilter                     check_passed
WorkflowPicShowFilter          check_passed
DateFormatFilter              check_passed
FileNamingCheckFilter          check_passed
WeaComponentFilter            check_passed

Start checking the Security Url

http://127.0.0.1:80/security/monitor/MonitorStatusForServer.jsp
Access Security page error,please insure the resin has been started! 
