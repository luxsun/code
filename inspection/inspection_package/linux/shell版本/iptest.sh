#!/bin/bash
#---------------------------------------------------------------------------
# File: iptest.sh
# Created Date: 2020-11-08
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Sunday November 8th 2020 11:30:03 am
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
#---------------------------------------------------------------------------
el7=$(uname -r | grep -c "el7")
export IPADDR

echo $IPADDR
