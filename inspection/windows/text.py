
# -*- coding:utf-8 -*-
###
# File: text.py
# Created Date: 2020-10-09
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Wednesday November 11th 2020 10:36:13 am
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
import os
filter_list= ['Compress','MultiLangFilter','SecurityFilter', 'encodingFilter','ECompatibleFilter', 'IECompatibleFilter', 'ConnFastFilter', 'CheckFilter', 'Compress', 'DialogHandleFilter', 'WStatic', 'resin-ln', 'CloudOaFilter', 'CloudStore', 'CloudStoreCompatible', 'SessionFilter', 'InitFilter', 'CloudStoreConfigFilter', 'WeaComponentFilter']


for i in filter_list:
    if i == 'WSessionClusterFilter':
        if  filter_list[0] == 'WSessionClusterFilter' and filter_list[1]=='SecurityFilter':
            print('{:<50s} {:<50s}'.format(i,'CheckPassed'))
        else:
            print('{:<50s} {:<50s}'.format(i,'CheckError'))
            # os._exit()
    elif i == 'SocialIMFilter':
        if  filter_list[filter_list.index('SocialIMFilter') + 1] == 'SecurityFilter':
           print('{:<50s} {:<50s}'.format(i,'CheckPassed'))
        else:
            print('{:<50s} {:<50s}'.format(i,'CheckError'))
    elif i == 'EMFilter':
        for i in filter_list[filter_list.index('EMFilter'):]:
            if i == 'SecurityFilter':
                print('{:<50s} {:<50s}'.format(i,'CheckPassed'))
            else:
                print('{:<50s} {:<50s}'.format('EMFilter','CheckError'))
                break
    elif i == 'SessionCloudFilter':
        str=''
        line=str.join(filter_list[filter_list.index('SessionCloudFilter'):])
        if line.find('SecurityFilter') != -1:
            print('{:<50s} {:<50s}'.format('SessionCloudFilter','CheckPassed'))
        else:
            print('{:<50s} {:<50s}'.format('SessionCloudFilter','CheckError'))
    elif i== 'MultiLangFilter':
        before_str = ''
        after_str = ''
        before=before_str.join(filter_list[:filter_list.index('MultiLangFilter')])
        after=after_str.join(filter_list[filter_list.index('MultiLangFilter'):])
        if before_str.find('WGzipFilter') != -1 and before_str.find('MobileFilter') !=-1 and after_str.find('DialogHandleFilter'):
            print('{:<50s} {:<50s}'.format(i,'CheckPassed'))
        else:
            print('{:<50s} {:<50s}'.format(i,'CheckError'))
    elif i == 'WGzipFilter':
        if  filter_list[filter_list.index('WGzipFilter')-1] == 'SecurityFilter':
            print('{:<50s} {:<50s}'.format(i,'CheckPassed'))
        else:
           print('{:<50s} {:<50s}'.format(i,'CheckError'))
    else:
        print('{:<50s} {:<50s}'.format(i,'CheckPassed'))