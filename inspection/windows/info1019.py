#coding=utf-8
###
# File: info1019.py
# Created Date: 2020-10-19
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Friday November 20th 2020 3:13:59 pm
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
###

###
# File: info.py
# Created Date: 2020-10-09
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Monday October 19th 2020 3:26:22 pm
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
###

import sys,platform
import time
import psutil
import socket
import os
import redis
import configparser
import datetime
import subprocess
import loggingtest
import requests
import json
from lxml import etree
import locale
import pymysql
import pymssql
import re
import sys,platform
import jpype,jaydebeapi

import diskspeed
from random import shuffle
import sqllist
import winreg as wreg





today = datetime.datetime.now()
# log.logger.debug(today)
formatted_today=today.strftime('%Y-%m-%d-%H-%M')
log = loggingtest.Logger('inspect-{0}.log'.format(formatted_today),level='debug')


# LOG_FORMAT = "%(message)s"
# DATE_FORMAT = "%m/%d/%Y %H:%M:%S %p"

# logging.basicConfig(filename='{0}.log'.format(formatted_today), level=logging.DEBUG, format=LOG_FORMAT, datefmt=DATE_FORMAT)

def get_system_info(os): 
    """  
    获取操作系统版本。  
    """  
    log.logger.debug('')

    log.logger.debug("Operating system:") 
    if os == "Windows": 
        c = wmi.WMI () 
        for sys in c.Win32_OperatingSystem(): 
            # log.logger.debug('\t' + "Version :\t%s" % sys.Caption)
            # log.logger.debug('\t' + "Vernum :\t%s" % sys.BuildNumber)
            log.logger.debug('\t' + "Version :\t%s" % sys.Caption)
            log.logger.debug('\t' + "Vernum :\t%s" % sys.BuildNumber)
        try:    
            time_zone=time.strftime('%Z',time.localtime())
            hostname=socket.gethostname()
        except BaseException as e:
            print(str(e))
            return
        log.logger.debug('\t' + "TimeZone :\t%s" % time_zone)
        log.logger.debug('\t' + "Hostname :\t%s" % hostname)
    elif os == "Linux":
        # !psutil.cpu_count()  CPU 核数
        # !psutil.disk_partitions()  磁盘分区
        log.logger.debug('\t' + "Version :\t%s" % '-'.join(platform.dist()))
        log.logger.debug('\t' + "Vernum :\t%s" % platform.uname().release)
        time_zone=time.strftime('%Z',time.localtime())
        hostname=socket.gethostname()
        log.logger.debug('\t' + "TimeZone :\t%s" % time_zone)
        log.logger.debug('\t' + "Hostname :\t%s" % hostname)

def get_memory_info(os): 
    """  
    获取物理内存和虚拟内存。  
    """  
    log.logger.debug('') 
    log.logger.debug("memory_info:" )
    if os == "Windows":
        try: 
            c = wmi.WMI () 
            cs = c.Win32_ComputerSystem()  
            # print('error')
            pfu = c.Win32_PageFileUsage()
            try:  
                pfu_file=c.Win32_PageFileUsage()[0].Caption
                SwapTotal = int(pfu[0].AllocatedBaseSize) 
            except BaseException as e:
                # print('====')
                # print('SwapFileError' + str(e))
                pfu_file = 'NotFound'
                SwapTotal = 'NotFound'
            MemTotal = int(cs[0].TotalPhysicalMemory)/1073741824
            mem=psutil.virtual_memory() 
            mem_free=mem.free/1073741824
            log.logger.debug('\t' + "TotalMemory :" + '\t' + str(round(MemTotal,2)) + "G" )
            log.logger.debug('\t' + "FreeMemory :" + '\t' + str(round(mem_free,2)) + "G")
            #tmpdict["MemFree"] = int(os[0].FreePhysicalMemory)/1024  
            # print('999999')
            log.logger.debug('\t' + "SwapTotal :" + '\t' + str(SwapTotal) + "M" )
            log.logger.debug('\t' + "SwapFile :" + '\t' + str(pfu_file))
        except BaseException as e:
            log.logger.debug('\tMemory Get Error,The reason is  {0}'.format(str(e())))
        #tmpdict["SwapFree"] = int(pfu[0].AllocatedBaseSize - pfu[0].CurrentUsage) 
    elif os == "Linux":
        mem=psutil.virtual_memory() 
        mem_free=mem.free/1073741824
        MemTotal = mem.total/1073741824
        log.logger.debug('\t' + "TotalMemory :" + '\t' + str(round(MemTotal,2)) + "G" )
        log.logger.debug('\t' + "FreeMemory :" + '\t' + str(round(mem_free,2)) + "G")
        SwapTotal = psutil.swap_memory()
        log.logger.debug('\t' + "SwapTotal :" + '\t' + str(round(SwapTotal.total/1073741824,2)) + "G" )
        log.logger.debug('\t' + "SwapFileFree :" + '\t' + str(round(SwapTotal.free/1073741824,2)) + "G")
def get_disk_info(os):  
    """  
    获取物理磁盘信息。  
    """  
    log.logger.debug('') 
    log.logger.debug("disk_info:") 
    if os == "Windows": 
        # tmplist = []
        tmplist = [] 
        c = wmi.WMI () 
        for physical_disk in c.Win32_DiskDrive (): 
            for partition in physical_disk.associators ("Win32_DiskDriveToDiskPartition"): 
                for logical_disk in partition.associators ("Win32_LogicalDiskToPartition"): 
                    tmpdict = {} 
                    tmpdict["Caption"] = logical_disk.Caption 
                    tmpdict["DiskTotal"] = float(logical_disk.Size)/1024/1024/1024 
                    tmpdict["UseSpace"] = (float(logical_disk.Size)-float(logical_disk.FreeSpace))/1024/1024/1024 
                    tmpdict["FreeSpace"] = float(logical_disk.FreeSpace)/1024/1024/1024 
                    tmpdict["Percent"] = int(100.0*(float(logical_disk.Size)-float(logical_disk.FreeSpace))/float(logical_disk.Size)) 
                    tmplist.append(tmpdict) 
        # for physical_disk in c.Win32_DiskDrive(): 
        #     if physical_disk.Size: 
        #         log.logger.debug('\t' + str(physical_disk.Caption) + ' :\t' + str(float(physical_disk.Size)/1024/1024/1024) + "G" )
        for i in tmplist:
            log.logger.debug('\t' + i['Caption']+  '\t'+ str(i['Percent']) + '%')
        for i in tmplist:
            if i['Percent'] > 80:
                log.logger.debug('\t' + i['Caption'] + '磁盘使用率过高')
    elif os == 'Linux':
        for i in psutil.disk_partitions():
            log.logger.debug('\t'+'{:<50s} {:<10s} {:>10s}'.format(str(i[0]),str(i[1]),str(psutil.disk_usage(i[1]).percent) + str('%')))
        for i in psutil.disk_partitions():
            if psutil.disk_usage(i[1]).percent > 80:
                log.logger.debug('\t' + i[1] +' ' + '分区使用率过高,请处理！')



def get_cpu_info(os):  
    """  
    获取CPU信息。  
    """  
    log.logger.debug('') 
    log.logger.debug("cpu_info:")
    if os == "Windows": 
        tmpdict = {}  
        tmpdict["CpuCores"] = 0  
        c = wmi.WMI ()  
        for cpu in c.Win32_Processor():             
            tmpdict["CpuType"] = cpu.Name  
        try:  
            tmpdict["CpuCores"] = cpu.NumberOfCores  
        except:  
            tmpdict["CpuCores"] += 1  
            tmpdict["CpuClock"] = cpu.MaxClockSpeed     
        log.logger.debug('\t' + 'CpuType :\t' + str(tmpdict["CpuType"])) 
        log.logger.debug('\t' + 'CpuCores :\t' + str(psutil.cpu_count()))
    elif os == "Linux":
        with open('/proc/cpuinfo','r',encoding='utf-8',errors='ignore') as f:
            for i in f:
                if i.find('model name') != -1:
                    log.logger.debug('\t' + 'CpuType :\t' + str(i.split(':')[1].strip())) 
                    # print()
                    break
        
        log.logger.debug('\t' + 'CpuCores :\t' + str(psutil.cpu_count()))
 
def get_network_info(os):  
    """  
    获取网卡信息和当前TCP连接数。  
    """  
    log.logger.debug('') 
    log.logger.debug("network_info:" )
    if os == "Windows": 
        c = wmi.WMI ()  
        tmplist = []  
        for interface in c.Win32_NetworkAdapterConfiguration (IPEnabled=1):  
                tmpdict = {}  
                tmpdict["Description"] = interface.Description  
                tmpdict["IPAddress"] = interface.IPAddress[0]  
                tmpdict["IPSubnet"] = interface.IPSubnet[0]  
                tmpdict["MAC"] = interface.MACAddress 
                tmplist.append(tmpdict)  
        for i in tmplist: 
            log.logger.debug('\t' + i["Description"])
            log.logger.debug('\t' + '\t' + "MAC :" + '\t' + i["MAC"])
            log.logger.debug('\t' + '\t' + "IPAddress :" + '\t' + i["IPAddress"]) 
            log.logger.debug('\t' + '\t' + "IPSubnet :" + '\t' + i["IPSubnet"] )
        for interfacePerfTCP in c.Win32_PerfRawData_Tcpip_TCPv4():  
                log.logger.debug('\t' + 'TCP Connect :\t' + str(interfacePerfTCP.ConnectionsEstablished))
    if os == 'Linux':
        log.logger.debug('\t'+'Name' + '\t' + 'IP' + '\t\t' + 'NetMask' + '\t\t' +'Broadcast')
        for i in psutil.net_if_addrs():
            log.logger.debug('\t' + str(i) + '\t' + str(psutil.net_if_addrs()[i][0][1]) + '\t' + str(psutil.net_if_addrs()[i][0][2])+'\t' +str(psutil.net_if_addrs()[i][0][3]))
def execCmd(cmd):
    """
    执行系统命令
    """
    try:  
        exec = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        # with os.popen(cmd,"r") as p:
        #     text = p.read()
        # r = os.popen(cmd)  
        text = exec.stdout.read()
    except BaseException as e:
        log.logger.debug('运行错误' + '\t' + str(e))  
        text = ''
    return text 
# def hdparm_install(path):
#     if os.path.exists(path):
#         result = subprocess.getstatusoutput("hdparm -V")[0]
#         if  result !=0:
#             execCmd('rpm -ivh {0}'.format(path))
#         else:
#             pass
#     else:
#         log.logger.debug('Please check the hdparm.rpm in current dir')

def get_disk_speed(path):
    """
    获取磁盘运行速度
    """
    log.logger.debug('')
    log.logger.debug("Disk_speed")
    log.logger.debug("Plase Wait")
    try:
        benchmark = diskspeed.Benchmark(os.path.join(path,"speedtest"), 100, 4,4)
        log.logger.debug("\t" + str(benchmark.print_result()))
        os.remove(os.path.join(path,"speedtest"))
    except  BaseException as e:
        log.logger.debug("Disk Speed Test Error,The reason maybe is {0}".format(str(e)))
        return
    # if os_type == "Windows":

    #     log.logger.debug('')
    #     log.logger.debug("Disk_speed")
    #     disk_driver=path.split(':')[0]
    #     result = execCmd("winsat disk -drive {0}".format(disk_driver))
    #     with open('speed.out','w',encoding='utf-8') as f:
    #         f.write(result)
    #     with open('speed.out','r',encoding='utf-8') as f:
    #         for i in f:
    #             if i.find(r'Disk  Sequential 64.0 Read') != -1:
    #                 disk_read_speed=i[47:58]
    #                 log.logger.debug('\t' + 'Disk_Read' + '\t' + str(disk_read_speed))
    #             elif i.find(r'Disk  Sequential 64.0 Write') !=-1:
    #                 disk_write_speed=i[47:59]
    #                 log.logger.debug('\t' + 'Disk_Write' + '\t' + str(disk_write_speed))
    #             else:
    #                 pass
    #     os.remove('speed.out')
    # elif os_type == "Linux":
    #     log.logger.debug('')
    #     log.logger.debug("Disk_speed")
    #     hdparm_install(os.path.join(os.getcwd(),'hdparm-9.43-4.el6.x86_64.rpm'))
    #     part_result = execCmd("df -h {0}".format(path) + "| awk '{print $1}' | grep -v Filesystem |grep -v 文件系统 | awk 'NR==1{print $1}'").strip()
    #     findally = execCmd('hdparm -Tt {0}'.format(part_result.decode()) + "| grep Timing | awk -F '=' '{print $2 }'").strip().decode()
    #     log.logger.debug('\t' + 'Disk_part' + '\t' + str(part_result.decode()))
    #     log.logger.debug('\t' + 'Disk_Read' + '\t' + str(findally.split('\n')[0]))
    #     log.logger.debug('\t' + 'Disk_Write' + '\t' + str(findally.split('\n')[1]))


def check_sequence(list,filter_class_list):
    '''
    测试web.xml中各filter的顺序
    '''
    log.logger.debug('')
    log.logger.debug('Sequence:')
    for i in filter_list:
        if i == 'WSessionClusterFilter':
            if  filter_list[0] == 'WSessionClusterFilter' and filter_list[1]=='SecurityFilter':
                log.logger.debug('\t'+'{:<50s} {:<50s}'.format(i,'CheckPassed'))
            else:
                log.logger.debug('\t'+'{:<50s} {:<50s}'.format(i,'CheckError'))
                # os._exit()
        elif i == 'SocialIMFilter':
            if  filter_list[filter_list.index('SocialIMFilter') + 1] == 'SecurityFilter':
                log.logger.debug('\t'+'{:<50s} {:<50s}'.format(i,'CheckPassed'))
            else:
                log.logger.debug('\t'+'{:<50s} {:<50s}'.format(i,'CheckError'))
        elif i == 'EMFilter':
            if "com.cloudstore.dev.api.service.EMFilter" in ''.join(filter_class_list):
                if 'SecurityFilter' in ''.join(filter_list[filter_list.index('EMFilter'):]):
                    log.logger.debug('\t'+'{:<50s} {:<50s}'.format(i,'CheckPassed'))
                else:
                    log.logger.debug('\t'+'{:<50s} {:<50s}'.format('EMFilter','CheckError'))
            else:
                log.logger.debug('\t'+'{:<50s} {:<50s}'.format('EMFilter','does not contain com.cloudstore.dev.api.service.EMFilter'))
            # for j in filter_list[filter_list.index('EMFilter'):]:
            #     if j == 'SecurityFilter':
            #         log.logger.debug('\t'+'{:<50s} {:<50s}'.format(i,'CheckPassed'))
            #     else:
                    
            #         break
        elif i == 'SessionCloudFilter':
            str=''
            line=str.join(filter_list[filter_list.index('SessionCloudFilter'):])
            if line.find('SecurityFilter') != -1:
                log.logger.debug('\t'+'{:<50s} {:<50s}'.format('SessionCloudFilter','CheckPassed'))
            else:
                log.logger.debug('\t'+'{:<50s} {:<50s}'.format('SessionCloudFilter','CheckError'))
        elif i== 'MultiLangFilter':
            before_str = ''
            after_str = ''
            before=before_str.join(filter_list[:filter_list.index('MultiLangFilter')])
            after=after_str.join(filter_list[filter_list.index('MultiLangFilter'):])
            if before.find('Compress') != -1 :
                # and before.find('MobileFilter') !=-1 and after.find('DialogHandleFilter')
                log.logger.debug('\t'+'{:<50s} {:<50s}'.format(i,'CheckPassed'))
            else:
                log.logger.debug('\t'+'{:<50s} {:<50s}'.format(i,'CheckError'))
        elif i == 'Compress':
            if "weaver.filter.WGzipFilter" in ''.join(filter_class_list):
                after=''.join(filter_list[:filter_list.index('Compress')])
                # print(after)
                if  after.find('SecurityFilter') != -1:
                    log.logger.debug('\t'+'{:<50s} {:<50s}'.format(i,'CheckPassed'))
                else:
                    log.logger.debug('\t'+'{:<50s} {:<50s}'.format(i,'CheckError'))
            else:
                log.logger.debug('\t'+'{:<50s} {:<50s}'.format('Compress','does not contain weaver.filter.WGzipFilter'))
        # elif 'WGzipFilter' not in ''.join(filter_list):
        #     log.logger.debug('\t'+'{:<50s} {:<50s}'.format('WGzipFilter','NotFound'))
        # elif 'MultiLangFilter' not in ''.join(filter_list):
        #     log.logger.debug('\t'+'{:<50s} {:<50s}'.format('MultiLangFilter','NotFound'))
        else:
            log.logger.debug('\t'+'{:<50s} {:<50s}'.format(i,'CheckPassed'))
    if 'Compress' not in ''.join(filter_list):
        log.logger.debug('\t'+'{:<50s} {:<50s}'.format('WGzipFilter','NotFound'))
    if 'MultiLangFilter' not in ''.join(filter_list):
        log.logger.debug('\t'+'{:<50s} {:<50s}'.format('MultiLangFilter','NotFound'))
    else:
        ...

def resin_check(java_path,resin_path,resin_jar):
    log.logger.debug('')
    log.logger.debug('Resin_Check')
    # log.logger.debug(java_path,resin_path,resin_jar)
    
    if os.path.exists(java_path) and os.path.exists(resin_jar):
        # cmd=sh.Command("{0}".format(java_path))
        # result=cmd("-classpath","{0}".format(resin_jar),"com.caucho.Version")
        cmd = java_path +  ' ' + '-classpath '  + resin_jar + ' ' +  'com.caucho.Version'
        result=execCmd(cmd).decode().split('(')[0]
        try:
            if result.find('Resin-4.') != -1:
                log.logger.debug('\t' + 'Version {0}'.format(result))
                resin_conf_path=os.path.join(resin_path,'conf/resin.properties')
                with open(resin_conf_path,'r',encoding='utf-8') as f:
                    for i in f:
                        if i.find('jvm_args') !=-1:
                            log.logger.debug('\t' + 'MaxMem:'  + '\t' + i.split(':')[1].split(' ')[1])
                            
            else:
                log.logger.debug('\t' + 'Version {0}'.format(result))
                resin_conf_path=os.path.join(resin_path,'conf/resin.conf')
                with open(resin_conf_path,'r',encoding='utf-8') as f:
                    for i in f:
                        if i.find('<jvm-arg>-Xmx') !=-1:
                            log.logger.debug('\t' + 'MaxMem:' + '\t' + i.split('>')[1].split('<')[0])
        except BaseException as e:
            log.logger.debug('\t' + 'Run Error,The reason maybe is {0} Please Check!'.format(e))
    else:
        log.logger.debug('\t'+ "The path does't exists,please check" + java_path + '\t' + resin_jar )

def esearch_check(path):
    if os.path.exists(path):
        with open(path,'r',encoding='GBK',errors='ignore') as f:
            for i in f:
                if i.find('DOCSEARCH') != -1:
                    return True
                else:
                    return False

def decrypt_weaver(resin_path):

        '''
        decrypt the aes encrypted data
        '''
        log.logger.debug('')
        log.logger.debug('Security Check')
        resin4_conf = os.path.join(resin_path,'conf/resin.properties')
        resin3_conf = os.path.join(resin_path,'conf/resin.conf')
        if os.path.exists(resin4_conf):
            with open(resin4_conf,'r',encoding='utf-8',errors='ignore') as f:
                app_http_list = re.findall(r'app.http.+\d',f.read())
                port = app_http_list[0].split(':')[1].strip() 
        elif os.path.exists(resin3_conf):
            with open(resin3_conf,'r',encoding='utf-8',errors='ignore') as f:
                try:
                    app_http_list = re.findall(r'<http.*',f.read())
                    fin = re.findall(r"\d+\.?\d*",''.join(app_http_list))
                    port=int(fin[0]) 
                except  BaseException as e:
                    port=80
                    print(str(e))
        else:
            log.logger.debug('\tCant Find The resin.properties or Resin.conf ,Please Check! Exit!')
            return
            
        
        try:
            url = 'http://127.0.0.1:' +str(port) + "/security/monitor/MonitorStatusForServer.jsp"
            print(url)
            ret = requests.get(url)
        except BaseException as e:
            log.logger.debug('\t' + 'The Url cant reach,the reason maybe is {0},please check!'.format(str(e)))
            return
            # os._exit(0)
        if ret.status_code == 200:
            data = ret.content.strip()
        else:
            log.logger.debug('\t' + "The  Security url can't reach,please check!")
            return
        if os.path.exists('aescoder.jar'):
            # os.environ["JAVA_HOME"] = jre_path
            # jpype.startJVM(classpath=['aescoder.jar'])
            # os.listdir()
            # 加载自定义的java class
            JDClass = jpype.JClass("com.AESCoder")
            jd = JDClass()
            res=jd.decrypt(data, private_key)
            _dic = json.loads(str(res))
            # log.logger.debug(_dic)
            log.logger.debug('\t'+'{:<30s} {:<30s}'.format('FirewallStatus',str(_dic['firewallStatus'])))
            log.logger.debug('\t'+'{:<30s} {:<30s}'.format('AutoUpdateStatus',str(_dic['autoUpdateStatus'])))
        else:
            log.logger.debug('\t' + 'Decrypt aescoder.jar does not exists,Please check!')
            # os._exit(0)
            return
        try:
            remoteversion = _dic['remoteVersion']
            log.logger.debug('\t' + '{:<30s} {:<30s}'.format('RemoteVersion', remoteversion))
        except BaseException as e:
            # remoteversion = ''
            log.logger.debug('\t' + '{:<30s} {:<30s}'.format('RemoteVersion', '未检测到RemoteVersion'))

        log.logger.debug('\t'+'{:<30s} {:<30s}'.format('SoftwareVersion', str(_dic['softwareVersion'])))
        # log.logger.debug('FirewallStatus' + '\t' +  + '\n'  +'AutoUpdateStatus' + '\t'+ str(_dic['autoUpdateStatus']))
        # return res
        # jpype.shutdownJVM()

def check_maxconn(path):
    log.logger.debug('')
    log.logger.debug('Database Conn')
    if os.path.exists(path):
        with open(path,'r',encoding='utf-8') as f:
            for i in f:
                if i.find('ecology.maxconn') !=-1:
                    log.logger.debug('\t' + 'MaxConn' + '\t' +i.split('=')[1].strip())
    else:
        log.logger.debug("{0} does't not exists!".format(path))


def check_cache(path):
    log.logger.debug('')
    log.logger.debug('IsCache')
    if os.path.exists(path):
        with open(path,'r',encoding='GBK',errors='ignore') as f:
            for i in f:
                if i.find('iscache') != -1:
                    log.logger.debug('\t' + i.strip())
    else:
        log.logger.debug('\t'+"{0} does't not exists!".format(path))


def check_redis(path):
    log.logger.debug('')
    log.logger.debug('Redis_Connect_Test')
    if os.path.exists(path):
        if os.path.exists(path):
            with open(path,'r',encoding='GBK',errors='ignore') as f:
                for i in f:
                    if i.find('redisIp') != -1:
                        redis_host=i.split('=')[1].strip()
                    elif i.find('redisPort') != -1:
                        redis_port=i.split('=')[1].strip()
                    elif i.find('redisPassword') != -1:
                        redis_passwd = i.split('=')[1].strip()
                    else:
                        pass
            try:
                r= redis.Redis(host=redis_host,port=int(redis_port),password=redis_passwd,socket_connect_timeout=10)
                r.info()
                log.logger.debug('\t' + 'Redis Connect Success')
            except BaseException as e:
                log.logger.debug('\t' + 'Redis Connect Error,The reason maybe is {0}'.format(str(e)))     
    else:
        log.logger.debug('\t'+ "{0} does't not exists!".format(path))

def check_doc_search(path):
    log.logger.debug('')
    log.logger.debug('Check_Doc_Search')
    if os.path.exists(path):
        with open(path,'r',encoding='GBK',errors='ignore') as f:
            for i in f:
                if i.find('use_full_search') != -1:
                    log.logger.debug('\t' + i.strip())
    else:
        log.logger.debug('\t' + "{0} does't not exists!".format(path))
def mysql_test(host, port, user, passwd, db):
    try:
        db_connect = pymysql.connect(
            host=host, port=port, user=user, password=passwd, db=db, charset='utf8')
    except BaseException as e:
        log.logger.debug('\tThe Connection to the DB Error,the reason maybe is {0}'.format(e))
        return
    try:
        cursor = db_connect.cursor()
        for i in sqllist.mysql_list:
            # print(i)
            cursor.execute(i)
            row = cursor.fetchall()
            log.logger.debug('\t\n' +i + '\n'+ str(row))
        for i in table_list:
            cursor.execute("SELECT count(*) FROM {0}".format(i))
            row = cursor.fetchone()
            log.logger.debug('\t' +'Table'+ '\t'  + i + "\t" + str(row[0]))
    except BaseException as e:
        log.logger.debug('\tThe Query is error,the reason maybe is {0}'.format(e),'red')


def sqlserver_test(host, user, passwd, db,port):
    try:
        # print(host,port,user,passwd,db)
        conn = pymssql.connect(host=str(host+':'+port),user=str(user),password=str(passwd),database=str(db))
    except BaseException as e:
        log.logger.debug('\tThe Connection to the DB Error,the reason maybe is {0}'.format(e))
        return
    try:
        cursor = conn.cursor()
        for i in sqllist.sqlserver_list:
            # print(i)
            cursor.execute(i)
            row = cursor.fetchall()
            log.logger.debug('\t\n' +i + '\n'+ str(row))
        for i in table_list:
            cursor.execute("SELECT count(*) FROM {0}".format(i))
            row = cursor.fetchone()
            log.logger.debug('\t' +'Table' + '\t' + i + "\t" + str(row[0]))
        
    except BaseException as e:
        log.logger.debug('\tThe Query is error,the reason maybe is {0}'.format(e))


def oracle_test(url,user,password):
    try:
        # print('\t{0}'.format(jarpath), '')
        # jpype.startJVM(classpath=[jarpath])
        db=jaydebeapi.connect('oracle.jdbc.OracleDriver',url,[user,password])
    except BaseException as e:
        log.logger.debug(str(e))
    try:
        cursor = db.cursor()
        for i in sqllist.oracle_sqllist:
            # print(i)
            cursor.execute(i)
            row = cursor.fetchall()
            log.logger.debug('\t\n' +i + '\n'+ str(row))
        for i in table_list:
            cursor.execute("SELECT count(*) FROM {0}".format(i))
            row = cursor.fetchone()
            log.logger.debug('\t' +'Table' + '\t' + i + "\t" + str(row[0]))
    except BaseException as e:
        log.logger.debug('\tThe Query is error,the reason maybe is {0}'.format(e))


def check_database(app_path):
    log.logger.debug('')
    log.logger.debug('Check_Database')
    weaver_conf = os.path.join(app_path, 'WEB-INF/prop/weaver.properties')
    if os.path.exists(weaver_conf):
        if os.path.getsize(weaver_conf) > 0:
            with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as f:
                if 'mysql' in f.read():
                    # print('According to the configuration file, determine that the database type is mysql, and start to verify the mysql link')
                    with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as g:
                        for i in g:
                            if 'jdbc:mysql' in i:
                                str(i).replace("\\","")
                                print('\t' + i,'')
                                db_host_ip = re.findall(r'\d+.\d+.\d+.\d+', i)
                                db_port = ((i.split('?')[0]).split(
                                    '/')[2]).split(':')[1].strip()
                                db_name = ((i.split('?')[0])).split('/')[3].strip()
                            elif 'ecology.user' in i:
                                db_user = i.split('=')[1].strip()
                            elif 'ecology.password' in i:
                                db_passwd = i.split('=')[1].strip()
                    try:
                        log.logger.debug('\tThe DB properties in weaver.properties is {0} {1} {2} {3} {4}\n'.format(db_host_ip[0],int(db_port),
                               db_user, db_passwd, db_name))
                        mysql_test(db_host_ip[0],int(db_port),db_user,db_passwd,db_name)
                    except BaseException as e:
                        log.logger.debug(e)
            with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as f:
                # print('sqlserver')
                if 'microsoft.sqlserver' in f.read():
                    # print('According to the configuration file, determine that the database type is sqlserver, and start to verify the sqlserver link')
                    with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as g:
                        for i in g:
                            if 'jdbc:sqlserver' in i:
                                log.logger.debug('\t' + i)
                                # print(i)
                                db_host_ip = (
                                    (i.split('//')[1]).split(';')[0]).split(':')[0].strip()
                                db_port = (
                                    (i.split('//')[1]).split(';')[0]).split(':')[1].strip()
                                db_name = i.split('=')[2].strip()
                            elif 'ecology.user' in i:
                                db_user = i.split('=')[1].strip()
                            elif 'ecology.password' in i:
                                db_passwd = i.split('=')[1].strip()
                    try:
                        log.logger.debug('\tThe DB properties in weaver.properties is {0} {1} {2} {3} {4}\n'.format(db_host_ip,db_port,db_user,db_passwd
                        ,db_name))
                        sqlserver_test(db_host_ip,db_user,db_passwd,db_name,db_port)
                    except BaseException as e:
                        log.logger.debug(e)
            with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as f:
                if 'oracle' in f.read():
                    # print('According to the configuration file, determine that the database type is Oracle, and start to verify the Oracle link')
                    with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as g:
                        for i in g:
                            if 'oracle' in i and "jdbc" in i and ':' in i:
                                # print(i)
                                
                                i = i.replace("\\",'')
                                log.logger.debug('\t' + i)
                                url=i.split("=")[1].strip()
                            elif 'ecology.user' in i:
                                db_user = i.split('=')[1].strip()
                            elif 'ecology.password' in i:
                                db_passwd = i.split('=')[1].strip()
                    
                    try: 
                        log.logger.debug('\tThe DB properties in weaver.properties is {0} {1} {2}\n'.format(url, db_user, db_passwd))
                        oracle_test(url, db_user,db_passwd)
                    except BaseException as e:
                        log.logger.debug('\t Get DB info error,the reason maybe is {0},Please check the weaver.properties'.format(str(e)))
                        # ...
            with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as f:
                count = 0
                for i in f:
                    if i.find('oracle') != -1 or i.find('sqlserver') !=-1 or i.find('mysql') != -1:
                        count+=1
                    else:
                        pass
                if count > 0:
                    pass
                else:
                    log.logger.debug('\tThe link information of the database oracle or sqlserver or mysql is not found, please check!')
        else:
            log.logger.debug('\tThe file weaver.properties is Empty,Please Check')
    else:
        log.logger.debug('\tThe {0} Path can not be found,Please Check!'.format(weaver_conf))
def esearch(esearch_path):
    log.logger.debug('')
    log.logger.debug('Check_ESearch')
    esearch_log = os.path.join(esearch_path,'webapps\ROOT\log\searchLog.log')
    if esearch_path != '':
        # print(esearch_path)
        if os.path.exists(esearch_log):
            if esearch_check(esearch_log) == True:
                log.logger.debug('\t' + "Esearch CheckPassed!")
            else:
                log.logger.debug('\t' + 'Esearch CheckError!')
        else:
            log.logger.debug('\t' + '{0} does not exists,please Check!'.format(esearch_log))
    else:
        log.logger.debug('\tNot Found Esearch path,Skip!')
def system_file_cache():
    log.logger.debug('')
    log.logger.debug('SystemFileCache')
    if platform.system() == "Windows":
        if  os.path.exists(os.path.join(os.getcwd(),"SetSystemFileCache.exe")):
            log.logger.debug("\t Begin to set the system fileCache")
            execCmd("./SetSystemFileCache.exe 100 512")
            execCmd("./SetSystemFileCache.exe -flush")
            log.logger.debug("\t Sucess")
        else:
            log.logger.debug("\t File does't Exist,Skip!")

def resin_process_check(resin_path):
    log.logger.debug('')
    log.logger.debug('ResinProcess')
    resin_pid=0
    java_pid = 0
    pid_list=[]
    if platform.system() =="Linux":
        for i in list(psutil.process_iter()):
            if i.name() == 'java':
                log.logger.debug("\t"+i)
                if resin_path+"/log"   in str(i.cmdline()) and 'watchdog' in str(i.cmdline()):
                    resin_pid+=1
                    pid_list.append([i.name,i.cmdline()])
                if resin_path+"/conf" in str(i.cmdline()):
                    java_pid +=1
                    pid_list.append([i.name,i.cmdline()])
    else:
        resin_new_path=resin_path.replace("\\","/")
        resin_conf=str(os.path.join(resin_path,"conf")).lower()
        for i in list(psutil.process_iter()):
            if i.name() == "javaw.exe":
                log.logger.debug(i)
                # d:/weaver/resin/log
                if str(resin_new_path).lower() + "/log" in str(i.cmdline()).lower() and "watchdog" in str(i.cmdline()).lower() :
                    resin_pid+=1
                    pid_list.append([i.name,i.cmdline()])
                if  resin_conf.replace("\\",'') in str(i.cmdline()).lower().replace("\\",""):
                    java_pid +=1
                    pid_list.append([i.name,i.cmdline()])

    if resin_pid == java_pid == 1:
        log.logger.debug('\tCheckPassed')
    else:
        log.logger.debug("\tResin Process sum {0} {1},CheckError!".format(resin_pid,java_pid))
        for i in pid_list:
            log.logger.debug( "\t" +str(i))

def tcp_ip(os_type):
    log.logger.debug('')
    log.logger.debug('TcpIP parameters')
    if os_type == "Windows":
        try:
            key = wreg.OpenKey(wreg.HKEY_LOCAL_MACHINE, r'SYSTEM\CurrentControlSet\Services\Tcpip\Parameters', 0, wreg.KEY_SET_VALUE)
            wreg.SetValueEx(key, "TcpTimedWaitDelay", 1, wreg.REG_DWORD, 30)
            wreg.SetValueEx(key, "MaxUserPort", 1, wreg.REG_DWORD, 65535)
            log.logger.debug("\t Set Success!")
        except BaseException as e:
            log.logger.debug("\t Reg chanage Error,Please Check!")
            return
    else:
        with open("/etc/sysctl.conf",'r',encoding="utf-8",errors="ignore") as f:
            if "tcp_fin_timeout" in f.read():
                log.logger.debug("\tCheck the tcp_fin_timeout in sysctl.conf,Skip!")
            else:
                with open("/etc/sysctl.conf",'a+',encoding="utf-8",errors="ignore") as f:
                    f.write("""
net.ipv4.tcp_fin_timeout = 30
net.ipv4.tcp_keepalive_time = 1200
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_tw_reuse = 1
net.ipv4.tcp_tw_recycle = 1
net.ipv4.ip_local_port_range = 10000 65000
net.ipv4.tcp_max_syn_backlog = 8192
net.ipv4.tcp_max_tw_buckets = 5000
""")
                log.logger.debug("\tAdd TCPip parameters Success!")
        execCmd("/sbin/sysctl -p") 

def resin_hot_deploy(resin_path):
    log.logger.debug('')
    log.logger.debug('Resin Hot deployment')
    if os.path.exists(os.path.join(resin_path,"stophotdeploy.jar")):
        log.logger.debug("\t StophotDeploy.jar Exist,check Pass")
    else:
        log.logger.debug("\t Check Error,The StophotDeploy.jar does'n Exists!")

if __name__ == "__main__":
    table_list = ["docdetail","shareinnerdoc"]
    private_key="WEAVERECOLOGYSECURITY3.0VERSION201607150857"
    cfg = configparser.ConfigParser()
    
    try:
        if os.path.exists('config'):
            cfg.read('config')
            ecology_path = cfg['config']['Ecology_path']
            resin_path = cfg['config']['Resin_path']
            jdk_path = cfg['config']['Jdk_path']
            jre_path = cfg['config']['Jre_path']
            esearch_path = cfg['config']['Esearch_path']
            # sys_url = cfg['config']['OA_sys_url']
        else:
            log.logger.debug("The config.txt does't not exist,please check!")
            os._exit(0)
            
    except BaseException as e:
        log.logger.debug('Configfile parse Error,Please check the config file ! the reason maybe is {0}'.format(str(e)))
        os._exit(0)

    # the releated config file path
    if platform.system() == "Windows":
        import wmi 
        locale.setlocale(locale.LC_CTYPE, 'chinese')
        weaverpro_path = os.path.join(ecology_path,'WEB-INF\prop\weaver.properties')
        cache_path = os.path.join(ecology_path,'WEB-INF\prop\initCache.properties')
        weaver_redis_path = os.path.join(ecology_path,'WEB-INF\prop\weaver_new_session.properties')
        doc_full_search_path = os.path.join(ecology_path,'WEB-INF\prop/doc_full_search.properties')
        resin_exec = os.path.join(resin_path,'resin.exe')
        java_exec=os.path.join(jdk_path,'java.exe')
        resin_jar_path=os.path.join(resin_path,'resin.jar')
        
    elif platform.system() == "Linux":
        import sh
        weaverpro_path = os.path.join(ecology_path,'WEB-INF/prop/weaver.properties')
        cache_path = os.path.join(ecology_path,'WEB-INF/prop/initCache.properties')
        weaver_redis_path = os.path.join(ecology_path,'WEB-INF/prop/weaver_new_session.properties')
        doc_full_search_path = os.path.join(ecology_path,'WEB-INF/prop/doc_full_search.properties')
        # resin_exec = os.path.join(resin_path,'resin.exe')
        java_exec=os.path.join(jdk_path,'java')
        resin_jar_path=os.path.join(resin_path,'resin.jar')
        # esearch_log = os.path.join(esearch_path,'webapps/ROOT/log/searchLog.log')
    if os.path.exists(jre_path):
        os.environ["JAVA_HOME"] = jre_path
    else:
        log.logger.debug("\t Jre_path not exists,config maybe error,please check!")
        os._exit(0)
    if os.path.exists(os.path.join(ecology_path,'WEB-INF/lib/ojdbc6.jar')):
        jar_path=os.path.join(ecology_path,'WEB-INF/lib/ojdbc6.jar')
    elif os.path.exists(os.path.join(ecology_path,'WEB-INF/lib/ojdbc-6g.jar')):
        jar_path=os.path.join(ecology_path,'WEB-INF/lib/ojdbc-6g.jar')
    else:
        log.logger.debug('ojdbc.jar or ojdbc-6g can not found,the oracle db connect will error')
        jar_path=''
    
    # log.logger.debug(weaverpro_path,cache_path,weaver_redis_path,doc_full_search_path,resin_exec)
    try:
        filter_list= []
        filter_class_list = []
        webxml_path = os.path.join(ecology_path,'WEB-INF/web.xml')
        # tree = ET.parse(webxml_path)
        #
        tree=etree.parse(webxml_path) 

        for i in tree.iterfind('filter/filter-name'):
            filter_list.append(i.text)
        for i in tree.iterfind('filter/filter-class'):
            filter_class_list.append(i.text)
    except BaseException as e:
        log.logger.debug('WEB-INF/web.xml parse error ,The reason maybe is {0} '.format(str(e)))
    finally:
        pass
    if os.path.exists(ecology_path) and os.path.exists(resin_path) and os.path.exists(jdk_path):
        try:
        # disk_driver=ecology_path.split(':')[0]
            os_platform = platform.system() 
            get_system_info(os_platform) 
            get_memory_info(os_platform) 
            get_disk_info(os_platform) 
            get_cpu_info(os_platform) 
            get_network_info(os_platform)
            # get_disk_speed(ecology_path)
            esearch(esearch_path)
            check_maxconn(weaverpro_path)
            check_cache(cache_path)
            check_redis(weaver_redis_path)
            check_doc_search(doc_full_search_path)
            resin_check(java_exec,resin_path,resin_jar_path)
            check_sequence(filter_list,filter_class_list)
            system_file_cache()
            resin_process_check(resin_path)
            tcp_ip(os_platform)
            resin_hot_deploy(resin_path)
            jpype.startJVM(classpath=['aescoder.jar',jar_path])
            decrypt_weaver(resin_path)
            check_database(ecology_path)
        except BaseException as e:
            log.logger.debug(str(e))
    else:
        log.logger.debug("The path does'n esixts,Please Check!")
        os._exit(0)
    # os.system('Pause')


