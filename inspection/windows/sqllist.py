# -*- coding:utf-8 -*-
###
# File: sqllist.py
# Created Date: 2020-11-17
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Tuesday November 17th 2020 3:03:05 pm
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
###
oracle_sqllist=["SELECT inst_id, instance_name, host_name, VERSION,TO_CHAR(startup_time, 'yyyy-mm-dd hh24:mi:ss') AS startup_time, status, archiver, database_status FROM gv$instance",
"""SELECT inst_id, dbid, NAME
, TO_CHAR(created, 'yyyy-mm-dd hh24:mi:ss') AS created, log_mode
, TO_CHAR(version_time, 'yyyy-mm-dd hh24:mi:ss') AS version_time, open_mode
FROM gv$database""","SELECT SUM(bytes) / 1024 / 1024 FROM v$sgainfo","SELECT * FROM user_users",

"SELECT * FROM dba_profiles",

"""SELECT df.tablespace_name, COUNT(*) AS datafile_count
, ROUND(SUM(df.BYTES) / 1048576) AS size_mb
, ROUND(SUM(free.BYTES) / 1048576, 2) AS free_mb
, ROUND(SUM(df.BYTES) / 1048576 - SUM(free.BYTES) / 1048576, 2) AS used_mb
, ROUND(MAX(free.maxbytes) / 1048576, 2) AS maxfree
, 100 - ROUND(100.0 * SUM(free.BYTES) / SUM(df.BYTES), 2) AS pct_used
, ROUND(100.0 * SUM(free.BYTES) / SUM(df.BYTES), 2) AS pct_free
FROM dba_data_files df, (
SELECT tablespace_name, file_id, SUM(BYTES) AS BYTES
, MAX(BYTES) AS maxbytes
FROM dba_free_space
GROUP BY tablespace_name, file_id
) free
WHERE df.tablespace_name = free.tablespace_name(+)
AND df.file_id = free.file_id(+)
GROUP BY df.tablespace_name
ORDER BY 8""",

"""SELECT file#, ts#, NAME, status
, BYTES / 1024 / 1024 AS size_mb
FROM v$datafile
UNION ALL
SELECT file#, ts#, NAME, status
, BYTES / 1024 / 1024 AS size_mb
FROM v$tempfile""",
"SELECT name, value FROM v$parameter WHERE NAME IN ('processes')",

"""SELECT name, value
FROM v$parameter
WHERE NAME IN ('db_files')""",

"""SELECT name, value
FROM v$parameter
WHERE NAME IN ('cursor_sharing')"""]


sqlserver_list=["select @@version;","exec sp_spaceused;","dbcc sqlperf(logspace);",
"""SELECT CONVERT(CHAR(100), SERVERPROPERTY('Servername')) AS Server,
 bs.database_name,
 bs.backup_start_date,
 bs.backup_finish_date,
 bs.expiration_date,
 CASE bs.type
 WHEN 'D' THEN
 'Database'
 WHEN 'L' THEN
 'Log'
 END AS backup_type,
 bs.backup_size,
 bmf.logical_device_name,
 bmf.physical_device_name,
 bs.name AS backupset_name,
 bs.description
 FROM msdb.dbo.backupmediafamily bmf
 INNER JOIN msdb.dbo.backupset bs
 ON bmf.media_set_id = bs.media_set_id
 ORDER BY bs.backup_finish_date desc;
"""]

mysql_list=[
"""SHOW VARIABLES WHERE variable_name IN ('log_bin_trust_function_creators','transaction_isolation','lower_case_table_names','sql_mode','innodb_large_prefix','group_concat_max_len'
);""",

"""SHOW VARIABLES WHERE variable_name IN (
'binlog_format','log_bin','expire_logs_days'
);""",
"""SHOW VARIABLES WHERE variable_name IN (
'max_allowed_packet','slow_query_log','innodb_buffer_pool_size','innodb_buffer_pool_instances','innodb_thread_concurrency','sync_binlog','innodb_flush_log_at_trx_commit','max_connections'
);"""

]
