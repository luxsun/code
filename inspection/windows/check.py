# 上线前巡检
# Author： luxsn


import psutil
import time
import socket
mem=psutil.virtual_memory()
print(mem)
mem_total=mem.total/1073741824
mem_free=mem.free/1073741824
print("%.2f"% mem_total)
print("%.2f"% mem_free)


time_zone=time.strftime('%Z',time.localtime())
print(time_zone)

hostname=socket.gethostname()
print(hostname)

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(('8.8.8.8', 80))
ip = s.getsockname()[0]
print(ip)
