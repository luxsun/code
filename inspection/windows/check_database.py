# -*- coding:utf-8 -*-
###
# File: check_database.py
# Created Date: 2020-11-09
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Monday November 9th 2020 12:53:00 pm
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
###
#!/usr/bin/env python3
# -*- coding:utf-8 -*-
###
# File: check_database.py
###
import os
import pymysql
import pymssql
import re
import sys,platform
import jpype,jaydebeapi

# from common_error_keyword import db_error_dic


# def oracle_ins(path):
#     if os.path.exists(path):
#         common_tools.execCmd('rpm -ivh {0}'.format(path))
#         common_tools.execCmd('sudo sh -c "echo /usr/lib/oracle/18.3/client64/lib > /etc/ld.so.conf.d/oracle-instantclient.conf"')
#         common_tools.execCmd('export LD_LIBRARY_PATH=/usr/lib/oracle/18.3/client64/lib:$LD_LIBRARY_PATH')
#     else:
#        print('\tPlease check the oracle_instances.rpm in current dir','red')

def mysql_test(host, port, user, passwd, db):
    try:
        db_connect = pymysql.connect(
            host=host, port=port, user=user, password=passwd, db=db, charset='utf8')
    except BaseException as e:
        print('\tThe Connection to the DB Error,the reason maybe is {0}'.format(e), 'red')
        return
    try:
        cursor = db_connect.cursor()
        cursor.execute("SELECT * FROM hrmresourcemanager")
        row = cursor.fetchone()
        print('\t' + str(row),'green')
    except BaseException as e:
        print('\tThe Query is error,the reason maybe is {0}'.format(e),'red')


def sqlserver_test(host, user, passwd, db,port):
    try:
        # print(host,port,user,passwd,db)
        conn = pymssql.connect(host=str(host+':'+port),user=str(user),password=str(passwd),database=str(db))
    except BaseException as e:
        print('\tThe Connection to the DB Error,the reason maybe is {0}'.format(e), 'red')
        return
    try:
        cursor = conn.cursor()
        cursor.execute("select * from hrmresourcemanager;")
        row = cursor.fetchone()
        print('\t'+str(row),'green')
    except BaseException as e:
        print('\tThe Query is error,the reason maybe is {0}'.format(e),'red')


def oracle_test(url,user,password):
    try:
        # print('\t{0}'.format(jarpath), '')
        # jpype.startJVM(classpath=[jarpath])
        db=jaydebeapi.connect('oracle.jdbc.OracleDriver',url,[user,password])
    except BaseException as e:
        print(str(e))
    try:
        cursor = db.cursor()
        cursor.execute("SELECT  * FROM HRMRESOURCEMANAGER h ")
        row = cursor.fetchone()
        print('\t'+str(row),'green')
    except BaseException as e:
        print('\tThe Query is error,the reason maybe is {0}'.format(e),'red')


def check_database(app_path):
    weaver_conf = os.path.join(app_path, 'WEB-INF/prop/weaver.properties')
    if os.path.exists(weaver_conf):
        if os.path.getsize(weaver_conf) > 0:
            with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as f:
                if 'mysql' in f.read():
                    # print('According to the configuration file, determine that the database type is mysql, and start to verify the mysql link')
                    with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as g:
                        for i in g:
                            if 'jdbc:mysql' in i:
                                str(i).replace("\\","")
                                print('\t' + i,'')
                                db_host_ip = re.findall(r'\d+.\d+.\d+.\d+', i)
                                db_port = ((i.split('?')[0]).split(
                                    '/')[2]).split(':')[1].strip()
                                db_name = ((i.split('?')[0])).split('/')[3].strip()
                            elif 'ecology.user' in i:
                                db_user = i.split('=')[1].strip()
                            elif 'ecology.password' in i:
                                db_passwd = i.split('=')[1].strip()
                    try:
                        print('\tThe DB properties in weaver.properties is {0} {1} {2} {3} {4}'.format(db_host_ip[0],int(db_port),
                               db_user, db_passwd, db_name),'green')
                        mysql_test(db_host_ip[0],int(db_port),db_user,db_passwd,db_name)
                    except BaseException as e:
                        print(e,'red')
            with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as f:
                # print('sqlserver')
                if 'microsoft.sqlserver' in f.read():
                    # print('According to the configuration file, determine that the database type is sqlserver, and start to verify the sqlserver link')
                    with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as g:
                        for i in g:
                            if 'jdbc:sqlserver' in i:
                                print('\t' + i,'')
                                # print(i)
                                db_host_ip = (
                                    (i.split('//')[1]).split(';')[0]).split(':')[0].strip()
                                db_port = (
                                    (i.split('//')[1]).split(';')[0]).split(':')[1].strip()
                                db_name = i.split('=')[2].strip()
                            elif 'ecology.user' in i:
                                db_user = i.split('=')[1].strip()
                            elif 'ecology.password' in i:
                                db_passwd = i.split('=')[1].strip()
                    try:
                        print('\tThe DB properties in weaver.properties is {0} {1} {2} {3} {4}'.format(db_host_ip,db_port,db_user,db_passwd
                        ,db_name), 'green')
                        sqlserver_test(db_host_ip,db_user,db_passwd,db_name,db_port)
                    except BaseException as e:
                        print(e,'red')
            with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as f:
                if 'oracle' in f.read():
                    # print('According to the configuration file, determine that the database type is Oracle, and start to verify the Oracle link')
                    with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as g:
                        for i in g:
                            
                            if 'jdbc:oracle' in i:
                                print('\t' + i,'')
                                url=i.split("=")[1].strip()
                            elif 'ecology.user' in i:
                                db_user = i.split('=')[1].strip()
                            elif 'ecology.password' in i:
                                db_passwd = i.split('=')[1].strip()
                    
                    try: 
                        print('\tThe DB properties in weaver.properties is {0} {1} {2}'.format(url, db_user, db_passwd), 'green')
                        oracle_test(url, db_user,db_passwd)
                    except BaseException as e:
                        print('\t Get DB info error,the reason maybe is {0},Please check the weaver.properties'.format(str(e)),'red')
                        # ...
            with open(os.path.join(app_path, 'WEB-INF/prop/weaver.properties'), 'r', encoding='utf-8') as f:
                count = 0
                for i in f:
                    if i.find('oracle') != -1 or i.find('sqlserver') !=-1 or i.find('mysql') != -1:
                        count+=1
                    else:
                        pass
                if count > 0:
                    pass
                else:
                    print('\tThe link information of the database oracle or sqlserver or mysql is not found, please check!','red')
        else:
            print('\tThe file weaver.properties is Empty,Please Check','red')
    else:
        print('\tThe {0} Path can not be found,Please Check!'.format(weaver_conf),'red')
