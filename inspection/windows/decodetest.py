# -*- coding:utf-8 -*-
###
# File: decodetest.py
# Created Date: 2020-10-10
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Saturday October 10th 2020 12:47:03 pm
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
###
from Cryptodome.Cipher import AES
import base64
private_key ='WEAVERECOLOGYSECURITY3.0VERSION201607150857'
encrypted_text='993171e74f9d84e9c2c3cc4eb06634068cc8652001dc1766d57d7582868bcdf867f4f9d1be2e78cc1cd1082d22fa009bc181abf5e4673fe18c9e8931df3711d312edf76caaacd7bed59a8c13b0ef0cc7e74396ac587cc0df1882f8ea639cb2ba7a4a95f2706ba4d5dcdedab46187a22db95f18a8620baf29e00c49ae99045fcda5ee0e12a7077089d5b3ae6176aadd5e61e27341046277467fd47fd77cbd63bdff75f502cb806387ea3194f6513a807c4777198ddd0320c7d1478f198aff6e5261f45ee89d76cb57b36ca484dea8986c759c3a0d430de6acc40e04d7bb1909869f5012c5e5f27cea89b53ce7745aaf78d3335833e5f60212e547c7ee7e49c39e3c220cb1fd6635b1f1f398077bfd66c7cd0a2f668d08461754735a89755745634f3c2be59bc710e6e1e26200f2a64aaa4ede0b7d72541fe17b6a82deb6558849a86451f28bd52f2cf34e7dfdbf6395c172f7e80e1fc7f322cda8b73eccfa1b99c2278659b431e5fde4e3c7eefec232dbcb6304235f6456d40e5b7923acf9e2d295585f3f6c38df948950a27857c9b384a87d1976b341c64b14448a07e7245fd71154b3b2267944a4b8303d8a62c50135903cac31d24b3f3ce455ac30297619663e834cddc0eb128a4ea5c5d791f657931550b82a2587eb20d7222b7fa85c08eb42b35d340964703af833d9a7901183b751bf32355f7ad1612fcd68e03b190515425a14938f66de0b61a285e24d80116e639a4ffdc9ceb0d96f351747b4f49ac65fe956e1cfc6df1059930f0fda3099058f99318d38307f7da5108545b9d9cc5d286008f7c9a93d992f4484f8cc34d62d5c6a9631b090a95f5a93b416a97e3416a527e572752fde0a860191a561b070ec331df009a692cdb01b30c4cbf040bec156012f1ddd3ad6ca0ed6d0416b62f8661b023fc51390c43546f80caaeb8d71e8d9b813566fb7ce2c0dced1574e6d982788930ba97ace98e184bb603e2a1de6aa9888fa617a6661c2603a1db1a498bf9d'
text = 'abcdefg'

def add_to_16(s):
    while len(s) % 16 != 0:
        s += (16 - len(s) % 16) * chr(16 - len(s) % 16)
    return str.encode(s) 

aes = AES.new(str.encode(private_key), AES.MODE_ECB)

encrypted_text = str(base64.encodebytes(aes.encrypt(add_to_16(text))), encoding='utf8').replace('\n', '')  # 加密
# decrypted_text = aes.decrypt(base64.decodebytes(bytes(encrypted_text, encoding='utf8'))).decode("utf8")  # 解密
# decrypted_text = decrypted_text[:-ord(decrypted_text[-1])]  # 去除多余补位
 

# print('pkcs5解密值:', decrypted_text)

print('pkcs5加密值:', encrypted_text)