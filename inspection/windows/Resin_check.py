# -*- coding:utf-8 -*-
###
# File: Resin_check.py
# Created Date: 2020-10-09
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Friday October 16th 2020 1:08:01 pm
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
###
import os
import loggingtest


def execCmd(cmd):
    try:  
        r = os.popen(cmd)  
        text = r.read()  
        r.close()
    except BaseException as e:
        print('运行错误' + '\t' + str(e))  
        text = ''
    return text 
def resin_check(java_path,resin_path,resin_jar):
    print()
    print('Resin_Check')
    # print(java_path,resin_path,resin_jar)
    if os.path.exists(java_path) and os.path.exists(resin_jar):
        cmd = java_path +  ' ' + '-classpath '  + resin_jar + ' ' +  'com.caucho.Version'
        result=execCmd(cmd).split('(')[0]
        try:
            if result.find('Resin-4.') != -1:
                print('\t' + 'Version {0}'.format(result))
                resin_conf_path=os.path.join(resin_path,'conf/resin.properties')
                with open(resin_conf_path,'r',encoding='utf-8') as f:
                    for i in f:
                        if i.find('jvm_args') !=-1:
                            print('\t' + 'MaxMem:'  + '\t' + i.split(':')[1].split(' ')[1])
                            
            else:
                print('\t' + 'Version {0}'.format(result))
                resin_conf_path=os.path.join(resin_path,'conf/resin.conf')
                with open(resin_conf_path,'r',encoding='utf-8') as f:
                    for i in f:
                        if i.find('<jvm-arg>-Xmx') !=-1:
                            print('\t' + 'MaxMem:' + '\t' + i.split('>')[1].split('<')[0])
        except BaseException as e:
            print('\t' + 'Run Error,The reason maybe is {0} Please Check!'.format(e))
    else:
        print('\t'+ "The path does't exists,please check" + java_path + '\t' + resin_jar )

