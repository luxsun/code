# -*- coding:utf-8 -*-
###
# File: winregtest.py
# Created Date: 2020-11-18
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Wednesday November 18th 2020 2:08:42 pm
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
###
# encoding:utf-8
# encoding:utf-8
# from __future__ import print_function
# from winreg import *
# import ctypes, sys
import platform
import winreg as wreg
import psutil
import os

# key = wreg.OpenKey(wreg.HKEY_LOCAL_MACHINE, r'SYSTEM\CurrentControlSet\Services\Tcpip\Parameters', 0, wreg.KEY_SET_VALUE)
# wreg.SetValueEx(key, "TcpTimedWaitDelay", 1, wreg.REG_DWORD, 60)
# wreg.SetValueEx(key, "MaxUserPort", 1, wreg.REG_DWORD, 65532)
# subDir = r'SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy'
def resin_process_check(resin_path):
    log.logger.debug('')
    log.logger.debug('ResinProcess')
    resin_pid=0
    java_pid = 0
    pid_list=[]
    if platform.system() =="Linux":
        for i in list(psutil.process_iter()):
            if i.name() == 'java':
                log.logger.debug(i)
                if resin_path+"/log"   in str(i.cmdline()) and 'watchdog' in str(i.cmdline()):
                    resin_pid+=1
                    pid_list.append([i.name,i.cmdline()])
                if resin_path+"/conf" in str(i.cmdline()):
                    java_pid +=1
                    pid_list.append([i.name,i.cmdline()])
    else:
        resin_new_path=resin_path.replace("\\","/")
        resin_conf=str(os.path.join(resin_path,"conf")).lower()
        for i in list(psutil.process_iter()):
            if i.name() == "javaw.exe":
                log.logger.debug(i)
                # d:/weaver/resin/log
                if str(resin_new_path).lower() + "/log" in str(i.cmdline()).lower() and "watchdog" in str(i.cmdline()).lower() :
                    resin_pid+=1
                    pid_list.append([i.name,i.cmdline()])
                if  resin_conf.replace("\\",'') in str(i.cmdline()).lower().replace("\\",""):
                    java_pid +=1
                    pid_list.append([i.name,i.cmdline()])

    if resin_pid == java_pid == 1:
        log.logger.debug('\tCheckPassed')
    else:
        log.logger.debug("\tResin Process sum {0} {1},CheckError!".format(resin_pid,java_pid))
        for i in pid_list:
            log.logger.debug( "\t" +str(i))

resin_path="D:\weaver\Resin"
# resin_new_path=resin_path.replace("\\","/")
# def resin_process_check(resin_path):
#     os_type=platform.system()
#     print('')
#     print('ResinProcess')
#     resin_pid=0
#     java_pid = 0
    
#     resin_conf = os.path.join(resin_path,'conf')
#     pid_list=[]
#     if os_type=="Linux":
#         process_name="java"
#     else:
#         process_name="javaw.exe"
#     for i in list(psutil.process_iter()):
#         if i.name() == process_name:
#             print(i,i.cmdline())
#             if resin_new_path +"/log"   in str(i.cmdline()).lower() and 'watchdog' in str(i.cmdline()).lower():
#                 resin_pid+=1
#                 print(i.cmdline())
#                 pid_list.append([i.name,i.cmdline()])
#             if str(resin_conf).lower()  in str(i.cmdline()).lower():
#                 java_pid +=1
#                 print("test")
#                 pid_list.append([i.name,i.cmdline()])

#     if resin_pid == java_pid == 1:
#         print('\tCheckPassed')
#     else:
#         print("\tResin Process sum {0} {1},CheckError!".format(resin_pid,java_pid))
#         for i in pid_list:
#             print( "\t" +str(i))
resin_process_check(resin_path)
# standard = r'\StandardProfile'
# public = r'\PublicProfile'
# enableKey = 'EnableFirewall'
# value = 1

# def is_admin():
#     try:
#         return ctypes.windll.shell32.IsUserAnAdmin()
#     except:
#         return False

# def updateFireWall(keyName):
#     # 1.连接注册表根键
#     regRoot = ConnectRegistry(None, HKEY_LOCAL_MACHINE)
#     # 2.获取指定目录下键的控制
#     keyHandel = OpenKey(regRoot, subDir+keyName)
#     if is_admin():
#         # 3.设置该键的指定键值enableKey为value
#         SetValueEx(keyHandel, enableKey, value, REG_DWORD, value)
#     else:
#         if sys.version_info[0] == 3:
#             ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, __file__, None, 1)
#     # 4.关闭键的控制
#     CloseKey(keyHandel)
#     CloseKey(regRoot)

# if __name__ == '__main__':
#     # updateFireWall(standard)
#     updateFireWall(public)
