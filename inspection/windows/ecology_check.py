# -*- coding:utf-8 -*-
###
# File: ecology_check.py
# Created Date: 2020-10-09
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Friday October 9th 2020 4:19:11 pm
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
###
import os
import xml.etree.ElementTree as ET
import redis
# ecology_path=r'F:\\weaver\\ecology'
# weaverpro_path=os.path.join(ecology_path,'WEB-INF/prop/weaver.properties')
# cache_path=os.path.join(ecology_path,'WEB-INF/prop/initCache.properties')
# weaver_redis_path=os.path.join(ecology_path,'WEB-INF/prop/weaver_new_session.properties')
# doc_full_search_path=os.path.join(ecology_path,'WEB-INF/prop/doc_full_search.properties')


# filter_list= []
# webxml_path = os.path.join(ecology_path,'WEB-INF/web.xml')
# tree = ET.parse(webxml_path)

# for i in tree.iterfind('filter/filter-name'):
#     filter_list.append(i.text)


def check_maxconn(path):
    print()
    print('Database Conn')
    if os.path.exists(path):
        with open(path,'r',encoding='utf-8') as f:
            for i in f:
                if i.find('ecology.maxconn') !=-1:
                    print('\t' + 'MaxConn' + '\t' +i.split('=')[1].strip())
    else:
        print("{0} does't not exists!".format(path))


def check_cache(path):
    print()
    print('IsCache')
    if os.path.exists(path):
        with open(path,'r',encoding='GBK',errors='ignore') as f:
            for i in f:
                if i.find('iscache') != -1:
                    print('\t' + i.strip())
    else:
        print('\t'+"{0} does't not exists!".format(path))


def check_redis(path):
    print()
    print('Redis_Connect_Test')
    if os.path.exists(path):
        if os.path.exists(path):
            with open(path,'r',encoding='GBK',errors='ignore') as f:
                for i in f:
                    if i.find('redisIp') != -1:
                        redis_host=i.split('=')[1].strip()
                    elif i.find('redisPort') != -1:
                        redis_port=i.split('=')[1].strip()
                    elif i.find('redisPassword') != -1:
                        redis_passwd = i.split('=')[1].strip()
                    else:
                        pass
            try:
                r= redis.Redis(host=redis_host,port=int(redis_port),password=redis_passwd,socket_connect_timeout=10)
                r.info()
                print('\t' + 'Redis Connect Success')
            except BaseException as e:
                print('\t' + 'Redis Connect Error,The reason maybe is {0}'.format(str(e)))     
    else:
        print('\t'+ "{0} does't not exists!".format(path))

def check_doc_search(path):
    print()
    print('Check_Doc_Search')
    if os.path.exists(path):
        with open(path,'r',encoding='GBK',errors='ignore') as f:
            for i in f:
                if i.find('use_full_search') != -1:
                    print('\t' + i.strip())
    else:
        print('\t' + "{0} does't not exists!".format(path))
# check_sequence(filter_list)
# check_maxconn(weaverpro_path)
# check_cache(cache_path)
# check_redis(weaver_redis_path)
# check_doc_search(doc_full_search_path)


