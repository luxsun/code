# -*- coding:utf-8 -*-
###
# File: taktest.py
# Created Date: 2020-11-17
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Tuesday November 17th 2020 3:41:43 pm
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
###
from tkinter import *
import tkinter.messagebox as messagebox
from tkinter.filedialog import askdirectory

class Application(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.pack()
        self.createWidgets()

    def createWidgets(self):
        self.nameInput = Entry(self)
        self.nameInput.pack()
        self.alertButton = Button(self, text='选择路径', command=self.selectPath)
        self.alertButton.pack()

    def hello(self):
        name = self.nameInput.get() or 'world'
        messagebox.showinfo('Message', 'Hello, %s' % name)

    def selectPath(self):
        path = StringVar()
        path_ = askdirectory()
        path.set(path_)

app = Application()
# 设置窗口标题:
app.master.title('Hello World')
# 主消息循环:
app.mainloop()