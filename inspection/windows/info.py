
###
# File: info.py
# Created Date: 2020-10-09
# Author: sunzhe
# Contact: <sunzhenet@163.com>
#
# Last Modified: Friday October 16th 2020 4:21:09 pm
#
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
###
import wmi
import sys
import platform
import time
import psutil
import socket
import os
import redis
import xml.etree.ElementTree as ET
import Resin_check
import ecology_check
# import diskspeed
import security_check
import configparser
import logging
import datetime
import loggingtest
import jpype
import requests
import json


today = datetime.datetime.now()
# log.logger.debug(today)
formatted_today = today.strftime('%Y-%m-%d-%H-%M')
log = loggingtest.Logger(
    'inspect-{0}.log'.format(formatted_today), level='debug')

# LOG_FORMAT = "%(message)s"
# DATE_FORMAT = "%m/%d/%Y %H:%M:%S %p"

# logging.basicConfig(filename='{0}.log'.format(formatted_today), level=logging.DEBUG, format=LOG_FORMAT, datefmt=DATE_FORMAT)


def get_system_info(os):
    """  
    获取操作系统版本。  
    """
    log.logger.debug('')

    log.logger.debug("Operating system:")
    if os == "Windows":
        c = wmi.WMI()
        for sys in c.Win32_OperatingSystem():
            # log.logger.debug('\t' + "Version :\t%s" % sys.Caption)
            # log.logger.debug('\t' + "Vernum :\t%s" % sys.BuildNumber)
            log.logger.debug('\t' + "Version :\t%s" % sys.Caption)
            log.logger.debug('\t' + "Vernum :\t%s" % sys.BuildNumber)
        time_zone = time.strftime('%Z', time.localtime())
        hostname = socket.gethostname()
        log.logger.debug('\t' + "TimeZone :\t%s" % time_zone)
        log.logger.debug('\t' + "Hostname :\t%s" % hostname)


def get_memory_info(os):
    """  
    获取物理内存和虚拟内存。  
    """
    log.logger.debug('')
    log.logger.debug("memory_info:")
    if os == "Windows":
        c = wmi.WMI()
        cs = c.Win32_ComputerSystem()
        pfu = c.Win32_PageFileUsage()
        pfu_file = c.Win32_PageFileUsage()[0].Caption
        MemTotal = int(cs[0].TotalPhysicalMemory)/1073741824
        mem = psutil.virtual_memory()
        mem_free = mem.free/1073741824
        log.logger.debug('\t' + "TotalPhysicalMemory :" +
                         '\t' + str(round(MemTotal, 2)) + "G")
        log.logger.debug('\t' + "FreeMemory :" + '\t' +
                         str(round(mem_free, 2)) + "G")
        #tmpdict["MemFree"] = int(os[0].FreePhysicalMemory)/1024
        SwapTotal = int(pfu[0].AllocatedBaseSize)
        log.logger.debug('\t' + "SwapTotal :" + '\t' + str(SwapTotal) + "M")
        log.logger.debug('\t' + "SwapFile :" + '\t' + str(pfu_file))
        #tmpdict["SwapFree"] = int(pfu[0].AllocatedBaseSize - pfu[0].CurrentUsage)


def get_disk_info(os):
    """  
    获取物理磁盘信息。  
    """
    log.logger.debug('')
    log.logger.debug("disk_info:")
    if os == "Windows":
        # tmplist = []
        tmplist = []
        c = wmi.WMI()
        for physical_disk in c.Win32_DiskDrive():
            for partition in physical_disk.associators("Win32_DiskDriveToDiskPartition"):
                for logical_disk in partition.associators("Win32_LogicalDiskToPartition"):
                    tmpdict = {}
                    tmpdict["Caption"] = logical_disk.Caption
                    tmpdict["DiskTotal"] = float(
                        logical_disk.Size)/1024/1024/1024
                    tmpdict["UseSpace"] = (
                        float(logical_disk.Size)-float(logical_disk.FreeSpace))/1024/1024/1024
                    tmpdict["FreeSpace"] = float(
                        logical_disk.FreeSpace)/1024/1024/1024
                    tmpdict["Percent"] = int(
                        100.0*(float(logical_disk.Size)-float(logical_disk.FreeSpace))/float(logical_disk.Size))
                    tmplist.append(tmpdict)
        # for physical_disk in c.Win32_DiskDrive():
        #     if physical_disk.Size:
        #         log.logger.debug('\t' + str(physical_disk.Caption) + ' :\t' + str(float(physical_disk.Size)/1024/1024/1024) + "G" )
        for i in tmplist:
            log.logger.debug('\t' + i['Caption'] +
                             '\t' + str(i['Percent']) + '%')
            if i['Percent'] > 80:
                log.logger.debug('\t\t' + i['Caption'] + '磁盘使用率过高')


def get_cpu_info(os):
    """  
    获取CPU信息。  
    """
    log.logger.debug('')
    log.logger.debug("cpu_info:")
    if os == "Windows":
        tmpdict = {}
        tmpdict["CpuCores"] = 0
        c = wmi.WMI()
        for cpu in c.Win32_Processor():
            tmpdict["CpuType"] = cpu.Name
        try:
            tmpdict["CpuCores"] = cpu.NumberOfCores
        except:
            tmpdict["CpuCores"] += 1
            tmpdict["CpuClock"] = cpu.MaxClockSpeed
        log.logger.debug('\t' + 'CpuType :\t' + str(tmpdict["CpuType"]))
        log.logger.debug('\t' + 'CpuCores :\t' + str(tmpdict["CpuCores"]))


def get_network_info(os):
    """  
    获取网卡信息和当前TCP连接数。  
    """
    log.logger.debug('')
    log.logger.debug("network_info:")
    if os == "Windows":
        tmplist = []
        c = wmi.WMI()
        for interface in c.Win32_NetworkAdapterConfiguration(IPEnabled=1):
            tmpdict = {}
            tmpdict["Description"] = interface.Description
            tmpdict["IPAddress"] = interface.IPAddress[0]
            tmpdict["IPSubnet"] = interface.IPSubnet[0]
            tmpdict["MAC"] = interface.MACAddress
            tmplist.append(tmpdict)
        for i in tmplist:
            log.logger.debug('\t' + i["Description"])
            log.logger.debug('\t' + '\t' + "MAC :" + '\t' + i["MAC"])
            log.logger.debug('\t' + '\t' + "IPAddress :" +
                             '\t' + i["IPAddress"])
            log.logger.debug('\t' + '\t' + "IPSubnet :" + '\t' + i["IPSubnet"])
        for interfacePerfTCP in c.Win32_PerfRawData_Tcpip_TCPv4():
            log.logger.debug('\t' + 'TCP Connect :\t' +
                             str(interfacePerfTCP.ConnectionsEstablished))


def execCmd(cmd):
    """
    执行系统命令
    """
    try:
        with os.popen(cmd, "r") as p:
            text = p.read()
        # r = os.popen(cmd)
    except BaseException as e:
        log.logger.debug('运行错误' + '\t' + str(e))
        text = ''
    return text


def get_disk_speed(path):
    """
    获取磁盘运行速度
    """
    log.logger.debug('')
    log.logger.debug("Disk_speed")
    result = execCmd("winsat disk -drive {0}".format(path))
    with open('speed.out', 'w', encoding='utf-8') as f:
        f.write(result)
    with open('speed.out', 'r', encoding='utf-8') as f:
        for i in f:
            if i.find(r'Disk  Sequential 64.0 Read') != -1:
                disk_read_speed = i[47:58]
                log.logger.debug('\t' + 'Disk_Read' +
                                 '\t' + str(disk_read_speed))
            elif i.find(r'Disk  Sequential 64.0 Write') != -1:
                disk_write_speed = i[47:59]
                log.logger.debug('\t' + 'Disk_Write' +
                                 '\t' + str(disk_write_speed))
            else:
                pass
    os.remove('speed.out')


def check_sequence(list):
    """
    测试web.xml中各filter的顺序
    """
    log.logger.debug('')
    log.logger.debug('Sequence:')
    for i in filter_list:
        if i == 'WSessionClusterFilter':
            if filter_list[0] == 'WSessionClusterFilter' and filter_list[1] == 'SecurityFilter':
                log.logger.debug(
                    '\t'+'{:<50s} {:<50s}'.format(i, 'CheckPassed'))
            else:
                log.logger.debug(
                    '\t'+'{:<50s} {:<50s}'.format(i, 'CheckError'))
                # os._exit()
        elif i == 'SocialIMFilter':
            if filter_list[filter_list.index('SocialIMFilter') + 1] == 'SecurityFilter':
                log.logger.debug(
                    '\t'+'{:<50s} {:<50s}'.format(i, 'CheckPassed'))
            else:
                log.logger.debug(
                    '\t'+'{:<50s} {:<50s}'.format(i, 'CheckError'))
        elif i == 'EMFilter':
            for i in filter_list[filter_list.index('EMFilter'):]:
                if i == 'SecurityFilter':
                    log.logger.debug(
                        '\t'+'{:<50s} {:<50s}'.format(i, 'CheckPassed'))
                else:
                    log.logger.debug(
                        '\t'+'{:<50s} {:<50s}'.format('EMFilter', 'CheckError'))
                    break
        elif i == 'SessionCloudFilter':
            str = ''
            line = str.join(
                filter_list[filter_list.index('SessionCloudFilter'):])
            if line.find('SecurityFilter') != -1:
                log.logger.debug(
                    '\t'+'{:<50s} {:<50s}'.format('SessionCloudFilter', 'CheckPassed'))
            else:
                log.logger.debug(
                    '\t'+'{:<50s} {:<50s}'.format('SessionCloudFilter', 'CheckError'))
        elif i == 'MultiLangFilter':
            before_str = ''
            after_str = ''
            before = before_str.join(
                filter_list[:filter_list.index('MultiLangFilter')])
            after = after_str.join(
                filter_list[filter_list.index('MultiLangFilter'):])
            if before.find('WGzipFilter') != -1 and before.find('MobileFilter') != -1 and after.find('DialogHandleFilter'):
                log.logger.debug(
                    '\t'+'{:<50s} {:<50s}'.format(i, 'CheckPassed'))
            else:
                log.logger.debug(
                    '\t'+'{:<50s} {:<50s}'.format(i, 'CheckError'))
        elif i == 'WGzipFilter':
            if filter_list[filter_list.index('WGzipFilter')-1] == 'SecurityFilter':
                log.logger.debug(
                    '\t'+'{:<50s} {:<50s}'.format(i, 'CheckPassed'))
            else:
                log.logger.debug(
                    '\t'+'{:<50s} {:<50s}'.format(i, 'CheckError'))
        else:
            log.logger.debug('\t'+'{:<50s} {:<50s}'.format(i, 'CheckPassed'))


def resin_check(java_path, resin_path, resin_jar):
    log.logger.debug('')
    log.logger.debug('Resin_Check')
    # log.logger.debug(java_path,resin_path,resin_jar)
    if os.path.exists(java_path) and os.path.exists(resin_jar):
        cmd = java_path + ' ' + '-classpath ' + resin_jar + ' ' + 'com.caucho.Version'
        result = execCmd(cmd).split('(')[0]
        try:
            if result.find('Resin-4.') != -1:
                log.logger.debug('\t' + 'Version {0}'.format(result))
                resin_conf_path = os.path.join(
                    resin_path, 'conf/resin.properties')
                with open(resin_conf_path, 'r', encoding='utf-8') as f:
                    for i in f:
                        if i.find('jvm_args') != -1:
                            log.logger.debug(
                                '\t' + 'MaxMem:' + '\t' + i.split(':')[1].split(' ')[1])

            else:
                log.logger.debug('\t' + 'Version {0}'.format(result))
                resin_conf_path = os.path.join(resin_path, 'conf/resin.conf')
                with open(resin_conf_path, 'r', encoding='utf-8') as f:
                    for i in f:
                        if i.find('<jvm-arg>-Xmx') != -1:
                            log.logger.debug(
                                '\t' + 'MaxMem:' + '\t' + i.split('>')[1].split('<')[0])
        except BaseException as e:
            log.logger.debug(
                '\t' + 'Run Error,The reason maybe is {0} Please Check!'.format(e))
    else:
        log.logger.debug(
            '\t' + "The path does't exists,please check" + java_path + '\t' + resin_jar)


def decrypt_weaver(sys_url, jre_path):
    '''
    decrypt the aes encrypted data
    '''
    log.logger.debug('')
    log.logger.debug('Security Check')
    url = sys_url + "security/monitor/MonitorStatusForServer.jsp"
    try:
        ret = requests.get(url)
    except BaseException as e:
        log.logger.debug(
            '\t' + 'The Url cant reach,the reason maybe is {0},please check!'.format(str(e)))
        os._exit(0)
    if ret.status_code == 200:
        data = ret.content.strip()
    else:
        log.logger.debug('\t' + "The  Security url can't reach,please check!")
        os._exit(0)
    if os.path.exists('aescoder.jar'):
        os.environ["JAVA_HOME"] = jre_path
        jpype.startJVM(classpath=['aescoder.jar'])
        # os.listdir()
        # 加载自定义的java class
        JDClass = jpype.JClass("com.AESCoder")
        jd = JDClass()
        res = jd.decrypt(data, private_key)
        _dic = json.loads(str(res))
        # log.logger.debug(_dic)
        log.logger.debug(
            '\t'+'{:<30s} {:<30s}'.format('FirewallStatus', str(_dic['firewallStatus'])))
        log.logger.debug(
            '\t'+'{:<30s} {:<30s}'.format('AutoUpdateStatus', str(_dic['autoUpdateStatus'])))
    else:
        log.logger.debug(
            '\t' + 'Decrypt aescoder.jar does not exists,Please check!')
        os._exit(0)
    try:
        remoteversion = _dic['remoteVersion']
        log.logger.debug(
            '\t' + '{:<30s} {:<30s}'.format('RemoteVersion', remoteversion))
    except BaseException as e:
        # remoteversion = ''
        log.logger.debug(
            '\t' + '{:<30s} {:<30s}'.format('RemoteVersion', '未检测到RemoteVersion'))

    log.logger.debug(
        '\t'+'{:<30s} {:<30s}'.format('SoftwareVersion', str(_dic['softwareVersion'])))
    # log.logger.debug('FirewallStatus' + '\t' +  + '\n'  +'AutoUpdateStatus' + '\t'+ str(_dic['autoUpdateStatus']))
    # return res
    jpype.shutdownJVM()


def check_maxconn(path):
    log.logger.debug('')
    log.logger.debug('Database Conn')
    if os.path.exists(path):
        with open(path, 'r', encoding='utf-8') as f:
            for i in f:
                if i.find('ecology.maxconn') != -1:
                    log.logger.debug('\t' + 'MaxConn' + '\t' +
                                     i.split('=')[1].strip())
    else:
        log.logger.debug("{0} does't not exists!".format(path))


def check_cache(path):
    log.logger.debug('')
    log.logger.debug('IsCache')
    if os.path.exists(path):
        with open(path, 'r', encoding='GBK', errors='ignore') as f:
            for i in f:
                if i.find('iscache') != -1:
                    log.logger.debug('\t' + i.strip())
    else:
        log.logger.debug('\t'+"{0} does't not exists!".format(path))


def check_redis(path):
    # *This is Import
    log.logger.debug('')
    log.logger.debug('Redis_Connect_Test')
    if os.path.exists(path):
        if os.path.exists(path):
            with open(path, 'r', encoding='GBK', errors='ignore') as f:
                for i in f:
                    if i.find('redisIp') != -1:
                        redis_host = i.split('=')[1].strip()
                    elif i.find('redisPort') != -1:
                        redis_port = i.split('=')[1].strip()
                    elif i.find('redisPassword') != -1:
                        redis_passwd = i.split('=')[1].strip()
                    else:
                        pass
            try:
                r = redis.Redis(host=redis_host, port=int(
                    redis_port), password=redis_passwd, socket_connect_timeout=10)
                r.info()
                log.logger.debug('\t' + 'Redis Connect Success')
            except BaseException as e:
                log.logger.debug(
                    '\t' + 'Redis Connect Error,The reason maybe is {0}'.format(str(e)))
    else:
        log.logger.debug('\t' + "{0} does't not exists!".format(path))


def check_doc_search(path):
    log.logger.debug('')
    log.logger.debug('Check_Doc_Search')
    if os.path.exists(path):
        with open(path, 'r', encoding='GBK', errors='ignore') as f:
            for i in f:
                if i.find('use_full_search') != -1:
                    log.logger.debug('\t' + i.strip())
    else:
        log.logger.debug('\t' + "{0} does't not exists!".format(path))


if __name__ == "__main__":
    private_key = "WEAVERECOLOGYSECURITY3.0VERSION201607150857"
    cfg = configparser.ConfigParser()
    try:
        if os.path.exists('config'):
            cfg.read('config')
            ecology_path = cfg['config']['Ecology_path']
            resin_path = cfg['config']['Resin_path']
            jdk_path = cfg['config']['Jdk_path']
            jre_path = cfg['config']['Jre_path']
            sys_url = cfg['config']['OA_sys_url']
        else:
            log.logger.debug("The config.txt does't not exist,please check!")
            os._exit(0)

    except BaseException as e:
        log.logger.debug(
            'Configfile parse Error,Please check the config file ! the reason maybe is {0}'.format(str(e)))
        os._exit(0)

    # the releated config file path
    weaverpro_path = os.path.join(
        ecology_path, 'WEB-INF\\prop\\weaver.properties')
    cache_path = os.path.join(
        ecology_path, 'WEB-INF\\prop\\initCache.properties')
    weaver_redis_path = os.path.join(
        ecology_path, 'WEB-INF\\prop\\weaver_new_session.properties')
    doc_full_search_path = os.path.join(
        ecology_path, 'WEB-INF\\prop\\doc_full_search.properties')
    resin_exec = os.path.join(resin_path, 'resin.exe')

    java_exec = os.path.join(jdk_path, 'java.exe')
    resin_jar_path = os.path.join(resin_path, 'resin.jar')
    # log.logger.debug(weaverpro_path,cache_path,weaver_redis_path,doc_full_search_path,resin_exec)

    filter_list = []
    webxml_path = os.path.join(ecology_path, 'WEB-INF\\web.xml')
    tree = ET.parse(webxml_path)

    for i in tree.iterfind('filter/filter-name'):
        filter_list.append(i.text)

    if os.path.exists(ecology_path) and os.path.exists(resin_path) and os.path.exists(jdk_path) and sys_url != '':
        # todo
        # JAVA_HOME path 
        # esearch check
        disk_driver = ecology_path.split(':')[0]
        os_platform = platform.system()
        get_system_info(os_platform)
        get_memory_info(os_platform)
        get_disk_info(os_platform)
        get_cpu_info(os_platform)
        get_network_info(os_platform)
        get_disk_speed(disk_driver)
        check_maxconn(weaverpro_path)
        check_cache(cache_path)
        check_redis(weaver_redis_path)
        check_doc_search(doc_full_search_path)
        resin_check(java_exec, resin_path, resin_jar_path)
        check_sequence(filter_list)
        decrypt_weaver(sys_url, jre_path)
    else:
        log.logger.debug("The path does'n esixts,Please Check!")
        os._exit(0)
    os.system('Pause')
