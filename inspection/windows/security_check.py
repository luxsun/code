# -*- coding:utf-8 -*-
###
# File: python_java.py
# Created Date: 2020-10-10
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Friday October 16th 2020 12:28:07 pm
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
###
import jpype
import os
import requests
import json

# resin_path='F:\weaver\Resin'
# jdk_path = 'F:\weaver\jdk-8u151-windows-x64\JDK\\bin'
# sys_url = 'http://192.168.81.75:8081/'

# url = sys_url + "security/monitor/MonitorStatusForServer.jsp"
# headers = {'content-type': 'application/json'}

# ret = requests.get(url)
# if ret.status_code == 200:
#     data=ret.content.strip()

private_key="WEAVERECOLOGYSECURITY3.0VERSION201607150857"

def decrypt_weaver(sys_url):

        '''
        decrypt the aes encrypted data
        '''
        print()
        print('Security Check')
        url = sys_url + "security/monitor/MonitorStatusForServer.jsp"
        try:
            ret = requests.get(url)
        except BaseException as e:
            print('\t' + 'The Url cant reach,the reason maybe is {0},please check!'.format(str(e)))
            os._exit(0)
        if ret.status_code == 200:
            data = ret.content.strip()
        else:
            print('\t' + "The  Security url can't reach,please check!")
            os._exit(0)
        if os.path.exists('aescoder.jar'):
            jpype.startJVM(classpath=['aescoder.jar'])
            # os.listdir()
            # 加载自定义的java class
            JDClass = jpype.JClass("com.AESCoder")
            jd = JDClass()
            res=jd.decrypt(data, private_key)
            _dic = json.loads(str(res))
            print(_dic)
            print('\t'+'{:<30s} {:<30s}'.format('FirewallStatus',str(_dic['firewallStatus'])))
            print('\t'+'{:<30s} {:<30s}'.format('AutoUpdateStatus',str(_dic['autoUpdateStatus'])))
        else:
            print('\t' + 'Decrypt aescoder.jar does not exists,Please check!')
            os._exit(0)
        try:
            remoteversion = _dic['remoteVersion']
            print('\t' + '{:<30s} {:<30s}'.format('RemoteVersion', remoteversion))
        except BaseException as e:
            # remoteversion = ''
            print('\t' + '{:<30s} {:<30s}'.format('RemoteVersion', '未检测到RemoteVersion'))

        print('\t'+'{:<30s} {:<30s}'.format('SoftwareVersion', str(_dic['softwareVersion'])))
        # print('FirewallStatus' + '\t' +  + '\n'  +'AutoUpdateStatus' + '\t'+ str(_dic['autoUpdateStatus']))
        # return res
        jpype.shutdownJVM()






