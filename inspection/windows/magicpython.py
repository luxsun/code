# -*- coding:utf-8 -*-
###
# File: magicpython.py
# Created Date: 2020-10-20
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Tuesday October 20th 2020 2:40:27 pm
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
###
def test():
    ...
# 代替pass


print(id(...))  
# 单例
test()