# -*- coding:utf-8 -*-
###
# File: windowsresincheck.py
# Created Date: 2020-11-18
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Wednesday November 18th 2020 2:03:47 pm
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
###
import psutil
import os
resin_path = "D:\weaver\Resin"
resin_conf=str(os.path.join(resin_path,"conf")).lower()
java_pid = 0
def resin_process_check(resin_path):
    global java_pid
    resin_new_path=resin_path.replace("\\","/")
    for i in list(psutil.process_iter()):
        if i.name() == "javaw.exe":
            print(i.cmdline())
            if str(resin_new_path).lower() + "/log" in str(i.cmdline()).lower() and "watchdog" in str(i.cmdline()).lower() :
                print(i.cmdline())
            if  resin_conf.replace("\\",'') in str(i.cmdline()).lower().replace("\\",""):
                java_pid +=1
                print("test")
                



resin_process_check(resin_path)

# testlist=['d:\\WEAVER\\JDK\\bin\\javaw', '-Xmx5550m', '-Xms5550m', '-XX:ParallelGCThreads=20', '-XX:+UseConcMarkSweepGC', '-XX:-OmitStackTraceInFastThrow', '-XX:+UseParNewGC', '-XX:+DisableExplicitGC', '-javaagent:wagent.jar', '-Djdk.tls.ephemeralDHKeySize=2048', '-Dfile.encoding=GBK', '-Dresin.server=app-0', '-Djava.util.logging.manager=com.caucho.log.LogManagerImpl', '-Djava.system.class.loader=com.caucho.loader.SystemClassLoader', '-Djavax.management.builder.initial=com.caucho.jmx.MBeanServerBuilderImpl', '-Djava.awt.headless=true', '-Djava.awt.headlesslib=true', '-Dresin.home=/d:/WEAVER/Resin', '-Xrs', '-Dresin.exit.message=Shutdown: Server restarting due to configuration change', 'com.caucho.server.resin.Resin', '--root-directory', '/d:/WEAVER/Resin', '-conf', 'd:\\WEAVER\\Resin\\conf\\resin.xml', '-server', 'app-0', '-socketwait', '7810', '-resin-home', '/d:/WEAVER/Resin', '-root-directory', '/d:/WEAVER/Resin', '-server', 'app-0', 'start', '--log-directory', '/d:/WEAVER/Resin/log']

# if resin_conf.replace("\\",'') in str(testlist).lower().replace("\\",""):
#     print("testsuccess")
# else:
#     print(resin_conf)
#     print(str(testlist).lower().replace("\\",""))