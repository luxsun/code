#!/bin/bash
#---------------------------------------------------------------------------
# File: weaverbackup.sh
# Created Date: 2020-11-16
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Monday November 16th 2020 10:15:52 am
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
#---------------------------------------------------------------------------
basedir=$(pwd)
. $basedir/weaver_backup_vars.sh
function backup(){
if [ -d $backup_dir -a -d $destdir ];then
    echo '备份文件夹存在，开始备份'
    tar -zcvf weaver_backup$dt.tar.gz $backup_dir && mv weaver_backup$dt.tar.gz $destdir > /dev/null 2>&1
    if [ $? -eq 0 ];then
        echo '备份成功，移动到备份文件夹！'
    else
        echo '备份有误，请检查！'
    fi
    if [ $is_scp -eq 1 ];then
        echo "开始将备份移动到远程"
        sshpass  -V > /dev/null 2>&1
        if [ $? -eq 0 ];then
            sshpass -p $scp_passwd scp $destdir/weaver_backup$dt.tar.gz $scp_user@$scp_host:$scp_path
            if [ $? -eq 0 ];then
                echo '备份到远程成功'
            else
                echo "备份到远程失败，请检查"
            fi
        else
            yum install -y sshpass >/dev/null 2>&1
            backup
        fi
    fi
else
    echo "配置文件中需要备份的文件夹不存在，请检查！"
fi
}

backup > /var/log/weaver_backup$dt.log