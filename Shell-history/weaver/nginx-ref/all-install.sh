#!/bin/bash
#---------------------------------------------------------------------------
# File: all-install.sh
# Created Date: 2020-11-03
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Wednesday November 4th 2020 10:12:23 am
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
#---------------------------------------------------------------------------
#!/bin/bash
#used of one time install, version-2.0
#author: eryajf & shashuji
#2019-2-14 12:40
ip=http://weaver:208uUseppK@49.235.245.52
zabbixserver=192.168.10.11
#mkdir  "onetimeused"  && cd onetimeused && dir=`pwd`
#
##set color##
echoRed() { echo $'\e[0;31m'"$1"$'\e[0m'; }
echoGreen() { echo $'\e[0;32m'"$1"$'\e[0m'; }
echoYellow() { echo $'\e[0;33m'"$1"$'\e[0m'; }
##set color##
#
#判断一下当前用户
if [ "`whoami`" != "root" ];then
    echoRed "注意：当前系统用户非root用户，将无法执行安装等事宜！" && exit 1
fi
#---------------------------------------------------------------------------------------------------------------------------------------------
#               三级方法
#---------------------------------------------------------------------------------------------------------------------------------------------
S(){
    echo "-----------------------------------------------------"
    echo "运行source /etc/profile && source /etc/bashrc安装完成！"
    echo "-----------------------------------------------------"
}
#install
nginx(){
    cd `pwd`
    yum install -y zlib-devel pcre-devel e2fsprogs-devel  keyutils-libs-devel libsepol-devel libselinux-devel krb5-devel openssl openssl098e openssl-devel
    if [ $? -eq 0 ];then
        if [ -e nginx-1.16.1-install.tar.gz ];then
            if read -t 30 -p "please enter the dictory where nginx to install:" dictory   
            then                                             
                [ -d $dictory/nginx  ] && echoRed "检测到$dictory下已安装nginx，故而退出!" && rm -rf $dir && exit 1
                if [ -d $dictory ];then
                    tar -zxvf nginx-1.16.1-install.tar.gz
                    tar -zxvf nginx-sticky-module-ng.tar.gz -C $dictory
                    tar -zxvf nginx_upstream_check_module-master.tar.gz -C $dictory
                    tar -xvzf nginx-1.16.1.tar.gz && cd nginx-1.16.1 
                    ./configure --user=root --group=root --prefix=$dictory/nginx --with-http_stub_status_module --with-http_ssl_module --with-http_sub_module \
                    --add-module=$dictory/nginx_upstream_check_module-master --add-module=$dictory/nginx-sticky-module-ng   --with-stream --with-stream_ssl_module \
                    --with-http_realip_module && make && make install
                    ln -s $dictory/nginx/sbin/nginx /usr/local/sbin/nginx
                    cd ../
                    cp nginx.service /lib/systemd/system/ && systemctl enable nginx && cp nginx.conf $dictory/nginx/conf/nginx-reference.conf  
                    /usr/local/sbin/nginx -V &> /dev/null && echoGreen "nginx已完成安装，可尽情享用！" || echoYellow "可能安装有问题，请检查！"
                    rm -rf nginx-1.16.1.tar.gz  nginx-sticky-module-ng.tar.gz  nginx_upstream_check_module-master.tar.gz nginx-1.16.1 nginx.conf nginx.service
                else
                    echoYellow "The dictory that you input does'n not exists,please check!"
                fi    
            else                                             
                echo "Timeout"
                exit 1
            fi
        else
            echoYellow 'Please acquire nginx-1.16.1.tar.gz  nginx-sticky-module-ng.tar.gz nginx_upstream_check_module-master.tar.gz in current dictory!' 
        fi
       
    else
        echoYellow 'Yum install error,please acquire the yum is correct'
    fi
}
resin(){
    cd $dir && wget -V &> /dev/null || yum -y install wget
    [ -d /usr/local/resin ] && echoRed "检测到/usr/local下已安装resin，故而退出!" && rm -rf $dir && exit 1
    tar -xvzf resin-4.0.46.tar.gz && cd resin-4.0.46
    ./configure --prefix=/usr/local/resin
    mkdir -p /usr/local/resin/conf/
    mkdir -p /usr/local/resin/log
    useradd resin
    chown -R resin:resin /usr/local/resin/
    echo "all done"
}
tomcat(){
    cd $dir && wget -V &> /dev/null || yum -y install wget
    [ -d /usr/local/tomcat ] && echoRed "检测到/usr/local下已安装tomcat，故而退出！" && rm -rf $dir && exit 1
    wget $ip/pack/tomcat.tar.gz && tar xf tomcat.tar.gz && mv tomcat /usr/local/tomcat
    /usr/local/tomcat/bin/version.sh &> /dev/null  && echoGreen "已完成安装，可尽情享用！" || echoYellow "可能安装有问题，请检查！"
    rm -rf $dir
}
jdk8(){
    cd $dir && wget -V &> /dev/null || yum -y install wget
    java -version &> /dev/null && echoRed "检测到系统中有java命令，故而退出！" && rm -rf $dir && exit 1
    wget $ip/pack/jdk-8u192-linux-x64.tar.gz -O jdk.tar.gz && tar xf jdk.tar.gz -C /usr/local/
    echo 'JAVA_HOME=/usr/local/jdk1.8.0_192' >> /etc/profile && echo 'PATH=$PATH:$JAVA_HOME/bin' >> /etc/profile && echo 'export PATH' >> /etc/profile && source /etc/profile
    /usr/local/jdk1.8.0_192/bin/java -version &> /dev/null && echoGreen "已完成安装，可尽情享用！" || echoYellow "可能安装有问题，请检查！" 
    S && rm -rf $dir
}
jdk7(){
    java -version &> /dev/null && echoRed "检测到系统中有java命令，故而退出！" && rm -rf $dir && exit 1
    wget $ip/pack/jdk-7u79-linux-x64.tar.gz -O jdk7.tar.gz && tar xf jdk7.tar.gz && mv jdk*/ /usr/local/jdk7
    echo 'JAVA_HOME=/usr/local/jdk7' >> /etc/profile && echo 'PATH=$PATH:$JAVA_HOME/bin' >> /etc/profile && echo 'export PATH' >> /etc/profile && source /etc/profile
    /usr/local/jdk7/bin/java -version &> /dev/null && echoGreen "已完成安装，可尽情享用！" || echoYellow "可能安装有问题，请检查！" 
    S && rm -rf $dir
}
maven(){
    cd $dir && wget -V &> /dev/null || yum -y install wget
    mvn -v &> /dev/null && echoRed "检测到系统中有mvn命令，故而退出！" && rm -rf $dir && exit 1
    wget $ip/pack/maven.tar.gz && tar -xf maven.tar.gz -C /usr/local
    echo 'MAVEN_HOME=/usr/local/maven' >> /etc/profile && echo 'PATH=$PATH:$JAVA_HOME/bin:$MAVEN_HOME/bin' >> /etc/profile && source /etc/profile
    /usr/local/maven/bin/mvn -v &> /dev/null && echoGreen "已完成安装，可尽情享用！" || echoYellow "可能安装有问题，请检查！" 
    S && rm -rf $dir
}
mysql(){
    cd $dir && wget -V &> /dev/null || yum -y install wget
    mqnu=`cat /etc/passwd | grep mysql |wc -l`
    [ -d /usr/local/mysql ] && echoRed "检测到/usr/local下已安装mysql，故而退出！" && rm -rf $dir && exit 1
    if [ -f $dir/mysql-5.7.22-linux-glibc2.12-x86_64.tar.gz ];then
        echoGreen "mysql安装包存在,无需下载" 
    else
        wget $ip/pack/mysql-5.7.22-linux-glibc2.12-x86_64.tar.gz 
    fi
    if [ $mqnu -ne 1 ];then
        echoRed "mysql用户不存在，新建用户" && groupadd mysql && useradd -g mysql -s /sbin/nologin mysql
    else
        echoRed "mysql用户已经存在"
    fi
    yum install gcc gcc-c++ autoconf automake zlib* libxml* ncurses-devel libtool-ltdl-devel* make cmake -y
        (cat << EOF
[client]
port=3306
default_character_set=utf8

[mysql]
no_auto_rehash
default_character_set=utf8

[mysqld]
skip-name-resolve
back_log=1000
skip-ssl
character_set_server=utf8
interactive_timeout=600
wait_timeout=600
event_scheduler=ON
skip_name_resolve=ON
skip_external_locking=ON
max_connections=2500
server_id=1
port=3306
basedir=/usr/local/mysql
datadir=/usr/local/mysql/data
log_error=/usr/local/mysql/data/error.log

log_bin_trust_function_creators=1
transaction-isolation=READ-COMMITTED
sql-mode="STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION"


lower_case_table_names=1
group_concat_max_len = 102400

open-files-limit = 65535

innodb_buffer_pool_size=500M
innodb_data_file_path=ibdata1:200M:autoextend
innodb_flush_method=O_DIRECT
innodb_log_buffer_size=8M
innodb_log_file_size=1024M
innodb_open_files=1000
innodb_stats_on_metadata=OFF
innodb_file_per_table=ON
innodb_support_xa=ON
innodb_lock_wait_timeout=50
innodb_use_native_aio=ON
innodb_buffer_pool_instances=8
#sas
#innodb_io_capacity=200
#innodb_io_capacity_max=2000
#ssd
innodb_io_capacity=2000
innodb_io_capacity_max=4000

log_bin=mysql-bin
binlog_format=row
max_binlog_size=1024M
expire_logs_days=7

slow_query_log=1
log_slow_admin_statements=ON
#log_slow_slave_statements=ON
log_timestamps=SYSTEM
long_query_time=1
log_output=TABLE

key_buffer_size=8M
sort_buffer_size=800K
table_open_cache=2000
table_definition_cache=2000
thread_cache_size=100
tmp_table_size=2M
read_buffer_size=800K
read_rnd_buffer_size=256k
max_connect_errors=100000
max_allowed_packet=1024M

max_write_lock_count=102400
myisam_sort_buffer_size=8M
net_buffer_length=16K
open_files_limit=28192

#gtid
gtid_mode=on
enforce_gtid_consistency=ON
#slave_parallel_type=LOGICAL_CLOCK
#slave-parallel-workers=16
#master_info_repository=TABLE
#relay_log_info_repository=TABLE
#relay_log_recovery=ON
#skip_slave_start=1

#log_slave_updates=ON
#sync_master_info=1
query_cache_size=0
#query_cache_type=OFF
binlog_cache_size = 2M

log_warnings=1
[mysqldump]
quick
max_allowed_packet=50M

[myisamchk]
key_buffer_size=100M
sort_buffer_size=100M
read_buffer=2M
write_buffer=2M

[mysqlhotcopy]
interactive_timeout

[mysqld_safe]
open-files-limit=28192
EOF
) > /etc/my.cnf
    echoGreen "开始编译安装！！" && tar -xf  mysql-5.7.22-linux-glibc2.12-x86_64.tar.gz -C /usr/local/ 
    ln -s  /usr/local/mysql-5.7.22-linux-glibc2.12-x86_64  /usr/local/mysql && mkdir -p /usr/local/mysql/data && chown -R mysql:mysql /usr/local/mysql && /usr/local/mysql/bin/mysqld --initialize --user=mysql --datadir=/usr/local/mysql/data --basedir=/usr/local/mysql
    echoGreen "注册为服务！！" && ln -s /usr/local/mysql/support-files/mysql.server /etc/init.d/mysql && ln -s /usr/local/mysql/bin/mysql /usr/bin
    
    #  && service mysql start
    # 写入数据库配置文件

    passwd=`cat /usr/local/mysql/data/error.log | awk '/localhost\:/{print $NF}' `
    service mysql start 
    /usr/local/mysql/bin/mysqladmin -h localhost -u root -p"$passwd" password 'scmadmin' &&  echoGreen "初始密码已设置为scmadmin,请验证!"
    echoGreen "创建ecology数据库,默认密码为ecology123"
    /usr/local/mysql/bin/mysql -h  localhost -u root -pscmadmin -e "create database ecology;
    create user ecology identified with mysql_native_password by 'ecology123';
    grant all privileges on *.* to ' ecology '@'%';
    flush privileges;" 
    echo 'PATH=/usr/local/mysql/bin:$PATH' >> /etc/profile
    echo 'export PATH' >> /etc/profile && source /etc/profile
    /usr/local/mysql/bin/mysql -V &> /dev/null && echoGreen "已完成安装，可尽情享用！" || echoYellow "可能安装有问题，请检查！" 
    S && rm -rf $dir
}
node(){
    cd $dir && wget -V &> /dev/null || yum -y install wget
    [ -d /usr/local/node ] && echoRed "检测到/usr/local下已安装node，故而退出！" && rm -rf $dir && exit 1
    wget $ip/pack/node-v10.6.0-linux-x64.tar.xz && tar xf node-v10.6.0-linux-x64.tar.xz -C /usr/local &> /dev/null
    echo 'NODE=/usr/local/node' >> /etc/profile && echo 'PATH=$PATH:$NODE/bin' >> /etc/profile && echo 'export PATH' >> /etc/profile && source /etc/profile
    /usr/local/node/bin/node -v &> /dev/null && echoGreen "已完成安装，可尽情享用！" || echoYellow "可能安装有问题，请检查！" 
    S && rm -rf $dir
}
php(){
    cd $dir && wget -V &> /dev/null || yum -y install wget
    [ -d /usr/local/php ] && echoRed "检测到/usr/local下已安装php，故而退出！" && rm -rf $dir && exit 1
    wget $ip/pack/php-7.0.25.tar.gz --http-user 'xayw' --http-password 'SHKJ2019' && yum -y install libjpeg libjpeg-devel libpng libpng-devel freetype freetype-devel libxml2 libxml2-devel mysql pcre-devel curl-devel libxslt-devel openssl-devel
    tar -xf php-7.0.25.tar.gz && cd php-7.0.25 && ./configure --prefix=/usr/local/php --with-curl --with-freetype-dir --with-gd --with-gettext --with-iconv-dir --with-kerberos --with-libdir=lib64 --with-libxml-dir --with-mysqli --with-openssl --with-pcre-regex --with-pdo-mysql --with-pdo-sqlite --with-pear --with-png-dir --with-xmlrpc --with-xsl --with-zlib --enable-fpm --enable-bcmath --enable-libxml --enable-inline-optimization --enable-gd-native-ttf --enable-mbregex --enable-mbstring --enable-opcache --enable-pcntl --enable-shmop --enable-soap --enable-sockets --enable-sysvsem --enable-xml --enable-zip  && make && make test && make install
    \cp php.ini-development /usr/local/php/etc/php.ini && \cp /usr/local/php/etc/php-fpm.conf.default /usr/local/php/etc/php-fpm.conf && \cp /usr/local/php/etc/php-fpm.d/www.conf.default /usr/local/php/etc/php-fpm.d/www.conf && \cp -R ./sapi/fpm/php-fpm /etc/init.d/php-fpm
    /etc/init.d/php-fpm && echoGreen "已完成安装，可尽情享用！" || echoYellow "可能安装有问题，请检查！" 
    rm -rf $dir
}
zabbix(){
    cd $dir && wget -V &> /dev/null || yum -y install wget
    zabbix_agentd -V &> /dev/null && echoRed "检测到系统中有zabbix-agentd命令，故而退出！" && rm -rf $dir && exit 1
    wget $ip/pack/zabbix-agent-3.4.11-1.el7.x86_64.rpm && yum -y install $dir/zabbix-agent-3.4.11-1.el7.x86_64.rpm
    #修改相应的配置文件
    sed -i "s/^Server=.*/Server=$zabbixserver/g"  /etc/zabbix/zabbix_agentd.conf
    sed -i "s/^ServerActive=.*/ServerActive=$zabbixserver/g"  /etc/zabbix/zabbix_agentd.conf
    sed -i "s/^Hostname=.*/Hostname=$(hostname -I)/g"  /etc/zabbix/zabbix_agentd.conf
    systemctl enable zabbix-agent && systemctl restart zabbix-agent
    zabbix_agentd -V &> /dev/null && echoGreen "已完成安装，可尽情享用！" || echoYellow "可能安装有问题，请检查！" 
    rm -rf $dir
}
py3(){
    cd $dir && wget -V &> /dev/null || yum -y install wget
    python3.6 -V &> /dev/null && echoRed "检测到系统中有python3.6命令，故而退出！" && rm -rf $dir && exit 1
    yum -y install zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel wget gcc gcc-c++
    wget $ip/pack/Python-3.6.3.tgz && tar -xf Python-3.6.3.tgz && mv Python-3.6.3 /usr/local/Python-3.6.3 && cd /usr/local/Python-3.6.3 && ./configure \
    --prefix=/usr/local/python3 && make && make altinstall
    # cd /usr/bin && mv python python.backup && ln -s /usr/local/bin/python3.6 /usr/bin/python && ln -s /usr/local/bin/python3.6 /usr/bin/python3 && ln -s /usr/local/bin/pip3.6 /usr/bin/pip3
    # sed -i '1s/python/python2/g' /usr/bin/yum && sed -i '1s/python/python2/g' /usr/libexec/urlgrabber-ext-down
    ln -s /usr/local/python3/bin/python3.6 /usr/local/bin/python3
    ln -s /usr/local/python3/bin/pip3.6 /usr/local/bin/pip3
    mkdir -p  /root/.pip/

    cat >  /root/.pip/pip.conf   <<EOF
    [global]
    trusted-host=mirrors.aliyun.com
    index-url=http://mirrors.aliyun.com/pypi/simple/
    [install]
    trusted-host=mirrors.aliyun.com
EOF
    # /usr/local/bin/pip3.6
    /usr/local/bin/python3 -V && echoGreen "已完成安装，可尽情享用！" || echoYellow "可能安装有问题，请检查！" 
    rm -rf $dir
}
redis(){
    cd $dir && wget -V &> /dev/null || yum -y install wget
    redis-server -v &> /dev/null && echoRed "检测到系统中有redis-server命令，故而退出！" && rm -rf $dir && exit 1
    wget $ip/pack/redis-4.0.6.tar.gz && yum -y install gcc gcc-c++ && tar -xf redis-4.0.6.tar.gz && cd redis-4.0.6 && make && make install
    sed  -i '136s/no/yes/g' redis.conf && sed -i '166s/notice/warning/g' redis.conf && mv $dir/redis-4.0.6 /usr/local/redis
    /usr/local/redis/src/redis-server /usr/local/redis/redis.conf
    /usr/local/redis/src/redis-server && echoGreen "已完成安装，可尽情享用！" || echoYellow "可能安装有问题，请检查！" 
    rm -rf $dir
}
trash(){
    cd $dir && wget -V &> /dev/null || yum -y install wget
    /usr/bin/trash --version &> /dev/null && echoRed "检测到系统中有trash命令，故而退出！" && rm -rf $dir && exit 1
    wget $ip/pack/trash-cli.tar && tar xf trash-cli.tar && cd trash-cli && python setup.py install &> /dev/null
    #yum install -y python-setuptools.noarch &> /dev/null && easy_install trash-cli &> /dev/null && sleep 3
    echo "alias rm='trash-put'" >> /etc/bashrc && source /etc/bashrc 
    /usr/bin/trash --version &> /dev/null && echoGreen "回收站已完成安装，可尽情享用！" || echoYellow "可能安装有问题，请检查！"

    /usr/bin/autotrash -V &> /dev/null && echoRed "检测到系统中有autotrash命令，故而退出！" && rm -rf $dir && exit 1
    wget $ip/pack/autotrash.tar.gz && tar xf autotrash.tar.gz && cd autotrash && python setup.py install
    echo "#add clean tool" >> /var/spool/cron/root
    echo "@daily /usr/bin/autotrash -d 7" >> /var/spool/cron/root
    echo "#add clean tool" >> /var/spool/cron/aladin
    echo "@daily /usr/bin/autotrash -d 7" >> /var/spool/cron/aladin
    /usr/bin/autotrash -h &> /dev/null && echoGreen "自动删除回收站七天前的内容功能已配置完成，可尽情享用！" || echoYellow "可能安装有问题，请检查！"
    S && rm -rf $dir 
}

#init
vmtools(){
    #安装vmware tools
    [ -d  /usr/bin/vmware-toolbox-cmd ] && echo "已经安装vmwaretools $(vmware-toolbox-cmd -v)" &&  rm -rf $dir  &&  exit 0
    wget  $ip/pack/VMwareTools-10.1.7-5541682.tar.gz -O vmware-tools.tar.gz
    tar -xf vmware-tools.tar.gz && mv vmware-tools*/  ../vmware-tools && cd ../vmware-tools && chmod +x ./vmware-install.pl
    echo -e "\n\n请\n执\n行\n $(pwd)/vmware-install.pl\n手\n动\n完\n成\n安\n装\n\n"
    rm -rf $dir
}
allnewcentos(){
    yum install -y epel-release && mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
    wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
    yum clean all && yum makecache fast && yum install wget curl net-tools vim lrzsz ntpdate  -y
    timedatectl set-timezone Asia/Shanghai && /usr/sbin/ntpdate  -u ntp1.aliyun.com  &> /dev/null &
    echo "export  HISTTIMEFORMAT=\"`whoami` : %F %T :\""  >>  /etc/profile   && source /etc/profile
    #防火墙
    getenforce && setenforce 0 && sed -i  's/SELINUX=enforcing/SELINUX=disabled/g'  /etc/selinux/config
    systemctl status firewalld && systemctl stop firewalld && systemctl disable firewalld && systemctl status firewalld
    yum install iptables-services  -y && systemctl stop  iptables && systemctl disable   iptables
    /sbin/iptables -I INPUT -p tcp --dport 10036 -j ACCEPT && /sbin/iptables -I INPUT -p tcp --dport 80  -j ACCEPT
    service iptables save && sed -i 's/\\u@\\h\ /\\u@\\H\ /g' /etc/bashrc
    echo -e  "root soft nofile 65535\nroot hard nofile 65535\n* soft nofile 65535\n* hard nofile 65535\n"     >> /etc/security/limits.conf
    sed -i 's#4096#65535#g' /etc/security/limits.d/20-nproc.conf
    #下载vmtools
    [ ! -d /usr/bin/vmware-toolbox-cmd ] && wget  $ip/pack/VMwareTools-10.1.7-5541682.tar.gz  -O vmware-tools.tar.gz
    tar -xf vmware-tools.tar.gz && mv vmware-tools*/  ../vmware-tools  && chmod +x  ../vmware-tools/vmware-install.pl
    echo "----------------------------------------------------"
    echo -e "\n\n\n\n\n\n\n\n"
    cd .. && echo "请重启后手动执行 $(pwd)/vmware-install.pl 安装vmtools"
    echo -e "\n\n\n\n\n\n\n\n"
    sleep 5
    #调用模板脚本
    newvitrulhost
    echo
}
newvitrulhost(){
    #VM虚拟机克隆安装配置项。
    #默认删除网卡信息，配置静态IP，更改主机名,安装zabbixagent。
    #只需配置一次，用完即可删除本文档。
    echo "----------------------------------------------------"
    [ -e /etc/sysconfig/network-scripts/ifcfg-eth0 ] &&  echo -e "\n请勿\n    重复执行\n" &&  rm -rf $dir  &&exit 1
    hostnameip=$(hostname -I)
    int=$(ls /etc/sysconfig/network-scripts/ifcfg-*   | grep -v lo)
    eth=$(nmcli dev status | grep connected | awk '{print $1}')
    hostname=$(echo "ALD-"$(echo $hostnameip|awk -F'.' '{print $4}')"-SERVER")
    #在前面定义了zabbixserver=192.168.10.11
    rpm -ivh http://mirrors.aliyun.com/zabbix/zabbix/3.4/rhel/7/x86_64/zabbix-release-3.4-1.el7.centos.noarch.rpm
    sleep 2
    # yum install zabbix-agent  -y
    echo "当前IP地址： "[$hostnameip]
    echo "网卡名称：   "$eth
    echo "网络文件路径： "$int
    echo "当前主机名："$(hostname)
    echo "新主机名为："$hostname
    echo "zabbix服务器为：["$zabbixserver]
    echo "----------------------------------------------------"
    echo "即将进行："
    echo "         删除克隆的网卡信息、配置静态IP、更改主机名、修改zabbixAgent"
    echo "----------------------------------------------------"
    #read -n 1 -t 30  -p "是否进行修改?y/n" Number
    #echo
    #case $Number in
    #[Yy])
    echo "----------------------------------------------------"
    echo "    (1) 删除克隆的网卡信息"
    rm  -f   /etc/udev/rules.d/*.rules
    sed -i 's/UUID/#UUID/g'   $int
    grep ^DEVICE  $int &&   sed -i "s/^DEVICE.*/DEVICE=$eth/g"   $int ||echo "DEVICE="$eth  >>  $int
    cat $int | grep UUID
    cat $int  | grep DEVICE
    #配置静态IP
    echo "----------------------------------------------------"
    echo "    (2) 配置静态IP"
    sed -i 's/dhcp/static/i'  $int
    grep ^IPADDR  $int &&    sed -i "s/^IPADDR=.*/IPADDR=$hostnameip/g"  $int || echo "IPADDR="$hostnameip >>$int
    sed -i "s/^NETMASK=.*//g" $int
    grep ^PREFIX  $int &&  sed -i "s/^PREFIX=.*/PREFIX=24/g" $int || echo "PREFIX=24" >>$int
    grep ^GATEWAY $int &&  sed -i "s/^GATEWAY=.*/GATEWAY=$(echo $hostnameip|awk -F'.' '{print $1"."$2"."$3}').254/g" $int||echo "GATEWAY="$(echo $hostnameip | awk -F'.' '{print $1"."$2"."$3}')".254" >>$int
    grep ^DNS=  $int &&  sed -i "s/^DNS=.*//g" $int
    grep ^DNS1=  $int &&  sed -i "s/^DNS1=.*/DNS1=119.29.29.29/g" $int || echo "DNS1=119.29.29.29" >>$int
    grep ^DNS2=  $int &&  sed -i "s/^DNS2=.*/DNS2=223.5.5.5/g" $int || echo "DNS2=223.5.5.5" >>$int
    grep ^DNS3=  $int &&  sed -i "s/^DNS3=.*/DNS3=223.6.6.6/g" $int || echo "DNS3=223.6.6.6" >>$int
    cat $int | grep  BOOTPROTO
    cat $int | grep --color -o "\([0-9]\{1,3\}\.\)\{3\}[0-9]\{1,3\}"
    #更改主机名
    echo "----------------------------------------------------"
    echo "     (3) 更改主机名"
    hostnamectl set-hostname $hostname
    hostname
    sed -i 's/\\u@\\h\ /\\u@\\H\ /g'   /etc/bashrc
    #yum -y install open-vm-tools
    echo "----------------------------------------------------"
    systemctl restart network
    echo "    (4) 修改网卡名称为eth0"
    mv $(ls /etc/sysconfig/network-scripts/ifcfg-*   | grep -v lo )  /etc/sysconfig/network-scripts/ifcfg-eth0 &> /dev/null
    rm  -f   /etc/udev/rules.d/*.rules
    #sed -i 's/UUID/#UUID/g'   $int
    sed -i 's/NAME=.*/NAME=eth0/g'    /etc/sysconfig/network-scripts/ifcfg-eth0
    sed -i 's/DEVICE=.*/DEVICE=eth0/g'    /etc/sysconfig/network-scripts/ifcfg-eth0
    #关闭IPV6
    grep ^IPV6INIT  /etc/sysconfig/network-scripts/ifcfg-eth0  &&  sed -i "s/^IPV6INIT=yes/IPV6INIT=no/g"  /etc/sysconfig/network-scripts/ifcfg-eth0  ||  echo "IPV6INIT=no" >> /etc/sysconfig/network-scripts/ifcfg-eth0
    sed -i "/^IPV6_/d" /etc/sysconfig/network-scripts/ifcfg-eth0
    systemctl  disable   ip6tables.service
    echo "net.ipv6.conf.all.disable_ipv6=1"  >>/etc/sysctl.conf
    sed  -i   's/GRUB_CMDLINE_LINUX=\"crashkernel/GRUB_CMDLINE_LINUX=\"net.ifnames=0 biosdevname=0\ crashkernel/g' /etc/default/grub
    grub2-mkconfig -o /etc/grub2.cfg   &> /dev/null
    ls /etc/sysconfig/network-scripts/ifcfg-*   | grep -v lo
    echo "----------------------------------------------------"
    echo "    (5) 更新zabbixAgent"
    #rpm -ivh http://mirrors.aliyun.com/zabbix/zabbix/3.4/rhel/7/x86_64/zabbix-release-3.4-1.el7.centos.noarch.rpm
    #sleep 2
    #yum install zabbix-agent  -y
    #ls /etc/zabbix/zabbix_agentd.conf
    sed -i "s/^Server=.*/Server=$zabbixserver/g"  /etc/zabbix/zabbix_agentd.conf
    sed -i "s/^ServerActive=.*/ServerActive=$zabbixserver/g"  /etc/zabbix/zabbix_agentd.conf
    sed -i "s/^Hostname=.*/Hostname=$hostnameip/g"  /etc/zabbix/zabbix_agentd.conf
    cat /etc/zabbix/zabbix_agentd.conf   | grep ^Server
    cat /etc/zabbix/zabbix_agentd.conf   | grep ^Hostname
    systemctl enable zabbix-agent
    systemctl restart  zabbix-agent
    systemctl status zabbix-agent
    echo "----------------------------------------------------"
    echo "    (6) 配置完成，五秒后重启"
    #read -n 1 -t 30  -p "是否重启主机reboot，y/n? " Number
    echo "----------------------------------------------------"
    echo "$(date)"
    echo "----------------------------------------------------"
    rm -rf $dir
    echo "rebooting....."
    #case $Number in
    #[Yy])
    #重启
    #   echo "" >  ./.bash_history   && history -c
    shutdown -r  now  && exit 0
    #;;
    #esac
    #   ;;
    #   esac
}
changeipaddress(){
    changeip=$(whiptail --title "更改IP" --inputbox "请输入新的IP地址" 10 60 `hostname -I` 3>&1 1>&2 2>&3)
    exitstatus=$?
    if [ $exitstatus = 0 ]; then
        whiptail --title "Message" --msgbox "IP地址将由\n$(hostname -I)\n改为:\n$changeip\n" 10 60
        #判断IP是否
        if echo $changeip | grep "^\([0-9]\{1,3\}\.\)\{3\}[0-9]\{1,3\}$";then
            #判断文件是否规范
            [ ! -e /etc/sysconfig/network-scripts/ifcfg-eth0 ]   && echo -e "\n网卡配置文件不规范，请检查 ：\n  /etc/sysconfig/network-scripts/ifcfg-eth0\n"  &&  rm -rf $dir  &&  exit 1
            #判断IP是否可用
            ping -c 2 $changeip  > /dev/null && echo -e "\n[$changeip]\n 该IP已在使用中，请检查\n"   && rm -rf $dir && exit 1 || echo "该IP可用"
            #执行
            #       echo "$(hostname -I) >>> $changeip"
            sed -i 's/dhcp/static/i'  /etc/sysconfig/network-scripts/ifcfg-eth0
            grep ^IPADDR  /etc/sysconfig/network-scripts/ifcfg-eth0 &&    sed -i "s/^IPADDR=.*/IPADDR=$changeip/g"  /etc/sysconfig/network-scripts/ifcfg-eth0 || echo "IPADDR="$changeip >>/etc/sysconfig/network-scripts/ifcfg-eth0
            sed -i "s/^NETMASK=.*//g" /etc/sysconfig/network-scripts/ifcfg-eth0
            grep ^PREFIX  /etc/sysconfig/network-scripts/ifcfg-eth0 &&  sed -i "s/^PREFIX=.*/PREFIX=24/g" /etc/sysconfig/network-scripts/ifcfg-eth0 || echo "PREFIX=24" >>/etc/sysconfig/network-scripts/ifcfg-eth0
            grep ^GATEWAY /etc/sysconfig/network-scripts/ifcfg-eth0 &&  sed -i "s/^GATEWAY=.*/GATEWAY=$(echo $changeip|awk -F'.' '{print $1"."$2"."$3}').254/g" /etc/sysconfig/network-scripts/ifcfg-eth0||echo "GATEWAY="$(echo $changeip | awk -F'.' '{print $1"."$2"."$3}')".254" >>/etc/sysconfig/network-scripts/ifcfg-eth0
            grep ^DNS=  /etc/sysconfig/network-scripts/ifcfg-eth0 &&  sed -i "s/^DNS=.*//g" /etc/sysconfig/network-scripts/ifcfg-eth0
            grep ^DNS1=  /etc/sysconfig/network-scripts/ifcfg-eth0 &&  sed -i "s/^DNS1=.*/DNS1=100.100.2.136/g" /etc/sysconfig/network-scripts/ifcfg-eth0 || echo "DNS1=100.100.2.136" >>/etc/sysconfig/network-scripts/ifcfg-eth0
            grep ^DNS2=  /etc/sysconfig/network-scripts/ifcfg-eth0 &&  sed -i "s/^DNS2=.*/DNS2=100.100.2.138/g" /etc/sysconfig/network-scripts/ifcfg-eth0 || echo "DNS2=100.100.2.138" >>/etc/sysconfig/network-scripts/ifcfg-eth0
            grep ^DNS3=  /etc/sysconfig/network-scripts/ifcfg-eth0 &&  sed -i "s/^DNS3=.*/DNS3=114.114.114.114/g" /etc/sysconfig/network-scripts/ifcfg-eth0 || echo "DNS3=114.114.114.114" >>/etc/sysconfig/network-scripts/ifcfg-eth0
            sed -i "s/^Hostname=.*/Hostname=$changeip/g"  /etc/zabbix/zabbix_agentd.conf
            systemctl restart zabbix-agent
            echo -e "\n修改完毕，请手动重启网卡:\n    systemctl restart network\n"
            #systemctl restart network
            rm -rf $dir
        else
            echo "输入的IP不合法"
        fi
    else
        xuanxiang
    fi
}
changehostname(){
    CHANGENAME=$(whiptail --title "更改主机名" --inputbox "请输入新的主机名，用-来连接" 10 60 `hostname` 3>&1 1>&2 2>&3)
    exitstatus=$?
    if [ $exitstatus = 0 ]; then
        whiptail --title "Message" --msgbox "主机名由\n$(hostname)\n改为:\n$CHANGENAME\n" 10 60
        # whiptail --title "Yes/No Box" --yesno "Choose between Yes and No." --msgbox "主机名将由\n$(hostname)\n改为:\n\"$NAME\"\n""asdasdasd"  10 60
        hostnamectl set-hostname $CHANGENAME
        echo "hostname :  $(hostname)"
    else
        #echo "You chose Cancel."
        xuanxiang
    fi
}

alili(){
    #root用户操作
    #root用户操作
    useradd aladin && useradd develop
    yum -y install lrzsz net-tools vim curl wget unzip gunzip git mysql expect ntpdate
    echo "export HISTTIMEFORMAT=\"`whoami` : %F %T :\"" >> /etc/profile && source /etc/profile
    sed -i 's/HISTSIZE=1000/HISTSIZE=5000/g' /etc/profile
    sed -i 's/#Port 22/Port 10036/g' /etc/ssh/sshd_config && systemctl restart sshd
cat > /root/.bashrc << EOF
# .bashrc

# User specific aliases and functions

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias untar='tar xvf '
alias grep='grep --color=auto'
alias getpass="openssl rand -base64 20"

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi
PS1="\[\e[37;40m\][\[\e[36;40m\]\u\[\e[37;40m\]@\h \[\e[35;40m\]\W\[\e[0m\]]\$"
EOF
    source /root/.bashrc
    echo "0 */2 * * *  /usr/sbin/ntpdate  -u ntp1.aliyun.com  &> /dev/null # ntpdate" >> /var/spool/cron/root
    #配置回收站
    trash
#aladin init
    echoYellow "开始初始化aladin用户"
cat > /home/aladin/.bashrc << EOF
# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
PS1="\[\e[37;40m\][\[\e[36;40m\]\u\[\e[37;40m\]@\h \[\e[35;40m\]\W\[\e[0m\]]\$"
EOF
    chown aladin.aladin /home/aladin/.bashrc

    mkdir /home/aladin/.ssh && chmod 700 /home/aladin/.ssh
cat > /home/aladin/.ssh/authorized_keys << EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDdEw+PsiXnaM7feJ1cgpBFksWMM33QGAIB2E5fW5d85Cc/+4I0+2Som+ukV9uCxyuUs1xexqBKuT7PdQrxzHgUwnevJFFhFMagnCR00HKRjpmCC7QAK48SsvdbU/6hvGBsaTH8V0eipNgatnhFnnGGkDLtqw/aSjWbdT1dfAgHbuACzko1cnc0JjP6r+DGImWlNn+BKO2UziI5w0G1J7k2/WPFldj++2Ja6Q7clHB1FNz06mDxSfCmenrophaFjnvzwNoPITs2CTTp/yh+uvMZTaqcYfc93dy2fyvy+GsVH6+WjQVLxyGRIKxYsTh+spN9ngxHWqV7RTNHTbBEj1t3 aladin@jump
EOF
    chmod 600 /home/aladin/.ssh/authorized_keys && chown -R aladin.aladin /home/aladin/.ssh
    echoGreen "aladin用户初始化完毕！"
#develop init
    echoYellow "开始初始化develop用户"
    mkdir /home/develop/.ssh && chmod 700 /home/develop/.ssh
cat > /home/develop/.ssh/authorized_keys << EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1Cy+UURmFDlxT/RQcz7m2mws82Bf5ZYFD/RwjYNqkPDlayGihSU7LBfdgDKrJuaYt62SyGveV88z6xM+NzaPH6gZAWJr1S+lUBz27TLoBenMKfMahsrqI1eQ/Lic7wOLhiP3UOuFvG+y9udRNt56FiQsmrNzpHWPHLczpei4flPaGk7/Xv6yUmuEhRaa2itorA9qnUD2ZlGyTWaCWyroEdffeypq38aBFVSyKi3c3kbokx8anN6YFzNlZBvjbGDUzK6eOs3ufOiwJHdokTkMjBW9LtAbJPAEBzcSmozgi5Eg7Yn0TBu5RZ12xBBvXU8MHGGuYmjYeJjMyUS6e/8KX develop@jump
EOF
    chmod 600 /home/develop/.ssh/authorized_keys && chown -R develop.develop /home/develop/.ssh
    echoGreen "develop用户初始化完毕！"
    echoGreen "--------------------------------------------------------------------------------------------------------"
    echoRed "服务器已初始化完毕，所做事情：1，更改默认22端口。2，更改命令提示符颜色及添加命令别名。3，更改历史命令条数与显示规则。"
    echoRed "4，安装基础软件。5，配置回收站。6，初始化aladin用户。7，初始化develop用户。8，时间同步。"
    echoGreen "--------------------------------------------------------------------------------------------------------"
}

A(){
    echo -e  "\e[36m ****\n您\n选\n择\n安\n装\n的\n是\n$OPTION\n，\n现\n在\n开\n始\n安\n装\n$OPTION\n****  \e[39m"
}


#---------------------------------------------------------------------------------------------------------------------------------------------
#               二级菜单
#---------------------------------------------------------------------------------------------------------------------------------------------

anzhuang(){
    OPTION=$(whiptail --title "Weaver-安装脚本" --menu "请选择想要安装的项目，上下键进行选择，回车即安装，Tab键可选择<Cancel>返回上层！" 25 55 15 \
        "1" "nginx-1.16.1" \
        "2" "py-3.6" \
        "3" "redis-4.0.6" \
        "4" "mysql-5.7"  3>&1 1>&2 2>&3  )
        # "2" "jdk-1.8" \
        # "3" "tomcat-8" \
        # "5" "node-10.6" \
        # "6" "maven-3.3" \
        # "7" "php-7.0" \
        # "8" "zabbix-agent-3.4" \
        # "11" "trash-autotrash" \
        # "12" "jdk-1.7"  \
        # "5" "暂时未定义" 
    case $OPTION in
    1)
        A && nginx
        ;;
    2)
        A && py3
        ;;
    3)
        A && redis
        ;;
    4)
        A && mysql
        ;;
    *) 
        rm -rf $dir && xuanxiang
        ;;
    esac
}

chushihua(){
    OPTION=$(whiptail --title "运维外挂-初始化菜单" --menu "请选择想要初始化的选项，上下键进行选择，回车即运行，左右键可选择<Cancel>返回上层！" 25 50 10 \
    "1" "虚拟机 moban clone host" \
    "2" "init a new CeontOS" \
    "3" "zabbix agent" \
    "4" "vmtools" \
    "5" "change ip address" \
    "6" "change hostname"  \
    "7" "aliyun init" 3>&1 1>&2 2>&3 )

    case $OPTION in
    1)
        A && sleep 3 && newvitrulhost
        ;;
    2)
        A && sleep 3 && allnewcentos
        ;;
    3)
        A && sleep 3 && zabbix
        ;;
    4)
        A && sleep 3 && vmtools
        ;;
    5)
        A && changeipaddress
        ;;
    6)
        A && changehostname
        ;;
    7)
        A && alili
        ;;
    *) 
        rm -rf $dir && xuanxiang
        ;;
    esac
}

youhua(){
    OPTION=$(whiptail --title "运维外挂-优化菜单" --menu "请选择想要优化的选项，上下键进行选择，回车即运行，左右键可选择<Cancel>返回上层！" 25 50 4 \
    "1" "优化11111111111" \
    "2" "优化22222222222" \
    "3" "优化33333333333" \
    "4" "优化44444444444"  3>&1 1>&2 2>&3 )

    case $OPTION in
    1)
        A
        ;;
    2)
        A
        ;;
    3)
        A
        ;;
    4)
        A
        ;;
    *) 
        rm -rf $dir && xuanxiang
        ;;
    esac
}

qita(){
    OPTION=$(whiptail --title "运维外挂-其他菜单" --menu "请选择相应的选项，上下键进行选择，回车即运行，左右键可选择<Cancel>返回上层！" 25 50 4 \
    "1" "其他11111111111" \
    "2" "其他22222222222" \
    "3" "其他33333333333" \
    "4" "其他44444444444"  3>&1 1>&2 2>&3 )

    case $OPTION in
    1)
        A
        ;;
    2)
        A
        ;;
    3)
        A
        ;;
    4)
        A
        ;;
    *) 
        rm -rf $dir && xuanxiang
        ;;
    esac
}

#---------------------------------------------------------------------------------------------------------------------------------------------
#               入口菜单
#---------------------------------------------------------------------------------------------------------------------------------------------
xuanxiang(){
    OPTION=$(whiptail --title "Weaver-安装脚本" --menu "请选择想要操作的菜单，回车即可进入！" 30 60 6 \
    "1" "安装(install service)" \
    "2" "初始化(new initialization)"  3>&1 1>&2 2>&3 )
    # "3" "优化(optimization)" 
    # "3" "优化(optimization)" \
    # "4" "其他(others)"  3>&1 1>&2 2>&3 )

    case $OPTION in
    1)
        anzhuang 
        ;;
    2)
        chushihua
        ;;
    # 3)
    #     youhua
    #     ;;
    # 4)
    #     qita
    #     ;;
    *) 
        rm -rf $dir && echo "You chose Cancel."
        ;;
    esac
}

#调用首页
xuanxiang
