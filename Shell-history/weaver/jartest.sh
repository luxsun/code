#!/bin/bash
#---------------------------------------------------------------------------
# File: jartest.sh
# Created Date: 2020-10-22
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Thursday October 22nd 2020 6:51:21 pm
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
#---------------------------------------------------------------------------
# sys_url='http://192.168.7.239:8080/'
# url=$sys_url"security/monitor/MonitorStatusForServer.jsp"

# resutl=$(curl -s  $url > test.txt )
# fin=$(grep -v '^$' test.txt | tr -d '' |tr -d '\r')
# echo $fin


parse_json(){
echo "${1//\"/}" | sed "s/.*$2:\([^,}]*\).*/\1/"
}

fin="ae6502f1926b3f2dfcc591eba32fca6d6af38c5ea5a167bf571773031b86ea01ea2e0fb69cf81d4405c5635f9d6715e30b9baf86804d6a131333c648f72e1282b06ed54ae561a52f03519793741bbd3e1461aa931b372360b1e41e702f2d51d05882c9da81c06ba35f42c5ab349a1e804694a70a0fc72e4fae00cec8cb3ce372469c8003b77b27733b0c27bb2f10ffca23653813cf078b1d53074aeee3dbf703ebe9bc1fb4569a7589824ab7bf7cf1c432da0fd1e6f222bee9f18146cacf8e098355d85c771f7db01ca149c5aa534c7bfccc253b3b3c905a6eba823f6e0956e9864313b52bc47f0a9144a6cd87996500a97e66f8c4bd9d48c60453f09185e59be13bd87903fbd2e0c30b4dd275285f14b48257d701c29d3b2a1785cf4a112b47ca1fd391c05db6ce17891fcfbf3ec1011a6fecdc54482a715d7cf53b2637f89dd1b21afd0038e82880f3765dad97a65b70771f71d50da210f8f9b53210113f20681bfababc6a846e8c2713647cc3d0e59f62406319923f0746490921fb842ccd7154950d082127ce6f79f1d26b3ecce13311e3afe78b0c6a81f22a98ce62184330d249dcfab9707d1ae616a6cb4a47b5178f3dce8712f7d3b51c26d28388a03420a8b3bf592f772a3f65f6450536a684925645bfa9f5aed1b3f72e4d9e5a050ea7135751b69b1bbf82700508b366c246be80a1246028af3fe834256c828cd5255f62c69861254ef3f95f46147dfd252ff5f3df54e2ceced40b14e3ef0b8ecab8a307e50190ade42b4aaa595cf369bb4b26c648d5f459a50dd6a9e9eb8f9b7da4382f80523e1b7beae4f182918bed6145a70b67a7ac8076b4356a8d0a03e7b6b248989bb87bd4e15e737732b9e9f83520c47a7e36da6bb35f5e2748191002755b575f8c5605f5f77f1a10faa8db02a6a7b3f950d6f102cd14e8e7e0ab0a63fee6b205fab15099b92a2c2544f0fea55bc0f94a0404e34fd213b63cffbe0e1d558dabeb27adaad585f22d61477e060344bf9b67eca1709e29ff9dcf133a1b734079"

access_secur(){
    if [ -e AESCoder.jar ];then
    if [ -n $fin ];then
        # returnMsg="{sysid:6C3E226B4D4795D518AB341B0824EC29,companyName:E9TEST,ecVersion:9.00.2002.02,firewallStatus:true,autoUpdateStatus:true,softwareVersion:v10.29,ruleVersion:v10.10,loginStatus:true,pageStatus:true,dataStatus:true,enableServiceCheck:true,isUseESAPISQL:true,isUseESAPIXSS:true,isResetCookie:false,httpOnly:true,hostStatus:false,isRefAll:false,httpSep:true,isCheckSessionTimeout:true,isEnableForbiddenIp:false,autoRemind:true,isConfigFirewall:true,isEnableAccessLog:false,checkSocketTimeout:false,isResinAdmin:true,is404PageConfig:true,is500PageConfig:true,isDisabledHttpMethod:true,joinSystemSecurity:true,isWlanNetwork:true}"
        returnMsg=$(/usr/weaver/jdk1.8.0_151/bin/java -jar ./AESCoder.jar $fin | tr -d ' ')
        echo $returnMsg
        firewallstatus=$(parse_json $returnMsg "firewallStatus")
        AutoUpdateStatus=$(parse_json $returnMsg "autoUpdateStatus")
        SoftwareVersion=$(parse_json $returnMsg "softwareVersion")
        remote_stat=$(echo $returnMsg | grep remoteVersion | wc -l)
        echo $remote_stat
        if [ $remote_stat -gt 0 ];then
            RemoteVersion=$(parse_json $returnMsg "remoteVersion")
        else
            RemoteVersion='not found'
        fi
        echo firewallstatus: $firewallstatus
        echo AutoUpdateStatus: $AutoUpdateStatus
        echo SoftwareVersion: $SoftwareVersion
        echo RemoteVersion: $RemoteVersion
    else
        echo 'Decrypt Error,Plase Check!'
    fi
else
    echo 'AESCoder.jar Could not find Or Curl result is null,Please Check!'
fi
}


access_secur