#!/bin/bash
#---------------------------------------------------------------------------
# File: iperftest.sh
# Created Date: 2020-11-11
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Wednesday November 11th 2020 11:13:20 am
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
#---------------------------------------------------------------------------
trap_ctrlc() {
    echo -e "\n\nThe script will exit in 3600s after,if you want to exit,please open a new termail and use pkill iperf to exit!\n\n"
}

trap_ctrlc 
if [ -e "iperf3-3.1.3-1.fc24.x86_64.rpm" ];then
    result=$(iperf3 -v > /dev/null 2&>1)
    if [ $? -ne 0 ];then
        rpm -ivh  iperf3-3.1.3-1.fc24.x86_64.rpm
       timeout 3600 iperf3 -s

    else
        # kill -INT -$$
        timeout 3600 iperf3 -s

    fi
fi
