#!/bin/bash
#---------------------------------------------------------------------------
# File: weaver_backup_vars.sh
# Created Date: 2020-11-16
# Author: sunzhe
# Contact: <sunzhenet@163.com>
# 
# Last Modified: Monday November 16th 2020 10:20:41 am
# 
# Copyright (c) 2020 Weaver
# It is never too late to be what you might have been.
# Dec:
# -----
# HISTORY:
# Date      	 By	Comments
# ----------	---	----------------------------------------------------------
#---------------------------------------------------------------------------
dt="_`date '+%Y%m%d'`"
# 需要备份的文件夹
backup_dir='/root/go'
# 备份完成后需要放的目录
destdir='/tmp'
# 是否scp到远程服务器 1代表是 0代表不发送到远程
is_scp=1
# 远程服务器IP
scp_host='192.168.81.40'
# 远程服务器用户
scp_user='root'
# 远程服务器密码
scp_passwd='password'
# 远程服务器路径
scp_path='/data/'

# crontab 08 10 * * * cd /code/Shell-history/weaver/ && sh /code/Shell-history/weaver/weaverbackup.sh